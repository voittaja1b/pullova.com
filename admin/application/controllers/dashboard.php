<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {
    
	/**
	 * Dashboard Page
	 */
	public function index()
	{
        if( $this->_checkLoginOrRedirect() ) {
            $period = ($this->input->get('period')!==FALSE) ? $this->input->get('period') : 'all';
            $this->load->model('Summary_model');
            $summary = $this->Summary_model->getProfitSummary($period);
            $auth = array(
                'user_fullname'=>$this->session->userdata('SESS_RID_ADMNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_ADMAVATAR')
            );
            $data = array(
                'sideNavViewData' => array(
                    'active' => 'dashboard',
                    'auth' => $auth
                ),
                'contentPaneView' => 'dashboard',
                'contentPaneViewData' => array(
                    'summary' => $summary,
                    'period' => $period
                ),
                'auth' => $auth
            );
            $this->load->view('template-layout-auth', $data);
        }
    }
}