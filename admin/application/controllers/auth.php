<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller {
	public function index()
	{
        if( $this->_isLoggedIn() ) { //if already logged in, redirect to dashboard
            redirect('dashboard');
        } elseif($this->input->post('login') !== FALSE) { //when method is post, and the user has sent the login request
            $this->_doAuth();
        } else {
            $data = array(
                'contentPaneView' => 'login-form',
                'contentPaneViewData' => array()
            );
            $this->load->view('template-layout-pub', $data);
        }
    }

    public function logout() {
        $this->session->unset_userdata( array(
            'admin_logged_in' => false,
            'SESS_RID_ADMID' => '',
            'SESS_RID_ADMNAME' => '',
            'SESS_RID_ADMAVATAR' => '',
            'SESS_RID_ADMEMAIL' => '',
            'SESS_RID_ADMROLE' => '',
            'SESS_RID_ADMSTATUS' => ''
        ));
        redirect('login');
    }

    protected function _doAuth() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Administer_model');
        // setting up form validation
        $this->form_validation->set_rules('admin_name', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('admin_password', 'Password', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) { //form validation fails
            $viewData = array(
                'toasts' => array(
                    array('message' => 'Invalid Username and Password!', 'type'=>'warn')
                ),
                'contentPaneView' => 'login-form',
                'contentPaneViewData' => array()
            );
            $this->load->view('template-layout-pub', $viewData);
        } else { // form validation success
            $username = $this->input->post('admin_name');
            $password = $this->input->post('admin_password');
            $user = $this->Administer_model->doAuth($username, $password);
            if($user) { // authentication successs
                $this->session->set_userdata('admin_logged_in', TRUE);
                $this->session->set_userdata('SESS_RID_ADMID', $user->admin_id);
                $this->session->set_userdata('SESS_RID_ADMNAME', $user->admin_name);
                $this->session->set_userdata('SESS_RID_ADMAVATAR', 'default avatar');
                $this->session->set_userdata('SESS_RID_ADMEMAIL', $user->admin_email);
                $this->session->set_userdata('SESS_RID_ADMROLE', $user->admin_role);
                $this->session->set_userdata('SESS_RID_ADMSTATUS', $user->admin_status);
                redirect('dashboard');
                return;
            } else { // authentication failure
                $viewData = array(
                    'toasts' => array(
                        array('message' => 'Wrong Username or Password!', 'type'=>'warn')
                    ),
                    'contentPaneView' => 'login-form',
                    'contentPaneViewData' => array()
                );
                $this->load->view('template-layout-pub', $viewData);
            }
        }
    }
}