<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trips extends MY_Controller {
	public function index()
	{
        if( $this->_checkLoginOrRedirect() ) {
            $auth = array(
                'user_fullname'=>$this->session->userdata('SESS_RID_ADMNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_ADMAVATAR')
            );
            $data = array(
                'sideNavViewData' => array(
                    'active'=>'trips',
                    'auth' => $auth
                ),
                'contentPaneView' => 'trips_view',
                'contentPaneViewData' => array(),
                'auth' => $auth
            );
            $this->load->view('template-layout-auth', $data);
        }
    }

    public function get_detail($id) {
        $this->load->model('Trip_model');
        $predicate = array(
            'offset' => 0,
            'limit' => 1,
            'iFeedID' => $id
        );
        $dbColumns = array(
            'iFeedID',
            'vFeedTitle',
            'vUserView',
            'tFeedDescription',
            'vCost',
            'fAddress',
            'tAddress',
            'cAddress',
            'vStartDate',
            'currentLat',
            'currentLong',
            'toLat',
            'toLong',
            'user.userPhone',
        );
        $res = $this->Trip_model->find($predicate, $dbColumns);
        
        $this->output->set_content_type('application/json');
        if(count($res) > 0) {
            echo json_encode($res[0]);
        } else {
            echo json_encode(array('error'=>'No data found'));
        }
    }

    public function get_table_data()
    {
        //DB column map
        $columns = array(
            'iFeedID' => 'iFeedID',
            'userPhone' => 'user.userPhone',
            'vUserView' => 'vUserView',
            'fAddress' => 'fAddress',
            'tAddress' => 'tAddress',
            'cAddress' => 'cAddress'
        );
        $data = null;
        if( $this->_isLoggedIn() ) {
            $this->load->model('Trip_model');
            $predicate = array(
                'offset' => ($this->input->post('start') >= 0) ? $this->input->post('start') : 0,
                'limit' => ($this->input->post('length') > 0) ? $this->input->post('length') : 100
            );
            $recordsTotal = $this->Trip_model->count($predicate);
            
            $dbColumns = array_values($columns);
            $objKeys = array_keys($columns);

            $res = $this->Trip_model->find($predicate, $dbColumns);
            foreach($res as $i=>$r) { //convert objects to array
                $res[$i] = $this->_objToAry($r, $objKeys);
            }
            $data = array(
                'draw' => $this->input->post('draw'),
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsTotal,
                'data' => &$res
            );
        } else {
            $data = array(
                'draw' => $this->input->post('draw'),
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => [],
                'error' => 'Not authorized'
            );
        }
        $this->output->set_content_type('application/json');
        echo json_encode($data);
    }
}