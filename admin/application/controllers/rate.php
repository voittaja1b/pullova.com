<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Rate extends MY_Controller {
	public function index()
	{
        if( $this->_checkLoginOrRedirect() ) {
            $auth = array(
                'user_fullname'=>$this->session->userdata('SESS_RID_ADMNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_ADMAVATAR')
            );
            $data = array(
                'sideNavViewData' => array(
                    'active'=>'rate',
                    'auth' => $auth
                ),
                'contentPaneView' => 'rate_view',
                'contentPaneViewData' => array(),
                'auth' => $auth
            );
            $this->load->view('template-layout-auth', $data);
        }
    }

    
    public function get_table_data()
    {
        //DB column map
        $columns = array(
            'ID' => 'ID',
            'fBase' => 'fBase',
            'fPerMinute' => 'fPerMinute',
            'fPerMile' => 'fPerMile',
            'fBookingFee' => 'fBookingFee',
            'fMinimumFare' => 'fMinimumFare',
            'vCity' => 'vCity',
            'fAdminFee' => 'fAdminFee',
            'bDefault' => 'bDefault'
        );
        $data = null;
        if( $this->_isLoggedIn() ) {
            $this->load->model('Rate_model');
            $predicate = array(
                'offset' => ($this->input->post('start') >= 0) ? $this->input->post('start') : 0,
                'limit' => ($this->input->post('length') > 0) ? $this->input->post('length') : 100
            );
            $recordsTotal = $this->Rate_model->count($predicate);
            
            $dbColumns = array_values($columns);
            $objKeys = array_keys($columns);

            $res = $this->Rate_model->find($predicate, $dbColumns);
            foreach($res as $i=>$r) { //convert objects to array
                $res[$i] = $this->_objToAry($r, $objKeys);
            }
            $data = array(
                'draw' => $this->input->post('draw'),
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsTotal,
                'data' => &$res
            );
        } else {
            $data = array(
                'draw' => $this->input->post('draw'),
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => [],
                'error' => 'Not authorized'
            );
        }
        $this->output->set_content_type('application/json');
        echo json_encode($data);
    }
}