<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Drivers extends MY_Controller {

	public function index()
	{
        if( $this->_checkLoginOrRedirect() ) {
            $auth = array(
                'user_fullname'=>$this->session->userdata('SESS_RID_ADMNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_ADMAVATAR')
            );      

            $data = array(
                'sideNavViewData' => array(
                    'active'=>'drivers',
                    'auth' => $auth
                ),
                'contentPaneView' => 'drivers_view',
                'contentPaneViewData' => array(
                    'tab' => 'list',
                    'view' => 'list'
                ),
                'auth' => $auth
            );
            $this->load->view('template-layout-auth', $data);
        }
    }

    public function pending()
	{
        if( $this->_checkLoginOrRedirect() ) {
            $auth = array(
                'user_fullname'=>$this->session->userdata('SESS_RID_ADMNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_ADMAVATAR')
            );      

            $data = array(
                'sideNavViewData' => array(
                    'active'=>'drivers',
                    'auth' => $auth
                ),
                'contentPaneView' => 'drivers_view',
                'contentPaneViewData' => array(
                    'tab' => 'pending',
                    'view' => 'list'
                ),
                'auth' => $auth
            );
            $this->load->view('template-layout-auth', $data);
        }
    }

    public function messaging()
	{
        if( $this->_checkLoginOrRedirect() ) {
            $auth = array(
                'user_fullname'=>$this->session->userdata('SESS_RID_ADMNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_ADMAVATAR')
            );      

            $data = array(
                'sideNavViewData' => array(
                    'active'=>'drivers',
                    'auth' => $auth
                ),
                'contentPaneView' => 'drivers_view',
                'contentPaneViewData' => array(
                    'tab' => 'messaging'
                ),
                'auth' => $auth
            );
            $this->load->view('template-layout-auth', $data);
        }
    }

    public function edit_driver()
	{
        if( $this->_checkLoginOrRedirect() ) {
            $auth = array(
                'user_fullname'=>$this->session->userdata('SESS_RID_ADMNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_ADMAVATAR')
            );      
            $data = array(
                'sideNavViewData' => array(
                    'active'=>'drivers',
                    'auth' => $auth
                ),
                'contentPaneView' => 'drivers_view',
                'contentPaneViewData' => array(
                    'tab' => 'list',
                    'view' => 'edit'
                ),
                'auth' => $auth
            );
            $this->load->view('template-layout-auth', $data);
        }
    }

    public function edit_applicant()
	{
        if( $this->_checkLoginOrRedirect() ) {
            $auth = array(
                'user_fullname'=>$this->session->userdata('SESS_RID_ADMNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_ADMAVATAR')
            );      
            $data = array(
                'sideNavViewData' => array(
                    'active'=>'drivers',
                    'auth' => $auth
                ),
                'contentPaneView' => 'drivers_view',
                'contentPaneViewData' => array(
                    'tab' => 'list',
                    'view' => 'edit'
                ),
                'auth' => $auth
            );
            $this->load->view('template-layout-auth', $data);
        }
    }

    public function get_driver_table_data()
    {
        //DB column map
        $columns = array(
            'iUserID' => 'iUserID',
            'vImage' => 'vImage',
            'vFirst' => 'vFirst',
            'vLast' => 'vLast',
            'userPhone' => 'userPhone',
            'driverLicensePlate' => 'driverLicensePlate'
        );
        $data = null;
        if( $this->_isLoggedIn() ) {
            $this->load->model('Driver_model');
            $predicate = array(
                'offset' => ($this->input->post('start') >= 0) ? $this->input->post('start') : 0,
                'limit' => ($this->input->post('length') > 0) ? $this->input->post('length') : 100
            );
            $recordsTotal = $this->Driver_model->count($predicate);
            
            $dbColumns = array_values($columns);
            $objKeys = array_keys($columns);

            $res = $this->Driver_model->find($predicate, $dbColumns);
            foreach($res as $i=>$r) { //convert objects to array
                $res[$i] = $this->_objToAry($r, $objKeys);
            }
            $data = array(
                'draw' => $this->input->post('draw'),
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsTotal,
                'data' => &$res
            );
        } else {
            $data = array(
                'draw' => $this->input->post('draw'),
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => [],
                'error' => 'Not authorized'
            );
        }
        $this->output->set_content_type('application/json');
        echo json_encode($data);
    }

    public function get_applicant_table_data()
    {
        //DB column map
        $columns = array(
            'ApplicantNr' => 'ApplicantNr',
            'imageOfApplicant' => 'imageOfApplicant',
            'FullName' => 'FullName',
            'FullName' => 'FullName',
            'userPhone' => 'Refferalcode',
            'LicensePlateNr' => 'LicensePlateNr'
        );
        $data = null;
        if( $this->_isLoggedIn() ) {
            $this->load->model('Applicant_model');
            $predicate = array(
                'offset' => ($this->input->post('start') >= 0) ? $this->input->post('start') : 0,
                'limit' => ($this->input->post('length') > 0) ? $this->input->post('length') : 100,
                'Status' => 'pending'
            );
            $recordsTotal = $this->Applicant_model->count($predicate);
            
            $dbColumns = array_values($columns);
            $objKeys = array_keys($columns);

            $res = $this->Applicant_model->find($predicate, $dbColumns);
            foreach($res as $i=>$r) { //convert objects to array
                $res[$i] = $this->_objToAry($r, $objKeys);
            }
            $data = array(
                'draw' => $this->input->post('draw'),
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsTotal,
                'data' => &$res
            );
        } else {
            $data = array(
                'draw' => $this->input->post('draw'),
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => [],
                'error' => 'Not authorized'
            );
        }
        $this->output->set_content_type('application/json');
        echo json_encode($data);
    }
}