<form method="POST" action="<?=$this->config->item('base_url')?>login">
    <div class="form hcenter form-horizontal" style="width:40%; margin-top:80px;">
        <div class="paper-material form-group">
            <label class="col-md-3">Username</label>
            <div class="col-md-9">
                <input type="text" name="admin_name" label="" required="true" class="no-polymer" style="width:100%"/>
            </div>
        </div>
        <div class="paper-material form-group">
            <label class="col-md-3">Password</label>
            <div class="col-md-9">
                <input type="password" name="admin_password" label="" required="true" class="no-polymer" style="width:100%"/>
            </div>
        </div>
        <button name="login" class="button color-accent-1 text-uppercase form-group" type="submit">Sign in</button>
    </div>
</form>
