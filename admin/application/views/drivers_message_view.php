<form method="POST" action="<?=$baseUrl?>drivers/">
    <div class="col-sm-5">
        <div class="radio">
            <label>
                <input type="radio" name="to" id="message_to_opt" value="all" checked>
                Message to All Drivers
            </label>
        </div>
    </div>
    <div class="col-sm-7">
        <div class="radio col-sm-6">
            <label>
                <input type="radio" name="to" id="message_to_opt" value="all" checked>
                Message to All Drivers
            </label>
        </div>
        <div class="input-group input-group-sm col-sm-6" style="margin-top:5px">
            <input type="text" class="form-control" placeholder="Enter Driver License Plate" name="message_to_lic_plate">
            <span class="input-group-btn">
                <button class="btn btn-primary" type="button">Go</button>
            </span>
        </div>
    </div>
    <div class="col-sm-12 form-group">
        <textarea class="form-control" rows="4" placeholder="Enter any message"></textarea>
    </div>
    <div class="col-sm-12 text-center form-group">
        <button class="btn btn-primary btn-lg" type="submit" style="min-width:150px">Send</button>
    </div>
</form>