<div>
    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">Ride Details</div>
                <div class="panel-body">
                    <div class="row">
                        <span class="col-sm-5">Feed Title:</span>
                        <span id="trip_feed_title" class="col-sm-7"></span>
                    </div>
                    <div class="row">
                        <span class="col-sm-5">User View:</span>
                        <span id="trip_user_view" class="col-sm-7"></span>
                    </div>
                    <div class="row">
                        <span class="col-sm-5">Description:</span>
                        <span id="trip_description" class="col-sm-7"></span>
                    </div>
                    <div class="row">
                        <span class="col-sm-5">Start Date:</span>
                        <span id="trip_start_date" class="col-sm-7"></span>
                    </div>
                    <div class="row">
                        <span class="col-sm-5">Cost:</span>
                        <span id="trip_cost" class="col-sm-7"></span>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-default">
                <div class="panel-heading">Help Note</div>
                <div class="panel-body">
                    <div class="col-sm-12 no-padding">
                        <div style="height:10px;width:10px;background:#7BC33F;display:inline-block;margin-right: 10px;"></div>
                        A green pointer represents Start Point
                    </div>
                    <div class="col-sm-12 no-padding">
                        <div style="height:10px;width:10px;background:#F76C60;display:inline-block;margin-right: 10px;"></div>
                        B red pointer represents End Point
                    </div>
                    <div class="col-sm-12 no-padding">
                        <div style="height:10px;width:10px;background:#4E95FF;display:inline-block;margin-right: 10px;"></div>
                        Car blue pointer represents Current Point
                    </div>
                    <div class="col-sm-12 no-padding">
                        <button class="btn btn-default" onclick="calculateDistances()">Driver ETA?</button>
                        <span id="trip_eta"></span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div id="trip_map" class="col-sm-12 no-padding" style="border:1px solid #ddd">
        </div> 
    </div>
</div>