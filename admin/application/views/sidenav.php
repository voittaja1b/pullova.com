<?php
    $baseUrl = $this->config->item('base_url');
    $assetsUrl = $this->config->item('assets_url');
    $auth = isset($auth) ? $auth : array( 'user_fullname'=>'', 'user_avatar'=>'' );
?>
<div class="sidenav">
    <div class="text-center" style="padding: 30px">
        <div class="avatar hcenter" style="background-image:url('<?=$auth['user_avatar']?>')"></div>
        <span><?=$auth['user_fullname']?></span>
    </div>
    <ul class="list-group">
        <li class="list-group-item <?= $active=='dashboard' ? 'active' : ''?>" ><a href="<?=$baseUrl?>dashboard">Dashboard</a></li>
        <li class="list-group-item <?= $active=='trips' ? 'active' : ''?>" ><a href="<?=$baseUrl?>trips">Trips</a></li>
        <li class="list-group-item <?= $active=='drivers' ? 'active' : ''?>" ><a href="<?=$baseUrl?>drivers">Drivers</a></li>
        <li class="list-group-item <?= $active=='riders' ? 'active' : ''?>" ><a href="<?=$baseUrl?>riders">Riders</a></li>
        <li class="list-group-item <?= $active=='promocode' ? 'active' : ''?>" ><a href="<?=$baseUrl?>promocode">Promo Code</a></li>
        <li class="list-group-item <?= $active=='rate' ? 'active' : ''?>" ><a href="<?=$baseUrl?>rate">Rate</a></li>
        <li class="list-group-item <?= $active=='payment' ? 'active' : ''?>" ><a href="<?=$baseUrl?>payment">Payment</a></li>
    </ul>
</div>