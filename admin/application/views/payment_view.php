<div class="content-pane">
    <div class="pane-header abs-dock">
        <h3 class="text-uppercase">Payment</h3>
    </div>
    <div class="pane-body">
        <div class="nav-pill-wrapper">
            <div class="col-md-3 nav-pill active">Unpaid</div>
            <div class="col-md-3 nav-pill">Paid</div>
            <div class="col-md-6 nav-pill"></div>
        </div>
        <table id="data_table" class="table table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Phone #</th>
                <th>Balance</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
    </div>
</div>

<script>
function initDataTable() {
    if($('#data_table').length > 0) {
        $.fn.dataTable.ext.errMode = 'none';
        $('#data_table').DataTable( {
            "ajax" : '<?=$baseUrl?>payment/get_table_data',
            "columnDefs": [ //will use the first column as id column
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                    "sortable": false,
                },
                {
                    "targets": [ 5],
                    "searchable": false,
                    "sortable": false,
                    "data": null,
                    "render": function( data, type, row, meta ) {
                        return '<a class="btn btn-large btn-success text-uppercase" href="<?=$baseUrl?>payment/pay/'+data[0]+'">Pay</a>';
                    }
                }
            ],
            "scrollX": true
        });
    }
}
</script>