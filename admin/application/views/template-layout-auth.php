<!DOCTYPE encoding="utf8">
<html>
<?php
    $baseUrl = $this->config->item('base_url');
    $rootUrl = $this->config->item('root_url');
    $assetsUrl = $this->config->item('assets_url');
    $bowerUrl = $this->config->item('bower_dep_url');
    $auth = isset($auth) ? $auth : array( 'user_fullname'=>'', 'user_avatar'=>'' );
?>
<head>
    <title>Pullova</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?=$assetsUrl?>styles/bootstrap.custom.css">
    <link rel="stylesheet" href="<?=$assetsUrl?>styles/style.css">
    <link rel="stylesheet" href="<?=$bowerUrl?>datatables.net-bs/css/dataTables.bootstrap.min.css">

    <link rel="import" href="<?=$bowerUrl?>polymer/polymer.html">
    <link rel="import" href="<?=$bowerUrl?>iron-icons/iron-icons.html">
    <link rel="import" href="<?=$bowerUrl?>iron-icon/iron-icon.html">
    <link rel="import" href="<?=$bowerUrl?>paper-toast/paper-toast.html">
    <style is="custom-style">
        .warn {
            --paper-toast-background-color: red;
            --paper-toast-color: white;
        }
    </style>
</head>
<body>
    <div class="header color-1">
        <nav class="navbar navbar-inverse no-margin">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand no-padding" href="#">
                        <img src="<?=$assetsUrl?>images/logo.png" class="pull-left" style="margin:0 5px;"/>
                        <img src="<?=$assetsUrl?>images/logo-text.png" style="margin:2px 5px;" class="pull-left"/>
                    </a>
                </div>
                <ul class="nav navbar-nav">
                </ul>
                <div class="nav navbar-nav navbar-right">
                    <li><a class="color-1">Welcome, <?=$auth['user_fullname']?></a></li>
                    <li><img src="<?=$auth['user_avatar']?>" class="avatar"/></li>
                </div>
            </div><!-- /.container -->
        </nav>
    </div>
    <div class="steps-wrapper text-center">
        <div class="step-indicator done"></div>
        <div class="step-indicator done"></div>
        <div class="step-indicator"></div>
        <div class="step-indicator"></div>
    </div>
    <div class="container">
        <div class="content">
            <?php
            $this->view('sidenav', $sideNavViewData);
            $this->view($contentPaneView, $contentPaneViewData);
            ?>
        </div>
    </div>
    <div class="footer color-1">
        <nav>
            <a href="<?=$rootUrl?>" class="text-uppercase">Home</a>
            <a href="<?=$rootUrl?>about_us" class="text-uppercase">About us</a>
            <a href="<?=$rootUrl?>faq" class="text-uppercase">FAQ</a>
            <a href="<?=$rootUrl?>contact_us" class="text-uppercase">Contact us</a>
        </nav>
        <div class="copyright">Copyright 2016 &copy; Pullova LLC.</div>
    </div>

    <!-- Scripts to import -->
    <script src="<?=$bowerUrl?>webcomponentsjs/webcomponents-lite.min.js"></script>
    <script src="<?=$bowerUrl?>jquery/dist/jquery.min.js"></script>
    <script src="<?=$bowerUrl?>bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="<?=$bowerUrl?>datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="<?=$bowerUrl?>datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
        $(document).ready(function() {
            if(typeof(initDataTable) === 'function') {
                initDataTable();
            }
        } );
    </script>
</body>
</html>
