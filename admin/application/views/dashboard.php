<?php
$this->load->helper('dec_number');
$summary = isset($summary) ? $summary : array( 'totalEarnings'=>0, 'totalProfits'=>0, 'totalDrivers'=>0, 'totalRides'=>0 );
?>
<div class="content-pane">
    <div class="pane-header">
        <h3>Dashboard</h3>
    </div>
    <div class="pane-body">
        <div class="filterbox underlined">
            <div class="col-md-2 col-md-offset-10">
                <div class="dropdown">
                    <?php
                        $periodFltValues = array(
                            'all' => 'All Time',
                            'day' => 'Daily',
                            'week' => 'Weekly',
                            'month' => 'Monthly'
                        );
                    ?>
                    <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <?=$periodFltValues[$period]?>
                        <span class="caret pull-right"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                        <?php foreach($periodFltValues as $pfv => $pfl) {
                            echo '<li><a href="' . $baseUrl . 'dashboard?period=' . $pfv . '">' . $pfl . '</a></li>';
                        }?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-md-3">
                <h1 class="text-accent-1">$<?=dec_k_format($summary['totalEarnings'])?></h1>
                <p class="text-gray">Total Earnings</p>
            </div>
            <div class="col-md-3">
                <h1>$<?=dec_k_format($summary['totalProfits'])?></h1>
                <p class="text-gray">Total Profit</p>
            </div>
            <div class="col-md-3">
                <h1><?=dec_k_format($summary['totalDrivers'])?></h1>
                <p class="text-gray">Total Drivers</p>
            </div>
            <div class="col-md-3">
                <h1><?=dec_k_format($summary['totalRides'])?></h1>
                <p class="text-gray">Total Rides</p>
            </div>
        </div>
    </div>
</div>