<?php
$baseUrl = $this->config->item('base_url');
$tab = isset($tab) ? $tab : 'list';
$view = isset($view) ? $view : 'list';
$driverDetail = isset($driverDetail) ? $driverDetail : array();

$imgUpUrl = $this->config->item('img_upload_url');
?>
<div class="content-pane">
    <div class="pane-header abs-dock">
        <h3 class="text-uppercase">Drivers</h3>
        <a class="pull-right abs" style="top:30px">Add Drivers <iron-icon icon="icons:add" style="color:#ff1065"></iron-icon></a>
    </div>
    <div class="pane-body">
        <div class="nav-pill-wrapper">
            <a href="<?=$baseUrl?>drivers"><div class="col-md-3 nav-pill <?=$tab=='list' ? 'active':''?>">List</div></a>
            <a href="<?=$baseUrl?>drivers/pending"><div class="col-md-3 nav-pill <?=$tab=='pending' ? 'active':''?>">Pending</div></a>
            <a href="<?=$baseUrl?>drivers/messaging"><div class="col-md-3 nav-pill <?=$tab=='messaging' ? 'active':''?>">Message</div></a>
            <div class="col-md-3 nav-pill">
                <?php if(($tab == 'list' && $view == 'list') || ($tab == 'pending' && $view == 'list')){ ?>
                <div class="input-group input-group-sm">
                    <input type="text" class="form-control" placeholder="Search for...">
                    <span class="input-group-btn">
                        <button class="btn btn-primary" type="button">Go</button>
                    </span>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php
        if($tab == 'list' && $view == 'list') {
            include 'drivers_list_view.php';
        } elseif($tab == 'pending' && $view == 'list') {
            include 'drivers_pending_view.php';
        } elseif($tab == 'list' && $view == 'edit') {
            include 'drivers_edit_view.php';
        } elseif($tab == 'messaging') {
            include 'drivers_message_view.php';
        }
        ?>
    </div>
</div>