<table id="data_table" class="table table-hover" cellspacing="0" width="100%">
<thead>
    <tr>
        <th>Id</th>
        <th></th>
        <th>First Name</th>
        <th>Last Name</th>
        <th>Phone #</th>
        <th>License Plate</th>
        <th>Actions</th>
    </tr>
</thead>
<tbody>
</tbody>
</table>

<script>
function initDataTable() {
    if($('#data_table').length > 0) {
        /* Set the defaults for DataTables initialisation */
        $.extend( true, $.fn.dataTable.defaults, {
            dom:"<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-2'l><'col-sm-3 text-right'i><'col-sm-7'p>>",
            renderer: 'bootstrap'
        } );
        $.fn.dataTable.ext.errMode = 'none';
        
        $('#data_table').DataTable( {
            "ajax" : '<?=$baseUrl?>drivers/get_applicant_table_data',
            "columnDefs": [ //will use the first column as id column
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ 1 ],
                    "searchable": false,
                    "sortable": false,
                    "render": function( data, type, row, meta ) {
                        return '<img src="' + data + '" class="avatar-small"/>';
                    }
                },
                {
                    "targets": [ 6 ],
                    "searchable": false,
                    "sortable": false,
                    "data": null,
                    "render": function( data, type, row, meta ) {
                        return '<a href="<?=$baseUrl?>drivers/edit_applicant/'+data[0]+'"><iron-icon icon="icons:create"></iron-icon></a>'
                         + '<a href="#"><iron-icon icon="icons:check" style="color:green"></iron-icon></a>'
                         + '<a href="#"><iron-icon icon="icons:clear" style="color:red"></iron-icon></a>';
                    }
                }
            ]
        });
    }
}
</script>