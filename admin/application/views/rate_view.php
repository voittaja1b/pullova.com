<div class="content-pane">
    <div class="pane-header abs-dock">
        <h3 class="text-uppercase">Rate</h3>
    </div>
    <div class="pane-body">
        <div class="nav-pill-wrapper">
            <div class="col-md-3 nav-pill active">Current Rates</div>
            <div class="col-md-9 nav-pill"></div>
        </div>
        <table id="data_table" class="table table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Base Fare</th>
                <th>Per Minute</th>
                <th>Per Mile</th>
                <th>Booking Fee</th>
                <th>Minimum Fare</th>
                <th>City</th>
                <th>Admin Fee</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
    </div>
</div>

<script>
function initDataTable() {
    if($('#data_table').length > 0) {
        /* Set the defaults for DataTables initialisation */
        $.extend( true, $.fn.dataTable.defaults, {
            dom:"<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-2'l><'col-sm-3 text-right'i><'col-sm-7'p>>",
            renderer: 'bootstrap'
        } );
        $.fn.dataTable.ext.errMode = 'none';
        
        $('#data_table').DataTable( {
            "ajax" : '<?=$baseUrl?>rate/get_table_data',
            "columnDefs": [ //will use the first column as id column
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false,
                    "sortable": false,
                },
                {
                    "targets": [ 8 ],
                    "searchable": false,
                    "sortable": false,
                    "data": null,
                    "render": function( data, type, row, meta ) {
                        var domContent = '<a href="<?=$baseUrl?>rate/view_rate/'+data[0]+'"><iron-icon icon="icons:create"></iron-icon></a>';
                        if(data['8'] == '0') {
                            domContent = domContent + '<a href="<?=$baseUrl?>rate/delete_rate/'+data[0]+'"><iron-icon icon="icons:clear" style="color:red"></iron-icon></a>';
                        }
                        return domContent;
                    }
                }
            ],
            "scrollX": true
        });
    }
}
</script>