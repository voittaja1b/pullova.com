<!DOCTYPE html>
<html>
<?php
    $baseUrl = $this->config->item('base_url');
    $rootUrl = $this->config->item('root_url');
    $assetsUrl = $this->config->item('assets_url');
    $bowerUrl = $this->config->item('bower_dep_url');
?>
<head>
    <title>Pullova</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?=$assetsUrl?>styles/bootstrap.custom.css">
    <link rel="stylesheet" href="<?=$assetsUrl?>styles/style.css">

    <link rel="import" href="<?=$bowerUrl?>polymer/polymer.html">
    <link rel="import" href="<?=$bowerUrl?>paper-toast/paper-toast.html">
    <link rel="import" href="<?=$bowerUrl?>paper-card/paper-card.html">
    <link rel="import" href="<?=$bowerUrl?>gold-email-input/gold-email-input.html">
    <link rel="import" href="<?=$bowerUrl?>gold-phone-input/gold-phone-input.html">
    <style is="custom-style">
        .warn {
            --paper-toast-background-color: red;
            --paper-toast-color: white;
        }
    </style>
</head>

<body>
    <div class="header color-1">
        <nav class="navbar navbar-inverse no-margin">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand no-padding" href="<?=$rootUrl?>">
                        <img src="<?=$assetsUrl?>images/logo.png" class="pull-left" style="margin:0 5px;"/>
                        <img src="<?=$assetsUrl?>images/logo-text.png" style="margin:2px 5px;" class="pull-left"/>
                    </a>
                </div>
            </div><!-- /.container -->
        </nav>
    </div>
    <div class="container content">
        <?php
            $this->view('toasts', array('toasts' => isset($toasts) ? $toasts : array()));
            $this->view($contentPaneView, $contentPaneViewData);
        ?>
    </div>
    <div class="footer color-1">
        <nav>
            <a href="<?=$rootUrl?>" class="text-uppercase">Home</a>
            <a href="<?=$rootUrl?>about_us" class="text-uppercase">About us</a>
            <a href="<?=$rootUrl?>faq" class="text-uppercase">FAQ</a>
            <a href="<?=$rootUrl?>contact_us" class="text-uppercase">Contact us</a>
        </nav>
        <div class="copyright">Copyright 2016 &copy; Pullova LLC.</div>
    </div>

    <!-- Scripts to import -->
    <script src="<?=$bowerUrl?>webcomponentsjs/webcomponents-lite.min.js"></script>
</body>
</html>
