<?php
$baseUrl = $this->config->item('base_url');
$trips = isset($trips) ? $trips : array();
?>
<div class="content-pane">
    <div class="pane-header">
        <h3 class="text-uppercase">Trips</h3>
    </div>
    <div class="pane-body">
        <table id="data_table" class="table table-hover" width="100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Phone #</th>
                <th>Status</th>
                <th>From Address</th>
                <th>To Address</th>
                <th>Current Address</th>
                <th>View</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
    </div>
</div>

<div class="modal fade" tabindex="-1" role="dialog" id="trip_details_modal" data-trip-id="" data-backdrop="static">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title">Trip Details</h4>
      </div>
      <div class="modal-body">
        <?php include('trip_detail_view.php'); ?>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script>
//global variables
var tripDetailData = null,
    tripMap = null,
    tripMapMarkers = null,
    tripMapDirectionsDisplay = null,
    tripMapDirectionsService = null,
    tripMapDistanceMatrixService = null;

function showTripDetail(id) {
    $('#trip_details_modal').data('trip-id', id);
    $('#trip_details_modal').modal('show');
}

function calculateDistances() {
    window.tripMapDistanceMatrixService = window.tripMapDistanceMatrixService || (new google.maps.DistanceMatrixService());
    var service = window.tripMapDistanceMatrixService;
    service.getDistanceMatrix(
    {
        origins: [
            new google.maps.LatLng((tripDetailData.currentLat || 0)  + "," + (tripDetailData.currentLong || 0))
        ],
        destinations: [
            new google.maps.LatLng((tripDetailData.toLat || 0)  + "," + (tripDetailData.toLong || 0))
        ],
        travelMode: google.maps.TravelMode.DRIVING,
        unitSystem: google.maps.UnitSystem.IMPERIAL,
        avoidHighways: false,
        avoidTolls: false
    }, function(response, status) {
        if (status == google.maps.DistanceMatrixStatus.OK) {
            var origins = response.originAddresses || [];
            var destinations = response.destinationAddresses || [];
            var rows = response.rows || [];

            for (var i = 0; i < origins.length; i++) {
                var results = rows[i].elements || [];
                if(results[0] && results[0].status == google.maps.DistanceMatrixStatus.OK) {
                    $('#trip_eta').text( results[0].distance.text + 'les arriving in ' + results[0].duration.text + '<br>' );
                } else {
                    $('#trip_eta').text( 'Not Available' );
                }
            }
        } else {

        }
    });
}

function initTripDetailModal() {
    $('#trip_details_modal').on('show.bs.modal', function (e) {
        function setDetailView(data) {
            window.tripDetailData = data;
            $('#trip_feed_title').text(data.vFeedTitle || '');
            $('#trip_user_view').text(data.vUserView || '');
            $('#trip_description').text(data.tFeedDescription || '');
            $('#trip_start_date').text(data.vStartDate || '');
            $('#trip_cost').text(data.vCost || '');
        }

        function initMap() {
            var mapOptions = {
                zoom: 4,
                mapTypeControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            //create the map, and place it in the HTML map div
            window.tripMap = window.tripMap || (new google.maps.Map(document.getElementById("trip_map"), mapOptions));
            window.tripMapMarkers = window.tripMapMarkers || [];
            window.tripMapDirectionsDisplay = new google.maps.DirectionsRenderer();
            window.tripMapDirectionsService = new google.maps.DirectionsService();
            //clear markers
            while(window.tripMapMarkers.length) {
                var marker = window.tripMapMarkers[0];
                marker.setMap(null);
                window.tripMapMarkers.splice(0,1);
            }
        }

        function setMap(data) {
            var map = window.tripMap;
            var markers = window.tripMapMarkers;
            var directionsDisplay = window.tripMapDirectionsDisplay;
            var directionsService = window.tripMapDirectionsService;

            data.currentLat = (data.currentLat || 0);
            data.currentLong = (data.currentLong || 0);

            //place the initial marker
            var marker = new google.maps.Marker({
                position: new google.maps.LatLng(data.currentLat, data.currentLong),
                map: map,
                title: "Driver Current location!",
                icon: '<?php echo $global_config["SiteGlobalPath"]; ?>images/marker_green.png'
            });
            markers.push(marker);

            var infowindow = new google.maps.InfoWindow({
                content: 'Driver Current location:<br/>LatLng:' + data.currentLat + ","  + data.currentLong
            });

            // Source To Destionation
            var request = {
                origin: new google.maps.LatLng(data.currentLat + "," + data.currentLong),
                destination: new google.maps.LatLng(data.currentLat + ", " + data.currentLong),
                travelMode: google.maps.DirectionsTravelMode.DRIVING
            };
            directionsService.route(request, function(response, status) {
                if (status == google.maps.DirectionsStatus.OK) {
                    directionsDisplay.setDirections(response);
                }
            });
            google.maps.event.addListener(marker, 'click', function () {
                // Calling the open method of the infoWindow 
                infowindow.open(map, marker);
            });
            directionsDisplay.setMap(map);
        }

        // init dialog contents
        setDetailView({});
        initMap();
        // get more trip detail from the server
        $.ajax({
            url: '<?=$baseUrl?>trips/get_detail/' + $('#trip_details_modal').data('trip-id'),
            success: function(resp) {
                // set the dialog contents
                setDetailView(resp);
                setMap(resp);
            },
            dataType: 'json'
        });
    });
}

function initDataTable() {
    if($('#data_table').length > 0) {
        /* Set the defaults for DataTables initialisation */
        $.extend( true, $.fn.dataTable.defaults, {
            dom:"<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-2'l><'col-sm-3 text-right'i><'col-sm-7'p>>",
            renderer: 'bootstrap'
        } );

        $.fn.dataTable.ext.errMode = 'none';
        $('#data_table').DataTable( {
            "ajax" : '<?=$baseUrl?>trips/get_table_data',
            "columnDefs": [ //will use the first column as id column
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ 6 ],
                    "searchable": false,
                    "sortable": false,
                    "data": null,
                    "render": function( data, type, row, meta ) {
                        return '<a href="javascript: showTripDetail(' + data[0] + ')"><iron-icon icon="icons:visibility"></iron-icon></a>';
                    }
                }
            ]
        });

        initTripDetailModal();
    }
}
</script>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script></script>