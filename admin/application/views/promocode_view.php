<div class="content-pane">
    <div class="pane-header abs-dock">
        <h3 class="text-uppercase">Promo Code</h3>
    </div>
    <div class="pane-body">
        <div class="nav-pill-wrapper">
            <a href="<?=$baseUrl?>promocode"><div class="col-md-3 nav-pill active">List</div></a>
            <a href="<?=$baseUrl?>promocode/edit/new"><div class="col-md-3 nav-pill">Create</div></a>
            <div class="col-md-6 nav-pill"></div>
        </div>
        <table id="data_table" class="table table-hover" cellspacing="0" width="100%">
        <thead>
            <tr>
                <th>Title</th>
                <th>Expiration Date</th>
                <th>Code</th>
                <th>Discount</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
    </div>
</div>

<script>
function initDataTable() {
    if($('#data_table').length > 0) {
        /* Set the defaults for DataTables initialisation */
        $.extend( true, $.fn.dataTable.defaults, {
            dom:"<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-2'l><'col-sm-3 text-right'i><'col-sm-7'p>>",
            renderer: 'bootstrap'
        } );
        $.fn.dataTable.ext.errMode = 'none';

        $('#data_table').DataTable( {
            "ajax" : '<?=$baseUrl?>promocode/get_table_data',
            "columnDefs": [ //will use the first column as id column
                {
                    "targets": [ 3 ],
                    "render": function( data, type, row, meta ) {
                        return data + '%';
                    }
                },
                {
                    "targets": [ 4 ],
                    "searchable": false,
                    "sortable": false,
                    "data": null,
                    "render": function( data, type, row, meta ) {
                        return '<a href="<?=$baseUrl?>promocode/view_promocode/'+data[0]+'"><iron-icon icon="icons:create"></iron-icon></a>'
                         + '<a href="<?=$baseUrl?>promocode/delete_driver/'+data[0]+'"><iron-icon icon="icons:clear" style="color:red"></iron-icon></a>';
                    }
                }
            ]
        });
    }
}
</script>