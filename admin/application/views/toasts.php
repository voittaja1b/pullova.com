<?php
if(isset($toasts) && count($toasts)) {
    foreach($toasts as $f) {
        echo '<paper-toast class="fit-bottom ' .$f['type']. '" text="' . $f['message'] . '" opened></paper-toast>';
    }
}