<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Summary_model extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Try to get app usage state
     * @param $period :string all | day | week | month
     * @return array(
     *    'totalEarnings' => , 
     *    'totalProfits' => ,
     *    'totalDrivers' => ,
     *    'totalRides' => ,
     * )
     */
    function getProfitSummary($period = 'all') {
        $ret = array();
        
        // Count all Drivers
        $query = $this->db
            ->select('COUNT(*) as TotalCount')
            ->from('tbl_user')
            ->where('vDriverorNot', 'driver')
            ->limit(1)
            ->get();
        $res = $query->result() [0];
        $ret['totalDrivers'] = $res->TotalCount;

        // Count total Earnings, Profits, totalRides
        $query = $this->db
            ->select('SUM(vCost) as TotalEarnings, COUNT(*) as TotalRides')
            ->from('tbl_feed');
        switch($period) {
            case 'day':
                $query->where('dCreatedDate >= now() - INTERVAL 1 DAY ');
            case 'week':
                $query->where('dCreatedDate >= now() - INTERVAL 7 DAY ');
            case 'month':
                $query->where('dCreatedDate >= now() - INTERVAL 30 DAY ');
        }
        $query = $query->limit(1)->get();
        $res = $query->result() [0];
        $ret['totalEarnings'] = $res->TotalEarnings;
        $ret['totalProfits'] = $res->TotalEarnings * 0.1;
        $ret['totalRides'] = $res->TotalRides;

        return $ret;
    }
}