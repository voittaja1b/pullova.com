<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Promocode_model extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function _getQuery($predicate, $columns=[ 'vCode' ]) {
        $query = $this->db
            ->select(implode(',', $columns))
            ->from('tbl_promo_code')
            ->order_by('dCreateDate desc');
        $allowedPredicates = array('vCode');
        foreach($allowedPredicates as $p) {
            if(isset($predicate[$p])) {
                $query = $query->where($p, $predicate[$p]);
            }
        }
        return $query;
    }

    function count($predicate) {
        $query = $this->_getQuery($predicate);
        return $query->count_all_results();
    }

    /**
     * Try to get list of promo-codes
     * @param $predicate
     * @return array of promo-codes
     */
    function find($predicate, $columns=[ 'vCode' ]) {
        $query = $this->_getQuery($predicate, $columns);
        if(isset($predicate['limit']) && $predicate['limit']>0) {
            $query = $query->get(null, $predicate['limit'], isset($predicate['offset']) ? $predicate['offset'] : 0);
        } else {
            $query = $query->get();
        }
        $res = $query->result();

        return $res;
    }
}