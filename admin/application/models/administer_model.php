<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Administer_model extends CI_Model {

    var $title   = '';
    var $content = '';
    var $date    = '';

    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Try authenticate an admin user by username and password
     * @return admin user record if successful, false if not successful
     */
    function doAuth($username, $password) {
        $hashedPassword = $password; // !BEWARE: raw password is being saved on DB. The original dev did this.
        $query = $this->db
            ->select('admin_id, admin_name, admin_email, admin_password, admin_role, admin_status')
            ->from('tbl_admin')
            ->where('admin_name', $username)
            ->where('admin_password', $hashedPassword)
            ->where('admin_status', 1)
            ->limit(1)
            ->get();
        if ($query->num_rows() == 1) {
            return $query->result() [0];
        } else {
            return false;
        }
    }

}