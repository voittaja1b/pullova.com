<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Trip_model extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    function geolocateAddress($lat,$lng)
	{
		$url = 'http://maps.googleapis.com/maps/api/geocode/json?latlng='.trim($lat).','.trim($lng).'&sensor=false';
		$json = @file_get_contents($url);
		$data=json_decode($json);
		$status = $data->status;
		if($status=="OK")
			return $data->results[0]->formatted_address;
		else
			return false;
	}

    function updateAddresses() {
        $query = $this->db
            ->select('iFeedID,fLat,fLong,fAddress,toLat,toLong,tAddress,currentLat,currentLong,cAddress')
            ->from('tbl_feed')
            ->where("`fAddress` = '' OR `tAddress` = '' OR `cAddress` = ''")
            ->limit(10)
            ->get();
        if($query->num_rows() > 0) {
            $res = $query->result();
            foreach($res as &$r) {
                if($r->fLat && $r->fLong) {
                    $address = $this->geolocateAddress($r->fLat, $r->fLong); 
                    if ($address) {
                        $this->db
                            ->where('iFeedID', $r->iFeedID)
                            ->update('tbl_feed', array('fAddress'=>$address));
                    } else {
                        $this->db
                            ->where('iFeedID', $r->iFeedID)
                            ->update('tbl_feed', array('validAddress'=>'No'));
                    }
                }
                
                if($r->toLat && $r->toLong) {
                    $address1 = $this->geolocateAddress($r->toLat, $r->toLong); 
                    if($address1) {
                        $this->db
                            ->where('iFeedID', $r->iFeedID)
                            ->update('tbl_feed', array('tAddress'=>$address1));
                    } else {
                        $this->db
                            ->where('iFeedID', $r->iFeedID)
                            ->update('tbl_feed', array('validAddress'=>'No'));
                    }
                }
                
                if($r->currentLat && $r->currentLong) {
                    $address2 = $this->geolocateAddress($r->currentLat, $r->currentLong); 
                    if($address2) {
                        $this->db
                            ->where('iFeedID', $r->iFeedID)
                            ->update('tbl_feed', array('cAddress'=>$address2));
                    } else {
                        $this->db
                            ->where('iFeedID', $r->iFeedID)
                            ->update('tbl_feed', array('validAddress'=>'No'));
                    }
                }
            }
        }
    }

    function _getQuery($predicate, $columns=[ 'iFeedID' ]) {
        $query = $this->db
            ->select(implode(',', $columns))
            ->from('tbl_feed')
            ->join('tbl_user as user', 'tbl_feed.iUserID = user.iUserID', 'left')
            ->join('tbl_user as driver', 'tbl_feed.iDriverID = driver.iUserID', 'left')
            ->order_by('dCreatedDate desc');
        $allowedPredicates = array('iFeedID', 'iDriverID');
        foreach($allowedPredicates as $p) {
            if(isset($predicate[$p])) {
                $query = $query->where($p, $predicate[$p]);
            }
        }
        return $query;
    }

    function count($predicate) {
        $query = $this->_getQuery($predicate);
        return $query->count_all_results();
    }

    /**
     * Try to get list of trips
     * @param $predicate
     * @return array of trips
     */
    function find($predicate, $columns=[ 'iFeedID' ]) {
//        $this->updateAddresses();
        
        $query = $this->_getQuery($predicate, $columns);
        if(isset($predicate['limit']) && $predicate['limit']>0) {
            $query = $query->get(null, $predicate['limit'], isset($predicate['offset']) ? $predicate['offset'] : 0);
        } else {
            $query = $query->get();
        }
        $res = $query->result();

        return $res;
    }
}