<?php
$lang['STATUS_NOT_CHANGED'] = "Problem changing status, Please try again.";

$lang['EMAIL_ADDRESS_NOT_FOUND'] = "No user exists with this email. ";
$lang['PASSWORD_NOT_SENT'] = "Problem in sending password, Please try again.";
$lang['OLD_PASSWORD_NOT_OK'] = "Old Password you have entered is not correct.";
$lang['PASSWORD_NOT_CHANGED'] = "Problem changing password, Please try again.";
$lang['SETTINGS_NOT_CHANGED'] = "Problem changing Settings, Please try again.";
$lang['SETTING_NOT_FOUND'] = "Settings detail not found.";
$lang['PROFILE_NOT_EDITED'] = "Problem editing profile, Please try again.";
$lang['PROFILE_NOT_FOUND'] = "Profile detail not found.";
$lang['ADMIN_EXISTS'] = "Admin already exists with this email.";
$lang['TEMPLATE_STATUS_NOT_CHANGE'] = "Problem changing status, Please try again.";
$lang['TEMPLATE_NOT_DELETED'] = "Problem removing template Please try again.";
$lang['TEMPLATE_NOT_ADDED'] = "Problem adding template, Please try again.";
$lang['TEMPLATE_NOT_EDITED'] = "Problem editing template, Please try again.";
$lang['PASSWORD_SAME_OR_UNKNOWN_ERROR'] = "Your new password is same as old one, please use different password";
$lang['USER_STATUS_NOT_CHANGE'] = "Problem changing status, Please try again.";
$lang['USER_NOT_DELETED'] = "Problem removing User Please try again.";
$lang['USER_NOT_ADDED'] = "Problem adding User, Please try again.";
$lang['USER_NOT_EDITED'] = "Problem editing User, Please try again.";
$lang['USERNAME_EXISTS'] = "Username already exists.";
$lang['USER_MAIL_NOT_SENT'] = "Problem in sending mail to user";
$lang['USER_EXISTS'] = "User already exists with this title.";
$lang['IMAGE_REQUIRED'] = "Please select a image";

$lang['LOGIN_ERROR'] = "Wrong user name or password.";


/* ********************************************************** 
 * ADMIN
 ********************************************************** */

$lang['ADMIN_STATUS_NOT_CHANGE'] = "Problem changing status, Please try again.";
$lang['ADMIN_NOT_DELETED'] = "Problem removing admin, Please try again.";
$lang['ADMIN_NOT_ADDED'] = "Problem adding Admin, Please try again.";
$lang['ADMIN_EXISTS'] = "Admin already exists with this email.";
$lang['ADMIN_NOT_EDITED'] = "Problem editing Admin, Please try again.";
