<?php
$lang['STATUS_CHANGED'] = "Status successfully changed.";


$lang['PASSWORD_SENT'] = "Reset password link send to your mail, Please reset your password.";
$lang['PASSWORD_CHANGED'] = "Your Password changed successfully.";
$lang['SETTINGS_CHANGED'] = "Settings Changed successfully.";
$lang['PROFILE_EDITED'] = "Profile Edited successfully.";
$lang['TEMPLATE_DELETED'] = "Template Removed successfully.";
$lang['TEMPLATE_ADDED'] = "Template Added successfully.";
$lang['TEMPLATE_EDITED'] = "Template Edited successfully.";
$lang['USER_STATUS_CHANGE'] = "User status change successfully.";
$lang['USER_ADDED'] = "User Added successfully.";
$lang['USER_EDITED'] = "User Edited successfully.";
$lang['USER_DELETED'] = "User Removed successfully.";


/* ********************************************************** 
ADMIN
 ********************************************************** */

$lang['ADMIN_STATUS_CHANGE'] = "Admin status change successfully.";
$lang['ADMIN_DELETED'] = "Admin Removed successfully.";
$lang['ADMIN_ADDED'] = "Admin Added successfully.";
$lang['ADMIN_EDITED'] = "Admin Edited successfully.";

