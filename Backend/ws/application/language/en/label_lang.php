<?php
$lang['HOME'] = 'Home';
$lang['LOGO'] = 'Gran Canaria';
$lang['LOGIN'] = 'Login';
$lang['LOGOUT'] = 'Logout';
$lang['FORGOT_PASSWORD'] = 'Forgot Password';
$lang['RESET_PASSWORD'] = 'Reset Password';
$lang['DASHBOARD'] = 'Dashboard';
$lang['ADMIN_MANAGMENT'] = 'Admin Management';
$lang['USER_MANAGMENT'] = 'User Management';
$lang['SETTING'] = 'Site Setting';
$lang['ADDRESS'] = 'Address';
$lang['COUNTRY'] = 'Country';
$lang['STATE'] = 'State';
$lang['POST_CODE'] = 'Postcode';
$lang['PHONE_NUMBER'] = 'Phone Number';
$lang['STATUS'] = 'Status';
$lang['TYPE'] = 'Type';
$lang['FORGOT_PASSWORD'] = 'Forgot password?';
$lang['REMEMBER_ME'] = 'Remember me';
$lang['LBL_WEBSITE_URL'] = 'Website URL';
$lang['LBL_MAIN'] = 'Main';
$lang['LBL_PAYPAL'] = 'Paypal';
$lang['LBL_TITLE'] = 'Title';
$lang['LIST'] = 'List';
$lang['DASHBOARD'] = 'Dashboard';
$lang['PROFILE'] = 'Profile';
$lang['ADD'] = 'Add';
$lang['EDIT'] = 'Edit';
$lang['CANCEL'] = 'Cancel';
$lang['DELETE'] = 'Delete';
$lang['FIRST_NAME'] = "First Name";
$lang['LAST_NAME'] = "Last Name";
$lang['EMAIL_ADDRESS'] = "Email";
$lang['PASSWORD'] = "Password";
$lang['ACTION'] = "Action";

$lang['ADMIN_LOGIN_PAGE'] = 'WiZit Login';
$lang['ENTER_EMAIL_ADDRESS'] = 'Enter email address';
$lang['ENTER_PASSWORD'] = 'Enter password';
/********************************************
 * ADMIN
 ******************************************** */
$lang["ADMINISTRATOR_LIST"] = "Administrator List";
$lang["ADMINISTRATORS"] = "Administrators";
$lang["ADMINISTRATOR"] = "Administrator";

/********************************************
 * ADMIN
 ******************************************** */
$lang["USER_LIST"] = "User List";
$lang["USERS"] = "Users";
$lang["USER"] = "User";



//$lang["TEXT_TITLE_ADMIN_PAGE"] = "Administrators List";
//$lang["TEXT_HEADING_ADMIN_PAGE"] = "Administrators List";
//$lang["TEXT_BREADCRUMB_ADMIN_PAGE"] = "Administrators List";


//$lang["TEXT_TITLE_ADMIN_ADD_PAGE"] = "Administrators";
//$lang["TEXT_HEADING_ADMIN_ADD_PAGE"] = "Administrators";
//$lang["TEXT_BREADCRUMB_ADMIN_ADD_PAGE"] = "Administrators";
//$lang["TEXT_ADMIN_ADD_PAGE"] = "Add Administrators";




