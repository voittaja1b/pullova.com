<?php
class User extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }
    function adddevicedoken() {
        extract($_REQUEST);
        if (isset($vDeviceToken) && $vDeviceToken != '' && isset($iUserID) && $iUserID != '') {
            $deviceTokenData = $this->user_model->addDeviceToken($_REQUEST);
            if ($deviceTokenData) {
                $responseData['status'] = "0";
                $responseData['data'] = "";
                $responseData['message'] = "Add Device Token Successfully";
            } else {
                $responseData['status'] = "1";
                $responseData['data'] = "";
                $responseData['message'] = "Something going wrong, Please try..";
            }
            je($responseData);
        }
    }
    function removedevicetoken() {
        if (isset($_REQUEST['iUserID']) && $_REQUEST['iUserID'] != '') {
            $postData['iUserID'] = $_REQUEST['iUserID'];
            $postData['vDeviceToken'] = "";
            $deviceTokenData = $this->user_model->removeDeviceToken($postData);
            if ($deviceTokenData) {
                $responseData['status'] = "0";
                $responseData['data'] = "";
                $responseData['message'] = "Device Token Removed Successfully";
            } else {
                $responseData['status'] = "1";
                $responseData['data'] = "";
                $responseData['message'] = "Something going wrong, Please try..";
            }
            je($responseData);
        }
    }
    
    function addPromoCode() {
        if (isset($_REQUEST) && $_REQUEST != "") {
            $this->load->model("user_model");
            extract($_REQUEST);
            $done = false;
            if (isset($iUserID) && $iUserID != "") {
              if (isset($vPromoCode) && $vPromoCode != "") {
                $postData = $_REQUEST;
                $userEdit = $this->user_model->addPromoCode($postData);
                $responseData['status'] = "0";

                $responseData['data'] = NULL;

                $responseData['message'] = "You have added promo code successfully";
                $done = true;
              }
            }
            if (!$done) {
                $responseData['status'] = "1";

                $responseData['data'] = NULL;

                $responseData['message'] = "Something going wrong, Please try later" . $iUserId . $vPromoCode;
            }
    
            je($responseData);
        }
    }
    
    function editprofile() {
        if (isset($_REQUEST) && $_REQUEST != "") {
            $this->load->model("user_model");
            extract($_REQUEST);
            if (isset($iUserID) && $iUserID != "") {
                if (isset($vEmail) && $vEmail != "") {
                    if (!$this->user_model->checkUserEmailAvailable($vEmail, $iUserID)) {
                        $responseData['status'] = "1";
                        $responseData['data'] = array();
                        $responseData['message'] = "Email address already exists.";
                        je($responseData);
                    }
                }
                if (isset($vUsername) && $vUsername != "") {
                    if (!$this->user_model->checkUsernameAvailable($vUsername, $iUserID)) {
                        $responseData['status'] = "1";
                        $responseData['data'] = array();
                        $responseData['message'] = "Username already exists.";
                        je($responseData);
                    }
                }
                if (isset($userPhone) && $userPhone != "") {
                    if (!$this->user_model->checkPhoneNumber($userPhone)) {
                        $responseData['status'] = "6";
                        $responseData['data'] = array();
                        $responseData['message'] = "Phonenumber is already registered.";
                        je($responseData);
                    }
                }
                $postData = $_REQUEST;
                $imageChk = 1;
                if ($_FILES && $_FILES['vImage']['name'] != '') {
                    $this->load->library('imageUpload/upload', $_FILES['vImage']);
                    $handleImage = new Upload($_FILES['vImage']);
                    if ($handleImage->uploaded) {
                        $handleImage->allowed = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
                        $handleImage->Process(PROFILE_IMAGE_PATH);
                        $handleImage->allowed = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
                        $handleImage->image_resize = true;
                        $handleImage->image_ratio_crop = true;
                        $handleImage->image_y = 190;
                        $handleImage->image_x = 190;
                        $handleImage->Process(PROFILE_THUMB_PATH);
                        if ($handleImage->processed) {
                            $hr_newname = $handleImage->file_dst_name_body;
                            $hr_newname .= '.' . $handleImage->file_dst_name_ext;

                            $postData['vImage'] = $hr_newname;

                            $get_image = $this->user_model->getUserDataByID($iUserID);

                            if (isset($get_image['vImage'])) {

                                if (file_exists(PROFILE_THUMB_PATH . $get_image['vImage'])) {

                                    unlink(PROFILE_THUMB_PATH . $get_image['vImage']);

                                }

                                if (file_exists(PROFILE_IMAGE_PATH . $get_image['vImage'])) {

                                    unlink(PROFILE_IMAGE_PATH . $get_image['vImage']);

                                }

                            }

                        } else {

                            $imageChk = 0;

                            $responseData['status'] = "1";

                            $responseData['data'] = array();

                            $responseData['message'] = $handleImage->error;

                            je($responseData);

                        }

                    } else {

                        $imageChk = 0;

                        $responseData['status'] = "1";

                        $responseData['data'] = array();

                        $responseData['message'] = "Problem in update image ";

                        je($responseData);

                    }

                }

                if ($imageChk) {

                    if (isset($postData['vPassword']))

                        $postData['vPassword'] = md5($postData['vPassword']);

                    $userEdit = $this->user_model->editProfile($postData);

                    $getUserDataByID['iUserID'] = $postData['iUserID'];

                    if ($userEdit)

                        $user_detail = $this->user_model->getUserDataByID($getUserDataByID['iUserID']);

                }

            }

            if (isset($user_detail)) {

                $user_arr = array();

                foreach ($user_detail as $key => $row) {

                    $user_arr[$key] = $row;

                    if (file_exists(PROFILE_IMAGE_PATH . $row) && $row != '') {

                        //$user_arr[$key] = PROFILE_IMAGE_URL . $row;

                        $user_arr['profileImage'] = array('original' => (isset($row) && $row != '' && file_exists(PROFILE_IMAGE_PATH . $user_detail['vImage']) ) ? PROFILE_IMAGE_URL . $user_detail['vImage'] : "",

                            'thumb' => (isset($row) && $row != '' && file_exists(PROFILE_THUMB_PATH . $row)) ? PROFILE_THUMB_URL . $user_detail['vImage'] : "");

                    } else {

                        if ($key == 'vImage') {

                            $user_arr['profileImage'] = array('original' => "", 'thumb' => '');

                        }

                    }

                }
                
                $link = $user_arr['vImage'];
                $this->updateStatusifApplication($iUserID,$link);

                $responseData['status'] = "0";

                $responseData['data'] = $user_arr;

                $responseData['message'] = "You have edited profile successfully";

            } else {

                $responseData['status'] = "1";

                $responseData['data'] = NULL;

                $responseData['message'] = "Something going wrong, Please try later";

            }

            je($responseData);

        }

    }
    
    
    
        function updatePhoneNumber(){
    $iUserID = intVal($this->input->get_post("iUserID"));
    $phoneNumber = $this->input->get_post('userPhone');
        $sql = "UPDATE  `tbl_user` SET  `userPhone` =  '$phoneNumber' WHERE  `tbl_user`.`iUserID` = '$iUserID';";
        $query = $this->db->query($sql);
            
        
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Updated Successfully";
        
        je($responseData);
}
    
    
    function updateStatusifApplication($iUserID,$link){
        $sql = "UPDATE  `tbl_applicants` SET  `Status` =  'pending' WHERE  `tbl_applicants`.`DriverID` = '$iUserID';";
        $query = $this->db->query($sql);
        
        $rows = $this->db->affected_rows();
        if ($rows==1){
            $this->updatePictureApplication($iUserID,$link);
        }
}
    function updatePictureApplication($iUserID,$link){
    $sql = "UPDATE  `tbl_applicants` SET  `imageofApplicant` =  'http://rideapp.us/ws/images/profile/original/$link' WHERE  `tbl_applicants`.`DriverID` = '$iUserID';";
        
        $this->updatePictureUserTable($iUserID,$user_arr);
        $query = $this->db->query($sql);
    }
    
    function updatePictureUserTable($iUserID,$user_arr){
        $sql = "UPDATE  `tbl_user` SET  `vImage` =  '$link' WHERE  `tbl_user`.`iUserID` = '$iUserID';";
        
        $query = $this->db->query($sql);    
    }
// UPDATE HERE!!


    function search() {

        if ($this->input->get_post('q') && $this->input->get_post('q') != '') {

            $user_arr = array();

            $q = trim($this->input->get_post('q'));

            $q = strtolower($q);

            $user_detail = $this->user_model->searchuser($q, $this->input->get_post('iUserID'));

            if (isset($user_detail) && !empty($user_detail)) {

                foreach ($user_detail as $key => $value) {

                    $userArray[$key]['iUserID'] = $value['iUserID'];

                    $userArray[$key]['vUsername'] = $value['vUsername'];

                    $userArray[$key]['vEmail'] = $value['vEmail'];

                    $userArray[$key]['vFirst'] = $value['vFirst'];

                    $userArray[$key]['vLast'] = $value['vLast'];
                    
                    $userArray[$key]['vUserPhone'] = $value['vUserPhone'];
                    
                    $userArray[$key]['vToken'] = $value['vToken'];
                    
                    $userArray[$key]['last4'] = $value['last4'];
                    
                    $userArray[$key]['vDriverorNot'] = $value['vDriverorNot'];
                    
                    $userArray[$key]['vCarType'] = $value['vCarType'];
                    
                    $userArray[$key]['driverLicensePlate'] = $value['driverLicensePlate'];
                    
                    
                    $userArray[$key]['typeofcard'] = $value['typeofcard'];
                    
                    $userArray[$key]['vCustomerId'] = $value['vCustomerId'];

                    $userArray[$key]['vImage'] = array('original' => (isset($value['vImage']) && $value['vImage'] != '' && file_exists(PROFILE_IMAGE_PATH . $value['vImage'])) ? PROFILE_IMAGE_URL . $value['vImage'] : "",

                        'thumb' => (isset($value['vImage']) && $value['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $value['vImage'])) ? PROFILE_THUMB_URL . $value['vImage'] : "");

                    $userArray[$key]['eLogin'] = $value['eLogin'];

                    $userArray[$key]['dLoginDate'] = $value['dLoginDate'];

                    $userArray[$key]['vFbID'] = $value['vFbID'];

                    $status_data = $this->user_model->getFriendshipStatus($this->input->get_post('iUserID'), $value['iUserID']);

                    if (isset($status_data['eStatus']) && $status_data['eStatus'] != '') {

                        $userArray[$key]['vFriendshipStatus'] = $status_data['eStatus'];

                    } else {

                        $userArray[$key]['vFriendshipStatus'] = '3';

                    }

                }

                $responseData['status'] = "0";

                $responseData['data'] = $userArray;

                $responseData['message'] = "User Detail";

            } else {

                $responseData['status'] = "1";

                $responseData['data'] = array();

                $responseData['message'] = "No User Found.";

            }

        } else {

            $responseData['status'] = "1";

            $responseData['data'] = array();

            $responseData['message'] = "Something going wrong, Please try later";

        }

//            $this->output->enable_profiler(TRUE);

        je($responseData);

    }



    function info() {

        if ($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') {

            $user_detail = $this->user_model->getUserByID($this->input->get_post('iUserID'));

            if (isset($user_detail) && !empty($user_detail)) {

                $userArray['iUserID'] = $user_detail['iUserID'];

                $userArray['vUsername'] = $user_detail['vUsername'];

                $userArray['vEmail'] = $user_detail['vEmail'];

                $userArray['vFirst'] = $user_detail['vFirst'];

                $userArray['vLast'] = $user_detail['vLast'];
                
                $userArray['userPhone'] = $user_detail['userPhone'];

                $userArray['profileImage'] = array('original' => (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_IMAGE_PATH . $user_detail['vImage'])) ? PROFILE_IMAGE_URL . $user_detail['vImage'] : "",

                    'thumb' => (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $user_detail['vImage'])) ? PROFILE_THUMB_URL . $user_detail['vImage'] : "");

                $userArray['eLogin'] = $user_detail['eLogin'];

                $userArray['dLoginDate'] = $user_detail['dLoginDate'];

                $userArray['vFbID'] = $user_detail['vFbID'];

                $responseData['status'] = "0";

                $responseData['data'] = $userArray;

                $responseData['message'] = "User Detail";

            } else {

                $responseData['status'] = "1";

                $responseData['data'] = array();

                $responseData['message'] = "Invalid user.";

            }

        } else {

            $responseData['status'] = "1";

            $responseData['data'] = array();

            $responseData['message'] = "Something going wrong, Please try later";

        }

        je($responseData);

    }



    function getuserdatabyfbid() {



        $vFbID = $_REQUEST['vFbID'];

        if (isset($_REQUEST['vDeviceToken']) && $_REQUEST['vDeviceToken'] != '')

            $vDeviceToken = $_REQUEST['vDeviceToken'];

        else

            $vDeviceToken = '';

        if (isset($vFbID) && $vFbID != "") {

            $postData['vFbID'] = $vFbID;

            $user_login = $this->user_model->checkLoginByFbIDAndDeviceToken($vFbID, $vDeviceToken);

        }

        if ($user_login) {

            $user_detail = $this->user_model->getUserDataByFbID($user_login);

            $userArray['iUserID'] = $user_detail['iUserID'];

            $userArray['vUsername'] = $user_detail['vUsername'];

            $userArray['vEmail'] = $user_detail['vEmail'];
            
            $userArray['userPhone'] = $user_detail['userPhone'];
            
            $userArray['codefordiscount'] = $user_detail['codefordiscount'];
            
            $userArray['verifiedPhone'] = $user_detail['verifiedPhone'];
            
            $userArray['vToken'] = $user_detail['vToken'];
            
            $userArray['last4'] = $user_detail['last4'];
            
            $userArray['vDriverorNot'] = $user_detail['vDriverorNot'];

            $userArray['typeofcard'] = $user_detail['typeofcard'];
            
            $userArray['vCarType'] = $user_detail['vCarType'];
            
            $userArray['driverLicensePlate'] = $user_detail['driverLicensePlate'];
            
            $userArray['vFirst'] = $user_detail['vFirst'];

            $userArray['vLast'] = $user_detail['vLast'];
            


            $userArray['profileImage']['original'] = (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_IMAGE_PATH . $user_detail['vImage'])) ? PROFILE_IMAGE_URL . $user_detail['vImage'] : "";



            $userArray['profileImage']['thumb'] = (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $user_detail['vImage'])) ? PROFILE_THUMB_URL . $user_detail['vImage'] : "";



            $userArray['eLogin'] = $user_detail['eLogin'];

            $userArray['dLoginDate'] = $user_detail['dLoginDate'];

            $userArray['vFbID'] = $user_detail['vFbID'];



            $responseData['status'] = "0";

            $responseData['data'] = $userArray;

            $responseData['message'] = "User information";

            je($responseData);

        } else {

            $responseData['status'] = "1";

            $responseData['data'] = array();

            $responseData['message'] = "Username not found.";

            je($responseData);

        }

    }



    function sendFriendRequest() {

        if (($this->input->get_post('iFriendRequestBy') && $this->input->get_post('iFriendRequestBy') != '') &&

                ($this->input->get_post('iFriendRequestTo') && $this->input->get_post('iFriendRequestTo') != '')

        ) {

            $friendList['iFriendRequestBy'] = $this->input->get_post('iFriendRequestBy');

            $friendList['iFriendRequestTo'] = $this->input->get_post('iFriendRequestTo');

            $friend = $this->user_model->checkFriendship($friendList);

            if (!$friend) {

                $postData['iFriendRequestBy'] = $this->input->get_post('iFriendRequestBy');

                $postData['iFriendRequestTo'] = $this->input->get_post('iFriendRequestTo');

                $postData['eRequestStatus'] = 'Pending';

                $friend_id = $this->user_model->setFriendRequest($postData);

                if ($friend_id > 0) {

                    $responseData['status'] = "0";

                    $responseData['message'] = "Friend request sent successfully";

                } else {

                    $responseData['status'] = "1";

                    $responseData['message'] = "Error in request";

                }

            } else {

                $responseData['status'] = "2";

                $responseData['message'] = "Already in friend list";

            }



            je($responseData);

        }

    }



    function locationRequest() {

        if (($this->input->get_post('iSenderID') && $this->input->get_post('iSenderID') != '') &&

                ($this->input->get_post('iRecieverID') && $this->input->get_post('iRecieverID') != '')

        ) {

            if ($this->input->get_post('eStatus') == '0') {

                $result = $this->user_model->checkLocationRequestStatus($this->input->get_post('iSenderID'), $this->input->get_post('iRecieverID'));

                $postData['iSenderID'] = $this->input->get_post('iSenderID');

                $postData['iRecieverID'] = $this->input->get_post('iRecieverID');

                $senderRecords = $this->user_model->getUserByID($this->input->get_post('iSenderID'));

                $receiverRecords = $this->user_model->getUserByID($this->input->get_post('iRecieverID'));

                if ($result == '') {

                    $postData['dCreatedDate'] = $this->general_model->getDBDateTime();

                    $requestResult = $this->user_model->addLocationRequest($postData);

                    if ($requestResult > 0) {


                        if (!empty($senderRecords)) {

                            $notificationMsg = $senderRecords['vFirst'] . " " . $senderRecords['vLast'] . " has sent your location request";

                            $generatePayloadData['iSenderID'] = $senderRecords['iUserID'];

                            $generatePayloadData['iRecieverID'] = $this->input->get_post('iRecieverID');

                            $generatePayloadData['isFriend'] = 'no';

                            $notificationType = "request";

                            $this->sendPushNotification($receiverRecords['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);

                            $this->sendNotificationAndroid(GOOGLE_API_KEY, $receiverRecords['vDeviceToken'], $notificationMsg, $generatePayloardData, $notificationType);

                        }

                        $responseData['status'] = "0";

                        $responseData['message'] = "Location request sent successfully";

                    } else {

                        $responseData['status'] = "1";

                        $responseData['message'] = "Error in request";

                    }

                } else {

                    $postData['iLocationRequestID'] = $result['iLocationRequestID'];

                    $postData['eStatus'] = $this->input->get_post('eStatus');

                    $requestResult = $this->user_model->updateLocationRequest($postData);

                    if ($requestResult > 0) {

                        if (!empty($senderRecords)) {

                            $notificationMsg = $senderRecords['vFirst'] . " " . $senderRecords['vLast'] . " has sent your location request";

                            $generatePayloadData['iSenderID'] = $senderRecords['iUserID'];

                            $generatePayloadData['iRecieverID'] = $this->input->get_post('iRecieverID');

                            $generatePayloadData['isFriend'] = 'no';

                            $notificationType = "request";

                            $this->sendPushNotification($receiverRecords['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);

                            $this->sendNotificationAndroid(GOOGLE_API_KEY, $receiverRecords['vDeviceToken'], $notificationMsg, $generatePayloardData, $notificationType);

                        }

                        $responseData['status'] = "0";

                        $responseData['message'] = "Location request sent successfully";

                    } else {

                        $responseData['status'] = "1";

                        $responseData['message'] = "Error in request";

                    }

                }

                je($responseData);

            } elseif (($this->input->get_post('iSenderID') && $this->input->get_post('iSenderID') != '') && ($this->input->get_post('iRecieverID') && $this->input->get_post('iRecieverID') != '')) {

                if (($this->input->get_post('eStatus') == '1') &&

                        ($this->input->get_post('fLat') && $this->input->get_post('fLat') != '') && ($this->input->get_post('fLong') && $this->input->get_post('fLong') != '')) {

                    $postArray['eStatus'] = $this->input->get_post('eStatus');

                    $postArray['fLat'] = $this->input->get_post('fLat');

                    $postArray['fLong'] = $this->input->get_post('fLong');



                    $senderData = $this->user_model->getUserByID($this->input->get_post('iSenderID'));

                    $friendData = $this->user_model->getUserByID($this->input->get_post('iRecieverID'));



                    $notificationMsg = $friendData['vFirst'] . " " . $friendData['vLast'] . " has accepted your location request";



                    $notificationType = "response";

                    $generatePayloadData = '';

                    $this->sendPushNotification($senderData['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);

                    $this->sendNotificationAndroid(GOOGLE_API_KEY, $senderData['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);

                } elseif ($this->input->get_post('eStatus') == '2') {

                    $postArray['eStatus'] = $this->input->get_post('eStatus');

                    $senderData = $this->user_model->getUserByID($this->input->get_post('iSenderID'));

                    $friendData = $this->user_model->getUserByID($this->input->get_post('iRecieverID'));



                    $notificationMsg = $friendData['vFirst'] . " " . $friendData['vLast'] . " has rejected your location request";



                    $notificationType = "response";

                    $generatePayloadData = '';

                    $this->sendPushNotification($senderData['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);

                    $this->sendNotificationAndroid(GOOGLE_API_KEY, $senderData['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);

                }

                $update = $this->user_model->updateRequestStatus($this->input->get_post('iSenderID'), $this->input->get_post('iRecieverID'), $postArray);

                if ($update) {



                    $responseData['status'] = "0";

                    $responseData['message'] = "Success";

                } else {

                    $responseData['status'] = "1";

                    $responseData['message'] = "Error";

                }

                je($responseData);

            }

        }

    }



    function pendingLocationRequest() {

        if ($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') {

            $locationRequestPendingList = $this->user_model->getLocationRequestPendingList($this->input->get_post('iUserID'));

            if ($locationRequestPendingList > 0) {

                foreach ($locationRequestPendingList as $key => $user_detail) {

                    $locationRequestPendingList[$key]['profileImage']['original'] = (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_IMAGE_PATH . $user_detail['vImage'])) ? PROFILE_IMAGE_URL . $user_detail['vImage'] : "";



                    $locationRequestPendingList[$key]['profileImage']['thumb'] = (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $user_detail['vImage'])) ? PROFILE_THUMB_URL . $user_detail['vImage'] : "";

                }

                $responseData['status'] = "0";

                $responseData['data'] = $locationRequestPendingList;

                $responseData['message'] = "Pending location request list";

            } else {

                $responseData['status'] = "0";

                $responseData['data'] = array();

                $responseData['message'] = "No pending location request found";

            }

            je($responseData);

        }

    }



    function locationRequestSentList() {

        if ($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') {

            $locationRequestSentList = $this->user_model->getSentLocationRequestList($this->input->get_post('iUserID'));

            if ($locationRequestSentList > 0) {

                foreach ($locationRequestSentList as $key => $user_detail) {

                    $locationRequestSentList[$key]['profileImage']['original'] = (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_IMAGE_PATH . $user_detail['vImage'])) ? PROFILE_IMAGE_URL . $user_detail['vImage'] : "";



                    $locationRequestSentList[$key]['profileImage']['thumb'] = (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $user_detail['vImage'])) ? PROFILE_THUMB_URL . $user_detail['vImage'] : "";

                }

                $responseData['status'] = "0";

                $responseData['data'] = $locationRequestSentList;

                $responseData['message'] = "Sent location request list";

            } else {

                $responseData['status'] = "0";

                $responseData['data'] = array();

                $responseData['message'] = "No sent location request found";

            }

        } else {

            $responseData['status'] = "1";

            $responseData['data'] = array();

            $responseData['message'] = "Something going wrong, please try again";

        }

        je($responseData);

    }



    function userLocationRequestResponseList() {

        //iUserID

        if ($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') {

            $userLocationRequestList = $this->user_model->getUserLocationRequestResponseList($this->input->get_post('iUserID'));

            if ($userLocationRequestList > 0) {

                foreach ($userLocationRequestList as $key => $user_detail) {

                    $userLocationRequestList[$key]['profileImage']['original'] = (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_IMAGE_PATH . $user_detail['vImage'])) ? PROFILE_IMAGE_URL . $user_detail['vImage'] : "";



                    $userLocationRequestList[$key]['profileImage']['thumb'] = (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $user_detail['vImage'])) ? PROFILE_THUMB_URL . $user_detail['vImage'] : "";

                }

                $responseData['status'] = "0";

                $responseData['data'] = $userLocationRequestList;

                $responseData['message'] = "Location sent/recieved request list";

            } else {

                $responseData['status'] = "0";

                $responseData['data'] = array();

                $responseData['message'] = "No location sent/recieved request found";

            }

        } else {

            $responseData['status'] = "1";

            $responseData['data'] = array();

            $responseData['message'] = "Something going wrong, please try again";

        }

        je($responseData);

    }



    function feedAdd() {
		if (($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') &&
                ($this->input->get_post('vFeedTitle') && $this->input->get_post('vFeedTitle') != '')

        ) {
		
            $postData = $_REQUEST;	
				
			if($this->input->get_post('type')=='video') {
				if ($_FILES && $_FILES['vFeedVideo']['name'] != '') {
					$videofile		=	time().'-'.$_FILES['vFeedVideo']['name'];
					$strvideoPath	=	FEED_ITEM_VIDEO_PATH.$videofile;
					copy($_FILES['vFeedVideo']['tmp_name'],$strvideoPath);	
					$postData['videofile'] = $videofile;			
				}
				$postData['type'] = 'video';	

				if ($_FILES && $_FILES['vFeedItemImage']['name'] != '') {
					$this->load->library('imageUpload/upload', $_FILES['vFeedItemImage']);
					$handleImage = new Upload($_FILES['vFeedItemImage']);
					if ($handleImage->uploaded) {
						$handleImage->allowed = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
						$handleImage->Process(FEED_ITEM_IMAGE_PATH);
						if ($handleImage->processed) {
							$hr_newname = $handleImage->file_dst_name_body;
							$hr_newname .= '.' . $handleImage->file_dst_name_ext;
							$postData['vFeedItemImage'] = $hr_newname;
						}
					}	
				}

		
			} else {
				if ($_FILES && $_FILES['vFeedMapImage']['name'] != '') {
					$this->load->library('imageUpload/upload', $_FILES['vFeedMapImage']);
					$handleImage = new Upload($_FILES['vFeedMapImage']);
					if ($handleImage->uploaded) {
						$handleImage->allowed = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
						$handleImage->Process(FEED_MAP_IMAGE_PATH);
						if ($handleImage->processed) {
							$hr_newname = $handleImage->file_dst_name_body;
							$hr_newname .= '.' . $handleImage->file_dst_name_ext;
							$postData['vFeedMapImage'] = $hr_newname;
						}
					}
				}
								
				if ($_FILES && $_FILES['vFeedItemImage']['name'] != '') {
					$this->load->library('imageUpload/upload', $_FILES['vFeedItemImage']);
					$handleImage = new Upload($_FILES['vFeedItemImage']);
					if ($handleImage->uploaded) {
						$handleImage->allowed = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
						$handleImage->Process(FEED_ITEM_IMAGE_PATH);
						if ($handleImage->processed) {
							$hr_newname = $handleImage->file_dst_name_body;
							$hr_newname .= '.' . $handleImage->file_dst_name_ext;
							$postData['vFeedItemImage'] = $hr_newname;
						}
					}	
				}
			} 	
        $postData['dCreatedDate'] = $this->general_model->getDBDateTime();
        $postData['vUserView'] = $this->input->get_post('vUserView');
        $feedID = $this->user_model->addFeed($postData);
		
		//find nearest divers
		$fLat	= $postData['fLat'];
		$fLong	= $postData['fLong'];
		$nearestDriverIDs	= $this->user_model->getDriversWithDistance($fLat, $fLong);
 
 		$postDriverData	= array();
		if(!empty($nearestDriverIDs)){
                 
			foreach($nearestDriverIDs as $key=>$DriverIDs){
                $distance = $DriverIDs["distance"];
                $distance = number_format($distance, 1);
                $maximum = "15";
                $maximum = number_format($maximum,1);
                if ($distance > $maximum){
            $postDriverData[$key]["iFeedID"]	= $feedID;
			$postDriverData[$key]["iUserID"]	= $DriverIDs["iUserID"];
				$postDriverData[$key]["fDistance"]	= $DriverIDs["distance"];
				$postDriverData[$key]["status"] 	= "rejected";	
                }else{
				$postDriverData[$key]["iFeedID"]	= $feedID;
				$postDriverData[$key]["iUserID"]	= $DriverIDs["iUserID"];
				$postDriverData[$key]["fDistance"]	= $DriverIDs["distance"];
				$postDriverData[$key]["status"] 	= "send";	
                }
			}
			$this->user_model->doInsertFeedWithDrivers($postDriverData);
		}
		
		if ($feedID > 0) {
			$responseData['status'] = "0";
			$responseData['message'] = "Feed added successfully";
            $checkDuplicateID = $this->input->get_post('iUserID');
            $this->checkDuplicateRequest($checkDuplicateID,$feedID);
            $this->sendingCustomPush($feedID);
		} else {
			$responseData['status'] = "1";
			$responseData['message'] = "Error";
		}
		je($responseData);
        }
    }

    function checkDuplicateRequest($checkDuplicateID,$feedID){
    //$checkDuplicateID is iUserID of the requester
        
    $sql = "UPDATE `tbl_feed` SET `vUserView` = 'delete' WHERE NOT (`iFeedID` = $feedID) AND `iUserID` = ".$checkDuplicateID.";";
    $query = $this->db->query($sql);
    }
    
    function sendingCustomPush($feedID) {
//        echo $feedID;
$sql = "SELECT * FROM `tbl_nearest_drivers` WHERE `iFeedID` = $feedID AND `status` = 'send' AND `fDistance` <= 15";
      $query = $this->db->query($sql);
        
        $rows = $query->result_array();
        
         foreach($rows as $row) {
                $this->$row['iUserID'];
                }
        
        if ($row['iUserID'] == null){
        $responseData['status'] = "0";
        $responseData['data'] = $feedID;
        $responseData['message'] = "No current route";
        }else{
        
         $responseData['status'] = "0";
        $responseData['data'] = array();
      $responseData['message'] = $row['iUserID'];
        $iUserID = $row['iUserID'];
        $this->pushNearestDriver($iUserID);
            
        }
        je($responseData);
    }
    
    
    function sendpushtoNearestDriver($deviceToken,$message) {
     $this->sendPushNotification($deviceToken, $message, $generatePayloadData, $notificationType);
        }
    
    
    
    function pushNearestDriver($iUserID){
            $sql = "SELECT `vDeviceToken` from tbl_user where `iUserID` = $iUserID; ";
            $query = $this->db->query($sql); 
            $rows = $query->result_array();
        
        if (!$rows['vDeviceToken']){
        }else{
        foreach($rows as $row) {
                    $this->$row['vDeviceToken'];
                }
                $deviceToken = $row['vDeviceToken'];
    $message = "New Ride Request! You're the closest one!";
    $this->sendpushtoNearestDriver($deviceToken,$message);
        }
    }
    
    



    function feedList() {

        //Get feed list of perticluar user & his/her friends



        if ($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') {

            $feedList1 = $this->user_model->getFeedList($this->input->get_post('iUserID'));
			$feedList =$this->user_model->array_msort($feedList1, 'iFeedID','DESC');

            if ($feedList > 0) {

                //pre($feedList);

                foreach ($feedList as $k => $v) {

					$feedList[$k]['LikeStatus'] = $this->user_model->doGetUserFeedLikeStatus($v['iFeedID'],$v['iUserID']);

					$feedList[$k]['LikeCount'] = $this->user_model->doGetLikeCountByFeedId($v['iFeedID']);

                    $feedList[$k]['DislikeCount'] = $this->user_model->doGetDislikeCountByFeedId($v['iFeedID']);

					$feedList[$k]['CommentArray'] = $this->user_model->doGetCommentDetailsByFeedId($v['iFeedID']);

					$feedUser	=	$this->user_model->getUserByID($v['iUserID']);
					$feedList[$k]['vFirst']	=	$feedUser['vFirst'];		
					$feedList[$k]['vLast']	=	$feedUser['vLast'];	
					$feedList[$k]['vToken']	=	$feedUser['vToken'];

                    $feedList[$k]['last4']	=	$feedUser['last4'];
                    
                    $feedList[$k]['vDriverorNot']	=	$feedUser['vDriverorNot'];
                    
                    $feedList[$k]['vCarType']	=	$feedUser['vCarType'];
                    
                    $feedList[$k]['driverLicensePlate']	=	$feedUser['driverLicensePlate'];
                    
                    $feedList[$k]['typeofcard']	=	$feedUser['typeofcard'];
                    
					if ($feedList[$k]['CommentArray']) {
						foreach($feedList[$k]['CommentArray'] as $key=>$comments) {
							$userArray = $this->user_model->getUserDataByID($comments['UserId']);
							$feedList[$k]['CommentArray'][$key]['iUserID']	=	$userArray['iUserID'];
							$feedList[$k]['CommentArray'][$key]['vFirst']	=	$userArray['vFirst'];		
							$feedList[$k]['CommentArray'][$key]['vLast']	=	$userArray['vLast'];	
							$feedList[$k]['CommentArray'][$key]['original']	=	PROFILE_IMAGE_URL . $userArray['vImage'];
							$feedList[$k]['CommentArray'][$key]['thumb']	=	PROFILE_THUMB_URL . $userArray['vImage'];				
						}
					}

                    $feedList[$k]['vFeedMapImage'] = (isset($v['vFeedMapImage']) && $v['vFeedMapImage'] != '' && file_exists(FEED_MAP_IMAGE_PATH . $v['vFeedMapImage'])) ? FEED_MAP_IMAGE_URL . $v['vFeedMapImage'] : "";

                    $feedList[$k]['vFeedItemImage'] = (isset($v['vFeedItemImage']) && $v['vFeedItemImage'] != '' && file_exists(FEED_ITEM_IMAGE_PATH . $v['vFeedItemImage'])) ? FEED_ITEM_IMAGE_URL . $v['vFeedItemImage'] : "";

                    $feedList[$k]['vImage'] = (isset($feedUser['vImage']) && $feedUser['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $feedUser['vImage'])) ? PROFILE_THUMB_URL . $feedUser['vImage'] : "";

					$feedList[$k]['videofile'] = (isset($v['videofile']) && $v['videofile'] != '' && file_exists(FEED_ITEM_VIDEO_PATH . $v['videofile'])) ? FEED_ITEM_VIDEO_URL . $v['videofile'] : "";


                    $tagFriends = $this->user_model->getFeedTagFriendInfo($v['iFeedID']);

                    if ($tagFriends > 0) {

//                        pre($tagFriends);

                        foreach ($tagFriends as $feedKey => $feedVal) {

                            $feedList[$k]['feedTagFriends'][$feedKey]['iUserID'] = $feedVal['iUserID'];

                            $feedList[$k]['feedTagFriends'][$feedKey]['vFirst'] = $feedVal['vFirst'];

                            $feedList[$k]['feedTagFriends'][$feedKey]['vLast'] = $feedVal['vLast'];

                            $feedList[$k]['feedTagFriends'][$feedKey]['vImage'] = (isset($feedVal['vImage']) && $feedVal['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $feedVal['vImage'])) ? PROFILE_THUMB_URL . $feedVal['vImage'] : "";

                            ;

                        }

                    } else {

                        $feedList[$k]['feedTagFriends'] = array();

                    }

                }

                $responseData['status'] = "0";

                $responseData['data'] = $feedList;

                $responseData['message'] = "Feed list";

            } else {

                $responseData['status'] = "0";

                $responseData['data'] = array();

                $responseData['message'] = "No feed found";

            }

        } else {

            $responseData['status'] = "1";

            $responseData['data'] = array();

            $responseData['message'] = "Something going wrong, please try again";

        }
		//printArray($responseData); exit;
	    je($responseData);
    }

    
    
    function driverFeedList() {

        if ($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') {

            $feedList1 = $this->user_model->driverGetFeed($this->input->get_post('iUserID'));
			$feedList =$this->user_model->array_msort($feedList1, 'iFeedID','DESC');

            if ($feedList > 0) {

                //pre($feedList);

                foreach ($feedList as $k => $v) {

					$feedList[$k]['LikeStatus'] = $this->user_model->doGetUserFeedLikeStatus($v['iFeedID'],$v['iUserID']);

					$feedList[$k]['LikeCount'] = $this->user_model->doGetLikeCountByFeedId($v['iFeedID']);

                    $feedList[$k]['DislikeCount'] = $this->user_model->doGetDislikeCountByFeedId($v['iFeedID']);

					$feedList[$k]['CommentArray'] = $this->user_model->doGetCommentDetailsByFeedId($v['iFeedID']);

					$feedUser	=	$this->user_model->getUserByID($v['iUserID']);
					$feedList[$k]['vFirst']	=	$feedUser['vFirst'];		
					$feedList[$k]['vLast']	=	$feedUser['vLast'];	

					if ($feedList[$k]['CommentArray']) {
						foreach($feedList[$k]['CommentArray'] as $key=>$comments) {
							$userArray = $this->user_model->getUserDataByID($comments['UserId']);
							$feedList[$k]['CommentArray'][$key]['iUserID']	=	$userArray['iUserID'];
							$feedList[$k]['CommentArray'][$key]['vFirst']	=	$userArray['vFirst'];		
							$feedList[$k]['CommentArray'][$key]['vLast']	=	$userArray['vLast'];	
							$feedList[$k]['CommentArray'][$key]['original']	=	PROFILE_IMAGE_URL . $userArray['vImage'];
							$feedList[$k]['CommentArray'][$key]['thumb']	=	PROFILE_THUMB_URL . $userArray['vImage'];				
						}
					}

                    $feedList[$k]['vFeedMapImage'] = (isset($v['vFeedMapImage']) && $v['vFeedMapImage'] != '' && file_exists(FEED_MAP_IMAGE_PATH . $v['vFeedMapImage'])) ? FEED_MAP_IMAGE_URL . $v['vFeedMapImage'] : "";

                    $feedList[$k]['vFeedItemImage'] = (isset($v['vFeedItemImage']) && $v['vFeedItemImage'] != '' && file_exists(FEED_ITEM_IMAGE_PATH . $v['vFeedItemImage'])) ? FEED_ITEM_IMAGE_URL . $v['vFeedItemImage'] : "";

                    $feedList[$k]['vImage'] = (isset($feedUser['vImage']) && $feedUser['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $feedUser['vImage'])) ? PROFILE_THUMB_URL . $feedUser['vImage'] : "";

					$feedList[$k]['videofile'] = (isset($v['videofile']) && $v['videofile'] != '' && file_exists(FEED_ITEM_VIDEO_PATH . $v['videofile'])) ? FEED_ITEM_VIDEO_URL . $v['videofile'] : "";


                    $tagFriends = $this->user_model->getFeedTagFriendInfo($v['iFeedID']);

                    if ($tagFriends > 0) {

//                        pre($tagFriends);

                        foreach ($tagFriends as $feedKey => $feedVal) {

                            $feedList[$k]['feedTagFriends'][$feedKey]['iUserID'] = $feedVal['iUserID'];

                            $feedList[$k]['feedTagFriends'][$feedKey]['vFirst'] = $feedVal['vFirst'];

                            $feedList[$k]['feedTagFriends'][$feedKey]['vLast'] = $feedVal['vLast'];

                            $feedList[$k]['feedTagFriends'][$feedKey]['vImage'] = (isset($feedVal['vImage']) && $feedVal['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $feedVal['vImage'])) ? PROFILE_THUMB_URL . $feedVal['vImage'] : "";

                            ;

                        }

                    } else {

                        $feedList[$k]['feedTagFriends'] = array();

                    }

                }

                $responseData['status'] = "0";

                $responseData['data'] = $feedList;

                $responseData['message'] = "Feed list";

            } else {

                $responseData['status'] = "0";

                $responseData['data'] = array();

                $responseData['message'] = "No feed found";

            }

        } else {

            $responseData['status'] = "1";

            $responseData['data'] = array();

            $responseData['message'] = "Something going wrong, please try again";

        }
		//printArray($responseData); exit;
	    je($responseData);
    }



    function getUserFeedList() {

        //Get feed list of perticluar user

        if ($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') {

            $feedList = $this->user_model->getFeedListByUserID($this->input->get_post('iUserID'));

            if ($feedList > 0) {

                foreach ($feedList as $k => $v) {

                    $feedList[$k]['vFeedMapImage'] = (isset($v['vFeedMapImage']) && $v['vFeedMapImage'] != '' && file_exists(FEED_MAP_IMAGE_PATH . $v['vFeedMapImage'])) ? FEED_MAP_IMAGE_URL . $v['vFeedMapImage'] : "";



                    $feedList[$k]['vFeedItemImage'] = (isset($v['vFeedItemImage']) && $v['vFeedItemImage'] != '' && file_exists(FEED_ITEM_IMAGE_PATH . $v['vFeedItemImage'])) ? FEED_ITEM_IMAGE_URL . $v['vFeedItemImage'] : "";



                    $feedList[$k]['vImage'] = (isset($v['vImage']) && $v['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $v['vImage'])) ? PROFILE_THUMB_URL . $v['vImage'] : "";

                }

                $responseData['status'] = "0";

                $responseData['data'] = $feedList;

                $responseData['message'] = "Feed list";

            } else {

                $responseData['status'] = "0";

                $responseData['data'] = array();

                $responseData['message'] = "No feed found";

            }

        } else {

            $responseData['status'] = "1";

            $responseData['data'] = array();

            $responseData['message'] = "Something going wrong, please try again";

        }

        je($responseData);

    }

	

	function doLike() {

		if ($this->input->get_post('iFeedID') !='' && $this->input->get_post('iUserID') != '' && $this->input->get_post('LikeStatus') != '' ) {

			$postData = $_REQUEST;

			$postData['UserId'] = $this->input->get_post('iUserID');

			$postData['FeedId'] = $this->input->get_post('iFeedID');

			$postData['AddedDate'] = $this->general_model->getDBDateTime();

			$feedLikeResult = $this->user_model->getCountFeedLike($postData['UserId'],$postData['FeedId']);

			if (!empty($feedLikeResult['Id'])) {

				$postData1['LikeStatus'] = $this->input->get_post('LikeStatus');

				$getResult = $this->user_model->updateLike($postData1,$feedLikeResult['Id']);    

			} else { 

				$getResult = $this->user_model->addLike($postData);

			}			

			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Added Successfully";

			je($responseData);

		}

	}

    
    function getLatestInfo() {
    
        		if ($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') {
			$latestBookingInfo = $this->user_model->getFeedInfoByDriverID($this->input->get_post('iUserID'));
			//printArray($latestBookingInfo); exit;
			 if (!empty($latestBookingInfo)) {
				/*$feedList	= $this->user_model->getDriverDistanceByUserID($iUserID,$fLat,$fLong);
				//printArray($feedList);			exit;*/
				$bookingInfo				= array();
				$bookingInfo['ID']			= $latestBookingInfo[0]["ID"];
				$bookingInfo['iFeedID']		= $latestBookingInfo[0]["iFeedID"];
				$bookingInfo['iUserID']		= $latestBookingInfo[0]['iUserID'];
				$bookingInfo['distance']	= round($latestBookingInfo[0]['fDistance'],1);
				$bookingInfo['vCost']		= $latestBookingInfo[0]["vCost"];
//				$bookingInfo['fAddress']	= $latestBookingInfo[0]['fAddress'];
				
				if (count($bookingInfo) > 0 && !empty($bookingInfo)) {
					$responseData['status'] = "0";
					$responseData['data'] = $bookingInfo;
					$responseData['message'] = "Nearest Driver Info";
				} else {
					$responseData['status'] = "0";
					$responseData['data'] = array();
					$responseData['message'] = "No feed found";
				}
			} else {
				$responseData['status'] = "0";
				$responseData['data'] = array();
				$responseData['message'] = "No feed found";
			}
		} else {
            $responseData['status'] = "1";
            $responseData['data'] = array();
            $responseData['message'] = "Something going wrong, please try again";
        }
        je($responseData);
        
    }
    
    
    function selectnearestdriver() {
        
    $iFeedID = $this->input->get_post('iFeedID');
        
    $sql = "SELECT * FROM  `tbl_nearest_drivers` WHERE `iFeedID` = $iFeedID AND `status` = 'send' ORDER BY `tbl_nearest_drivers`.`fDistance` ASC LIMIT 1";
      $query = $this->db->query($sql);
        
        $rows = $query->result_array();
        
         foreach($rows as $row) {
                $this->$row['iUserID'];
                }
        
        if ($row['iUserID'] == null){
        $responseData['status'] = "0";
        $responseData['data'] = array();
        $responseData['message'] = "No current route";
        }else{
        
         $responseData['status'] = "0";
        $responseData['data'] = array();
      $responseData['message'] = $row['iUserID'];
        }
        je($responseData);
    }
    
    
    function driverstatus() {
        
    $iUserID = intVal($this->input->get_post("iUserID"));
    $status = $this->input->get_post('status');
    
     $sql = "UPDATE  `tbl_user` SET  `onlineornot` = '$status' WHERE  `tbl_user`.`iUserID` = ".$iUserID.";";
        
        $query = $this->db->query($sql);
            
        
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Updated Successfully";
        
        je($responseData);
        
    }
    
    
    function getUsernamefromiUserID(){
    
     $iFeedID = intVal($this->input->get_post("iFeedID"));
    
     $sql = "SELECT * FROM `tbl_user` WHERE `iUserID` = (SELECT `iUserID` FROM `tbl_feed` WHERE `iFeedID` = $iFeedID);";
        
   $query = $this->db->query($sql);
        $rows = $query->result_array();
            
        je($rows);
        
    }
    
    
     function getInfoForPopUp(){
    
     $iFeedID = intVal($this->input->get_post("iFeedID"));
    
     $sql = "SELECT * FROM `tbl_feed` WHERE `iFeedID` = $iFeedID;";
        
        $query = $this->db->query($sql);
        $rows = $query->result_array();
            
        je($rows);
        
    }
    
    
function updatelastfourdigits() {
        
    $iUserID = intVal($this->input->get_post("iUserID"));
    $lastfour = $this->input->get_post('lastfour');
    $typeofcard = $this->input->get_post('typeofcard');
    
     $sql = "UPDATE  `tbl_user` SET  `typeofcard` = '$typeofcard', `last4` = '$lastfour'  WHERE  `tbl_user`.`iUserID` = ".$iUserID.";";
        
        $query = $this->db->query($sql);
            
        
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Updated Successfully";
        
        je($responseData);
        
    }
    
    
	function updateBookingStatus() {
		if ($this->input->get_post('ID') && $this->input->get_post('ID') != '') {
			$postData["status"] = $this->input->get_post('status');
			$result = $this->user_model->doUpdateBookingStatus($postData, $this->input->get_post('ID'),$this->input->get_post('iFeedID'));
			if($result==1){
				$responseData['status'] = "0";
            	$responseData['data'] = array();
            	$responseData['message'] = "Status updated successfully";
			}
		} else {
            $responseData['status'] = "1";
            $responseData['data'] = array();
            $responseData['message'] = "Something going wrong, please try again";
        }
        je($responseData);
	}
    
	function driverCurrentLocation () {
	
    $iUserID = intVal($this->input->get_post("iUserID"));
	$fLat	 = $this->input->get_post('fLat');
	$fLong	 = $this->input->get_post('fLong');
    
        $sql = "UPDATE `tbl_user` SET `fLat` = '$fLat',`fLong` = '$fLong' WHERE `tbl_user`.`iUserID` = ".$iUserID.";";
        
        $query = $this->db->query($sql);
            
        
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Updated Successfully";
        
        je($responseData);

}
    
    function getTokenData($iFeedID){
    // require_once($_SERVER['DOCUMENT_ROOT'].'/application/ws/stripe/lib/Stripe.php');
    require_once(SITE_PATH.'/application/ws/stripe/lib/Stripe.php');
        
    $sql = "SELECT * FROM `tbl_feed` WHERE `iFeedID` = $iFeedID; ";
        $query = $this->db->query($sql);
        
        $rows = $query->result_array();
        
        $iUserID = $rows['0']['iUserID'];
		$iCostID = $rows['0']['vCost'];
        $iCostID = number_format($iCostID, 2);
        $CheckCash = $rows['0']['vPayment'];
        $vToken = $this->getTokenFromUserTable($iUserID);
        $paymentstatus = $rows['0']['payment_status'];
        
        if ('Cash' == $CheckCash){
$responseData['status'] = "0";
$responseData['data'] = array();
$responseData['message'] = "Cash was selected. Ignorning.";
je($responseData);
        }else if ('Success' == $paymentstatus){
        $responseData['status'] = "0";
            $responseData['data'] = array();
            $responseData['message'] = "Already paid. Ignorning.";
            je($responseData);
        }else{
		Stripe::setApiKey(STR_APIKEY);
        
		$str = strpos($iCostID, '.');
		if($str !== false)
		{		
			$amount	=	str_replace(".","",$iCostID);
		} else {
			$amount	=	$iCostID.'00';
		}
        
		if ($vToken) {
		
			if($iFeedID){
				$strSql	=	"SELECT vCustomerId FROM `tbl_user` WHERE `tbl_user`.`iUserID` = ".$iUserID." ";
			}		
			$query = $this->db->query($strSql);
			$rows = $query->result_array();

			if ($rows[0]['vCustomerId']!='' && $rows[0]['vCustomerId']!='0')  {
				$sql2 = "UPDATE `tbl_user` SET `vToken` = '".$vToken."' WHERE `tbl_user`.`iUserID` = '".$iUserID."' ";
				$query = $this->db->query($sql2);
				$customerId	=	$rows[0]['vCustomerId'];			
			} else {
				// Create a Customer
				$customer = Stripe_Customer::create(array(
                    "card" => $vToken,
                    "email" => $vEmail)
				);
				$sql2 = "UPDATE `tbl_user` SET `vToken` = '".$vToken."', vCustomerId = '".$customer->id."' WHERE `tbl_user`.`iUserID` = '".$iUserID."' ";
				$query = $this->db->query($sql2);
				$customerId	=	$customer->id;	
			}	
			
			try {
				// Charge the Customer instead of the card			
				$charge = Stripe_Charge::create(array(
					"amount" => $amount, // amount in cents, again
					"currency" => "usd",
                    "description" => $iFeedID,
					"customer" => $customerId)
				);						
				
				$sql3  = "UPDATE `tbl_feed` SET `payment_status` = 'Success' WHERE `tbl_feed`.`iFeedID` = '".$iFeedID."' ";
				$query = $this->db->query($sql3);
				$responseData['status'] = "0";
				$responseData['data'] = array();
				$responseData['message'] = "Your payment was successful.";
	
			} catch(Stripe_CardError $e) {
                $this->failedTransaction($iUserID,$iCostID);
				$responseData['status'] = "1";
				$responseData['data'] = array();
				$responseData['message'] = "Payment Error";
			}
	
			$req = array();
			foreach ($responseData as $key => $value) {
				$value = urldecode(stripslashes($value));
				$req[$key] = $value;
				$strcontent.=$key." - ".$value."<BR>";	
			}
		}
         else {
			$responseData['status'] = "1";
			$responseData['data'] = array();
			$responseData['message'] = "No Token found.";
		}
		
        je($responseData);
            
            }
    }
    
    
    function getTokenDataSweden($iFeedID){
// require_once($_SERVER['DOCUMENT_ROOT'].'/application/ws/stripe/lib/Stripe.php');
 require_once(SITE_PATH.'/application/ws/stripe/lib/Stripe.php');
        
    $sql = "SELECT * FROM `tbl_feed` WHERE `iFeedID` = $iFeedID; ";
        $query = $this->db->query($sql);
        
        $rows = $query->result_array();
        
        $iUserID = $rows['0']['iUserID'];
		$iCostID = $rows['0']['vCost'];
        $iCostID = number_format($iCostID, 2);
        $CheckCash = $rows['0']['vPayment'];
        $vToken = $this->getTokenFromUserTable($iUserID);
        $paymentstatus = $rows['0']['payment_status'];
        
        if ('Cash' == $CheckCash){
$responseData['status'] = "0";
$responseData['data'] = array();
$responseData['message'] = "Cash was selected. Ignorning.";
je($responseData);
        }else if ('Success' == $paymentstatus){
        $responseData['status'] = "0";
            $responseData['data'] = array();
            $responseData['message'] = "Already paid. Ignorning.";
            je($responseData);
        }else{
            
            // check if payment status is equal to "success"###
        Stripe::setApiKey(STR_APIKEY);
        
		$str = strpos($iCostID, '.');
		if($str !== false)
		{		
			$amount	=	str_replace(".","",$iCostID);
		} else {
			$amount	=	$iCostID.'00';
		}
        
		if ($vToken) {
		
			if($iFeedID){
				$strSql	=	"SELECT vCustomerId FROM `tbl_user` WHERE `tbl_user`.`iUserID` = ".$iUserID." ";
			}		
			$query = $this->db->query($strSql);
			$rows = $query->result_array();

			if ($rows[0]['vCustomerId']!='' && $rows[0]['vCustomerId']!='0')  {
				$sql2 = "UPDATE `tbl_user` SET `vToken` = '".$vToken."' WHERE `tbl_user`.`iUserID` = '".$iUserID."' ";
				$query = $this->db->query($sql2);
				$customerId	=	$rows[0]['vCustomerId'];			
			} else {
				// Create a Customer
				$customer = Stripe_Customer::create(array(
                    "card" => $vToken,
                    "email" => $vEmail)
				);
				$sql2 = "UPDATE `tbl_user` SET `vToken` = '".$vToken."', vCustomerId = '".$customer->id."' WHERE `tbl_user`.`iUserID` = '".$iUserID."' ";
				$query = $this->db->query($sql2);
				$customerId	=	$customer->id;	
			}	
			
			try {
				// Charge the Customer instead of the card			
				$charge = Stripe_Charge::create(array(
					"amount" => $amount, // amount in cents, again
					"currency" => "sek",
                    "description" => $iFeedID,
					"customer" => $customerId)
				);						
				
				$sql3  = "UPDATE `tbl_feed` SET `payment_status` = 'Success' WHERE `tbl_feed`.`iFeedID` = '".$iFeedID."' ";
				$query = $this->db->query($sql3);
				$responseData['status'] = "0";
				$responseData['data'] = array();
				$responseData['message'] = "Your payment was successful.";
	
			} catch(Stripe_CardError $e) {
                $this->failedTransaction($iUserID,$iCostID);
				$responseData['status'] = "1";
				$responseData['data'] = array();
				$responseData['message'] = "Payment Error";
			}
	
			$req = array();
			foreach ($responseData as $key => $value) {
				$value = urldecode(stripslashes($value));
				$req[$key] = $value;
				$strcontent.=$key." - ".$value."<BR>";	
			}
		}
         else {
			$responseData['status'] = "1";
			$responseData['data'] = array();
			$responseData['message'] = "No Token found.";
		}
		
        je($responseData);
            
            }
}
    
    function getTokenFromUserTable($iUserID){    
    $sql = "SELECT `vToken` FROM `tbl_user` WHERE `iUserID` = ".$iUserID.";"; /// fix
    
 	$query = $this->db->query($sql);
	$rows = $query->row_array();

        
	$vToken = $rows['vToken'];

	return $vToken;
    
    }
    
    function chargeBasedonCountry($iFeedID){
    $sql = "SELECT `vFeedTitle` FROM `tbl_feed` WHERE `iFeedID` = ".$iFeedID.";";
    
 	$query = $this->db->query($sql);
	$rows = $query->row_array();

        
	$userPhone = $rows['vFeedTitle'];
        
    if(strlen($userPhone)==9){
    $this->getTokenDataSweden($iFeedID);
    }else if (strlen($userPhone)==10){
    $this->getTokenData($iFeedID);
    }
        
    }
    
    
    function completedRide () {
    $iFeedID = intVal($this->input->get_post("iFeedID"));
        $sql = "UPDATE `tbl_feed` SET `vUserView` = 'complete' WHERE `tbl_feed`.`iFeedID` = ".$iFeedID.";";
        
        $query = $this->db->query($sql);
        
        $this->chargeBasedonCountry($iFeedID);

}
    
    function pickedupRide(){
        $iFeedID = intVal($this->input->get_post("iFeedID"));
    
        $sql = "UPDATE `tbl_feed` SET `vUserView` = 'pickedup' WHERE `tbl_feed`.`iFeedID` = ".$iFeedID.";";
        
        $query = $this->db->query($sql);
            
        
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Added Successfully";
        
        je($responseData);
    
    }
    
        function applicationAddValues(){
            
        $iUserID = $this->input->get_post("iUserID");
        $fullname = $this->input->get_post("fullname");
        $SSN = $this->input->get_post("SSN");
        $DOB = $this->input->get_post("DOB");
        $licensenumber = $this->input->get_post("licensenumber");
        $image = $this->input->get_post("profileimage");
        $car = $this->input->get_post("carpicture");
        $insurance = $this->input->get_post("inspicture");
        $registration = $this->input->get_post("regpicture");
        $zipcode = $this->input->get_post("zipcode");
        $refferal = $this->input->get_post("refferal");
        $city = $this->input->get_post("city");
        $status = $this->input->get_post("status");
        $typeofcar = $this->input->get_post("typeofcar");
        $platenumber = $this->input->get_post("platenumber");
        $typeofdriver = $this->input->get_post("typeofdriver");
    
        $sql = "INSERT INTO `tbl_applicants` (`ApplicantNr`, `Status`, `DriverID`, `City`, `FullName`, `SSN`, `DOB`, `Zipcode`, `Refferalcode`, `LicenseNumber`, `imageofApplicant`, `VehicleRegPic`, `VehicleInsPic`, `VehiclePicture`, `LicensePlateNr`, `TypeofVehicle`, `TypeofDriver`) VALUES (NULL, '$status', '$iUserID', '$city', '$fullname', '$SSN', '$DOB', '$zipcode', '$refferal', '$licensenumber', '$image', '$registration', '$insurance', '$car', '$platenumber', '$typeofcar', '$typeofdriver');";
          
            
        $query = $this->db->query($sql);
            
        
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Added Successfully";
        
        je($responseData);
    
    }
    
    
            function changeApplicationStatustoPending(){
            
        $iUserID = $this->input->get_post("iUserID");
        $sql = "UPDATE  `tbl_applicants` SET  `Status` =  'pending' WHERE  `tbl_applicants`.`DriverID` = $iUserID;";
            
            
            
        $query = $this->db->query($sql);
            
        
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Added Successfully";
        
        je($responseData);
    
    }
    
 function checkBalance () {
    $iUserID = intVal($this->input->get_post("iUserID"));
         
        $sql = "SELECT * FROM `tbl_user` WHERE `balance` < 0 AND `iUserID` = ".$iUserID.";";

      $query = $this->db->query($sql);
    $rows = $query->num_rows;
     
    if ($rows == 0){
    $responseData['status'] = "0";

$responseData['data'] = array();
$responseData['message'] = "User does not have any negative balance.";
    }else{
$responseData['status'] = "1";

$responseData['data'] = array();
$responseData['balance'] = $this->getTotalBalance($iUserID);
}
    je($responseData);

}
    
 function updateBalance () { // Running in SEReceiptVC
    require_once(SITE_PATH.'/application/ws/stripe/lib/Stripe.php');
            
            // check if payment status is equal to "success"###
        Stripe::setApiKey(STR_APIKEY);
        $iUserID = $this->input->get_post("iUserID");
        $strSql =   "SELECT vCustomerId FROM `tbl_user` WHERE `tbl_user`.`iUserID` = ".$iUserID."";   
        $query = $this->db->query($strSql);
        $rows = $query->result_array();
        if ($rows[0]['vCustomerId']!='' && $rows[0]['vCustomerId']!='0')  {
            
            $customerId =   $rows[0]['vCustomerId'];

            try {
                    $requestID = "Cancel ride";
                    $iCostID = str_replace("$", "", $this->input->get_post("balance"));
                    $str = strpos($iCostID, '.');
                    if($str !== false)
                    {       
                    $amount =   str_replace(".","",$iCostID);
                        } else {
                    $amount =   $iCostID.'00';
                    }

                    // Charge the Customer instead of the card          
                    $charge = Stripe_Charge::create(array(
                        "amount" => $amount, // amount in cents, again
                        "currency" => "usd",
                        "description" => $requestID,
                        "customer" => $customerId)
                    );
                    $responseData['status'] = "0";
                    $responseData['amount'] = $amount;
                    $responseData['customer'] = $customerId;
                    $responseData['data'] = array();
                    $responseData['message'] = "Your payment was successful.";
        
                } catch(Stripe_CardError $e) {
                    $this->failedTransaction($iUserID,$iCostID);
                    

                    $iUserID = intVal($this->input->get_post("iUserID"));
                    $balance = intVal($this->input->get_post("balance")); 
      
                    $sql = "UPDATE  `tbl_user` SET  `balance` =  '$balance' WHERE  `tbl_user`.`iUserID` = ".$iUserID.";";

                    $query = $this->db->query($sql);
                    $responseData['status'] = "1";
                    $responseData['data'] = array();
                    $responseData['message'] = "Payment Error. Updated balance.";

                }
            
        }
        


    
    je($responseData);

}
    
    function failedTransaction ($iUserID,$iCostID) {  //Running in PHP
    $sql = "UPDATE  `tbl_user` SET  `balance` =  '$iCostID' WHERE  `tbl_user`.`iUserID` = ".$iUserID.";";
      $query = $this->db->query($sql);
    }
    

        function testPic () {
            $uploaddir = '../ws/images/drivercars/';
            $file = basename($_FILES['userfile']['name']);
            $uploadfile = $uploaddir . $file;

if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    echo "OK";
} else {
    echo "ERROR";
$dir = dirname(__FILE__);
echo "<p>Full path to this dir: " . $dir . "</p>";
}
}
    
            function uploadCourierPic () {
            $uploaddir = '/var/www/html/ws/images/courierInfo/';
            $file = basename($_FILES['userfile']['name']);
            $uploadfile = $uploaddir . $file;

if (move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile)) {
    echo "OK";
} else {
    echo "ERROR";
$dir = dirname(__FILE__);
echo "<p>Full path to this dir: " . $dir . "</p>";
}
}
    
    
function getTotalBalance($iUserID) {
    $sql = "SELECT * FROM `tbl_user` WHERE `balance` < 0 AND `iUserID` = ".$iUserID.";";
    
 	$query = $this->db->query($sql);
	echo $query;
	$rows = $query->row_array();

        
	$balance = $rows['balance'];

	return $balance;
}
    
    
        function feedback () {
    $iFeedID = intVal($this->input->get_post("iFeedID"));
    $feedback = intVal($this->input->get_post("feedback"));
            
        $sql = "UPDATE `tbl_feed` SET `feedback` = '.$feedback.' WHERE `tbl_feed`.`iFeedID` = ".$iFeedID.";";
        
        $query = $this->db->query($sql);
            
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Added Successfully";
        
        je($responseData);

}
            function coupondelete () {
    $iUserID = intVal($this->input->get_post("iUserID"));
    $coupon = $this->input->get_post("coupon");
            
        $sql = "UPDATE `tbl_user` SET `codefordiscount` = '$coupon' WHERE `tbl_user`.`iUserID` = ".$iUserID.";";
        
        $query = $this->db->query($sql);
            
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Added Successfully";
        
        je($responseData);

}
        
    function deleteRide () {
        $iFeedID = intVal($this->input->get_post("iFeedID"));
        $sql = "UPDATE `tbl_feed` SET `vUserView` = 'delete' WHERE `tbl_feed`.`iFeedID` = ".$iFeedID." AND `vUserView` = 'public' OR `vUserView` = 'friends' OR `vUserView` = 'courier' OR `vUserView` = 'food';";
        
        $query = $this->db->query($sql);
            
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Added Successfully";
        
        je($responseData);
        
}
    
    
function checkdriverRide() {
     $iFeedID = intVal($this->input->get_post("iFeedID"));
    
    $sql = "SELECT * FROM `tbl_feed` WHERE `vUserView` = 'public' AND `iDriverID` = '0' AND `iFeedID` = ".$iFeedID.";";

    $query = $this->db->query($sql);
    $rows = $query->num_rows;
    
    if ($rows == 0){
    $responseData['status'] = "1";

$responseData['data'] = array();
$responseData['message'] = "Already taken!";
    }else{
$responseData['status'] = "0";

$responseData['data'] = array();
$responseData['message'] = "its available and open";
}
    je($responseData);
}
    
    
    	function acceptedRide() {
            $iFeedID = intVal($this->input->get_post("iFeedID"));
            $iDriverID = intVal($this->input->get_post("iDriverID"));
            
            $sql = "UPDATE `tbl_feed` SET `vUserView` = 'friends', `iDriverID` = ".$iDriverID." WHERE `tbl_feed`.`iFeedID` = ".$iFeedID.";";
            
		  $query = $this->db->query($sql);
            
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Added Successfully";
            
            $notificationMsg=$this->getDriverFullName($iDriverID) . " is your new driver!";
            $this->sendPushNotification($this->arrivingPush(), $notificationMsg, $generatePayloadData, $notificationType);
            
			je($responseData);
            
            

}
    
    function pushdeletedride(){
    
             $iFeedID = intVal($this->input->get_post("iFeedID"));
            $iDriverID = intVal($this->input->get_post("iDriverID"));

            $notificationMsg=$this->getDriverFullName($iDriverID) . " has canceled your Ride. Please exit the application and request a new one :)";
            $this->sendPushNotification($this->arrivingPush(), $notificationMsg, $generatePayloadData, $notificationType);
        
            $responseData['status'] = "1";

			$responseData['data'] = array();

			$responseData['message'] = "Accepted Successfully";
        
			je($responseData);
        
    }
    
    function testAcceptRideApple (){
            
            $sql = "UPDATE `tbl_feed` SET `vUserView` = 'friends', `iDriverID` = 3508, `currentLat` =  '37.377720', `currentLong` =  '-121.837500' WHERE `tbl_feed`.`iUserID` = 3508";
            
		  $query = $this->db->query($sql);
        
        if ($query == 1){
			$responseData['status'] = "1";

			$responseData['data'] = array();

			$responseData['message'] = "Accepted Successfully";
        
			je($responseData);
        }else if ($query == 0){
            $responseData['status'] = "0";
			$responseData['data'] = array();

			$responseData['message'] = "Something went wrong..";
        }
    
    }
    
    function testCompleteApple (){
                $sql = "UPDATE `tbl_feed` SET `vUserView` = 'complete' WHERE `tbl_feed`.`iUserID` = 519";
            
		  $query = $this->db->query($sql);
        
        if ($query == 1){
			$responseData['status'] = "1";

			$responseData['data'] = array();

			$responseData['message'] = "Marked as complete!";
        
			je($responseData);
        }else if ($query == 0){
            $responseData['status'] = "0";
			$responseData['data'] = array();

			$responseData['message'] = "Something went wrong..";
        }
    
    }

    function arrivingDriver(){
        $iFeedID = $this->input->get_post('iFeedID');
        
        $sql = "SELECT `iUserID` FROM tbl_feed WHERE `iFeedID` = $iFeedID; ";
        $query = $this->db->query($sql);
        
        $rows = $query->result_array();
            
        foreach($rows as $row) {
                    $this->$row['iUserID'];
//                    echo $rows['iUserID'];
                }
        
        return $row['iUserID'];
    }
    
    function arrivingPush() {
            $iUserID = $this->arrivingDriver();
            $sql = "SELECT `vDeviceToken` from tbl_user where `iUserID` = $iUserID; ";
            $query = $this->db->query($sql); 
            $rows = $query->result_array();
        
        foreach($rows as $row) {
                    $this->$row['vDeviceToken'];
//                    echo $rows['iUserID'];
                }
        
//            echo $row['vDeviceToken'];
            return $row['vDeviceToken'];
            //$ridetoken = $rows['vDeviceToken'];
            //return $ridetoken;
        }
    
    function arrivingPushing() {           
                $devToken = $this->arrivingPush();
                $notificationMsg="Your driver is approaching!";  
                $this->sendPushNotification($devToken, $notificationMsg, $generatePayloadData, $notificationType);
        }
    
    /////
    
    
    
        function messageDriver() {
            $iUserID = $this->input->get_post('iUserID');
            $sql = "SELECT `vDeviceToken` from tbl_user where `iUserID` = $iUserID; ";
            $query = $this->db->query($sql); 
            $rows = $query->result_array();
        
        foreach($rows as $row) {
                    $this->$row['vDeviceToken'];
                    echo $rows['iUserID'];
                }
            
//            echo $row['vDeviceToken'];
            return $row['vDeviceToken'];
            //$ridetoken = $rows['vDeviceToken'];
            //return $ridetoken;
        }
    
    function sendMessage() {           
                $devToken = $this->messageDriver();
                $notificationMsg= $this->input->get_post('message');  
                $this->sendPushNotification($devToken, $notificationMsg, $generatePayloadData, $notificationType);
        echo "Message sent successfully!";
        }
    
    
    //////
function getNearstDriversByType()
    {
        $fLat = $this->input->get_post("fLat");
        $fLong = $this->input->get_post("fLong");
        $typeofdriver = $this->input->get_post("typeofdriver");
        $sql="SELECT `iUserID`,`fLat`,`fLong`,(((acos(sin((".$fLat."*pi()/180)) * 
                sin((`fLat`*pi()/180))+cos((".$fLat."*pi()/180)) * 
                cos((`fLat`*pi()/180)) * cos(((".$fLong."- `fLong`)* 
                pi()/180))))*180/pi())*60*1.1515
            ) as distance FROM `tbl_user` INNER JOIN `tbl_applicants`
                ON tbl_user.iUserID = tbl_applicants.driverID
                WHERE tbl_user.vDriverorNot = 'driver' AND tbl_user.onlineornot = 'online' AND tbl_applicants.typeofDriver = '$typeofdriver' ORDER BY distance ASC";
            
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        if ($query->num_rows > 0)
        {
            $responseData['status'] = "0";
            $responseData['data'] = $rows[0];
            $responseData['message'] = "Nearest Driver Info";
        } else {
            $responseData['status'] = "1";
            $responseData['data'] = array();
            $responseData['message'] = "No driver found";
        }
      je($responseData);
    }
   function recievePymentRequest() {

        $senderID = $this->input->get_post("senderID");
        $phoneNumber = $this->input->get_post("phoneNumber");
        $requestID = $this->input->get_post("requestID");
        $sql = "UPDATE  `tbl_request_payment` SET  `paymentStatus` =  'recieve' WHERE  `tbl_request_payment`.`senderID` = ".$senderID." AND `tbl_request_payment`.`recieverPhone` = ".$phoneNumber." AND `tbl_request_payment`.`id` = ".$requestID.";";
        $query = $this->db->query($sql);
            
        $responseData['status'] = "0";
        $responseData['data'] = "OK!";
        $responseData['message'] = "Received";
        
        je($responseData);
    }
   function acceptedPymentRequest() {
        require_once(SITE_PATH.'/application/ws/stripe/lib/Stripe.php');
            
            // check if payment status is equal to "success"###
        Stripe::setApiKey(STR_APIKEY);
        $iUserID = $this->input->get_post("userID");
        $strSql =   "SELECT vCustomerId FROM `tbl_user` WHERE `tbl_user`.`iUserID` = ".$iUserID."";   
        $query = $this->db->query($strSql);
        $rows = $query->result_array();
        if ($rows[0]['vCustomerId']!='' && $rows[0]['vCustomerId']!='0')  {
            
            $customerId =   $rows[0]['vCustomerId'];

            try {
                    $requestID = $this->input->get_post("requestID");
                    $iCostID = str_replace("$", "", $this->input->get_post("vCost"));
                    $str = strpos($iCostID, '.');
                    if($str !== false)
                    {       
                    $amount =   str_replace(".","",$iCostID);
                        } else {
                    $amount =   $iCostID.'00';
                    }

                    // Charge the Customer instead of the card          
                    $charge = Stripe_Charge::create(array(
                        "amount" => $amount, // amount in cents, again
                        "currency" => "usd",
                        "description" => $requestID,
                        "customer" => $customerId)
                    );                   

                    $senderID = $this->input->get_post("senderID");
                    $phoneNumber = $this->input->get_post("phoneNumber");
                    $requestID = $this->input->get_post("requestID");
                    $sql = "UPDATE  `tbl_request_payment` SET  `paymentStatus` =  'accepted' WHERE  `tbl_request_payment`.`senderID` = ".$senderID." AND `tbl_request_payment`.`recieverPhone` = ".$phoneNumber." AND `tbl_request_payment`.`id` = ".$requestID.";";
                    $query = $this->db->query($sql);
                    $responseData['status'] = "0";
                    $responseData['amount'] = $amount;
                    $responseData['customer'] = $customerId;
                    $responseData['data'] = array();
                    $responseData['message'] = "Your payment was successful.";
        
                } catch(Stripe_CardError $e) {
                    $this->failedTransaction($iUserID,$iCostID);
                    $responseData['status'] = "1";
                    $responseData['data'] = array();
                    $responseData['message'] = "Payment Error";
                }
            
        }
        
        je($responseData);
    }

   function canceledPymentRequest() {
        $senderID = $this->input->get_post("senderID");
        $phoneNumber = $this->input->get_post("phoneNumber");
        $requestID = $this->input->get_post("requestID");
        $sql = "UPDATE  `tbl_request_payment` SET  `paymentStatus` =  'canceled' WHERE  `tbl_request_payment`.`senderID` = ".$senderID." AND `tbl_request_payment`.`recieverPhone` = ".$phoneNumber." AND `tbl_request_payment`.`id` = ".$requestID.";";
        $query = $this->db->query($sql);
            
        $responseData['status'] = "0";
        $responseData['data'] = "OK!";
        $responseData['message'] = "Canceled";
        
        je($responseData);
    }

    function getPymentStatusForRequest() {
        $senderID = $this->input->get_post("senderID");
        $phoneNumber = $this->input->get_post("phoneNumber");
        $iFeedID = $this->input->get_post('iFeedID');
        $iUserID = $this->input->get_post('iDriverID');
        $requestID = $this->input->get_post("requestID");
        $sql = "SELECT `paymentStatus` FROM  `tbl_request_payment` WHERE  `tbl_request_payment`.`senderID` = ".$senderID." AND `tbl_request_payment`.`recieverPhone` = ".$phoneNumber." AND `tbl_request_payment`.`id` = ".$requestID.";";
            
        $query = $this->db->query($sql);

        $rows = $query->result_array();
        foreach($rows as $row) {
                    $this->$row['paymentStatus'];
                }
        $responseData['status'] = $row['paymentStatus'];        
        je($responseData);
    }

    function requestPayment() {
        $statusChanged = $this->general_model->getDBDateTime();
        $recieverPhone = $this->input->get_post('iRecieverPhone');
        $method = $this->input->get_post('vPaymentMethod');
        $sql = "INSERT INTO `tbl_sms` (`messageStatus`,`messageSid`, `errorCode`, `statusChanged`) VALUES ('getPaymentRequest', '$recieverPhone', '$method', '$statusChanged');";
        $query = $this->db->query($sql);
        
        $senderID = $this->input->get_post('iSenderID');
        $cost = str_replace("$", "", $this->input->get_post('vCost'));
        $requstCreated = $this->general_model->getDBDateTime();
        $sql = "INSERT INTO `tbl_request_payment` (`senderID`, `recieverPhone`, `paymentMethod`, `paymentStatus`, `vCost`, `createdTimeStamp`) VALUES ('$senderID', '$recieverPhone', '$method', 'send', '$cost', '$requstCreated');";
        $query = $this->db->query($sql);

        $sql = "SELECT `id` FROM `tbl_request_payment` WHERE `senderID` = '$senderID' AND `recieverPhone` = '$recieverPhone'  AND `paymentMethod` =  '$method' AND `paymentStatus` = 'send';";
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        foreach($rows as $row) {
                    $this->$row['id'];
                }
        $responseData['requestID'] = $row['id']; 
        

        $iRecieverID = $this->getUserIDviaPhone($this->input->get_post('iRecieverPhone'));
        if($iRecieverID == "") {
            $this -> SMSInvite($this->input->get_post('iRecieverPhone'));
        } else {
            $senderRecords = $this->user_model->getUserByID($this->input->get_post('iSenderID'));
            $receiverRecords = $this->user_model->getUserByID($iRecieverID);
            $devToken = $this->getDeviceTokenForPush($iRecieverID);
            if (!empty($senderRecords)) {
                if ($method == "Copay") {
                    $notificationMsg = $senderRecords['vFirst'] . " " . $senderRecords['vLast'] . " has invited you to split payment for a ride";
                } else {
                    $notificationMsg = $senderRecords['vFirst'] . " " . $senderRecords['vLast'] . " has invited you to pay for a ride";
                }
                $generatePayloadData['iSenderID'] = $senderRecords['iUserID'];
                $generatePayloadData['iRecieverPhone'] = $this->input->get_post('iRecieverPhone');
                $generatePayloadData['requestID'] = $responseData['requestID'];
                $generatePayloadData['vCost'] = $this->input->get_post('vCost');
                $generatePayloadData['vPaymentMethod'] = $this->input->get_post('vPaymentMethod');
                $generatePayloadData['vSenderFirstName'] = $senderRecords['vFirst'];
                $generatePayloadData['vSenderLastName'] = $senderRecords['vLast'];
                $generatePayloadData['iSenderProfileImage'] = $senderRecords['vImage'];
                $notificationType = "requestPayment";    
                $this->sendPushNotification($devToken, $notificationMsg, $generatePayloadData, $notificationType);
                $responseData['status'] = "1";
                $responseData['message'] = "Message sent successfully!\n";
                je($responseData);
            }
        }
                
    }
    function getUserIDviaPhone($userPhone){
    $sql = "SELECT * FROM `tbl_user` WHERE `userPhone` = ".$userPhone.";";
        $query = $this->db->query($sql); 
        $rows = $query->result_array();
        foreach($rows as $row) {
                    $this->$row['iUserID'];
                }
        $iUserID = $row['iUserID'];
        if ($row == 0){
            $statusChanged = $this->general_model->getDBDateTime();
            $sql = "INSERT INTO `tbl_sms` (`messageStatus`,`messageSid`, `errorCode`, `statusChanged`) VALUES ('didntFindUserID', '$iUserID', 'sendSMS', '$statusChanged');";
            $query = $this->db->query($sql);
            return "";
        }else{
            $statusChanged = $this->general_model->getDBDateTime();
            $sql = "INSERT INTO `tbl_sms` (`messageStatus`,`messageSid`, `errorCode`, `statusChanged`) VALUES ('findUserID', '$iUserID', 'sendPushNotification', '$statusChanged');";
            $query = $this->db->query($sql);
            return $iUserID;
        }
    }
   function SMSInvite($userPhone){
      // require_once($_SERVER['DOCUMENT_ROOT'].'/application/ws/Services/Twilio.php');
      require_once(SITE_PATH.'/application/ws/Services/Twilio.php');
      $account_sid = 'ACa87347dbf2a530214ae6c0ce2a94640f'; 
      $auth_token = '07bb4d3e5f4b8549617d77592a8fee9a'; 
      $client = new Services_Twilio($account_sid, $auth_token);   
      $errorMessage = null;
      try {
        $senderRecords = $this->user_model->getUserByID($this->input->get_post('iSenderID'));
        $method = $this->input->get_post('vPaymentMethod');
        if ($method == "Copay") {
                    $bodyMsg = $senderRecords['vFirst'] . " " . $senderRecords['vLast'] . " has invited you to split payment for a ride. Please login to the app to make payment: http://www.pullova.com";
                } else {
                    $bodyMsg = $senderRecords['vFirst'] . " " . $senderRecords['vLast'] . " has invited you to pay for a ride. Please login to the app to make payment: http://www.pullova.com";
                }
        
        $client->account->messages->create(array( 
            'To' => $userPhone, 
            'From' => "+14259708430", 
            'Body' => $bodyMsg,   
            'StatusCallback' => "http://mafiathepartygame.com/pullova/Backend/ws/user/SMSInviteCallback"
        ));

      } catch (Exception $e) {
        $errorMessage = $e->getMessage();
      }
      if (empty($errorMessage)) {
        $method = $this->input->get_post('vPaymentMethod');
        if ($method == "Copay") {
                    $bodyMsg = $senderRecords['vFirst'] . " " . $senderRecords['vLast'] . " has invited you to split payment for a ride. Please login to the app to make payment: http://www.pullova.com";
                } else {
                    $bodyMsg = $senderRecords['vFirst'] . " " . $senderRecords['vLast'] . " has invited you to pay for a ride. Please login to the app to make payment: http://www.pullova.com";
                }

        $responseData['status'] = "0";
        $responseData['data'] = array();
        $responseData['message'] = $bodyMsg;
        $statusChanged = $this->general_model->getDBDateTime();
        $message = $responseData['message'];
        $sql = "INSERT INTO `tbl_sms` (`messageStatus`,`messageSid`, `errorCode`, `statusChanged`) VALUES ('created', '$userPhone', '$message', '$statusChanged');";
        $query = $this->db->query($sql);
      } else {
        $responseData['status'] = "1";
        $responseData['data'] = array();
        $responseData['message'] = $errorMessage;
        $statusChanged = $this->general_model->getDBDateTime();
        $errorCode = str_replace("'", "", $errorMessage);
        $sql = "INSERT INTO `tbl_sms` (`messageStatus`,`messageSid`, `errorCode`, `statusChanged`) VALUES ('createdWithError', '$userPhone', '$errorCode', '$statusChanged');";
        $query = $this->db->query($sql);
      }
      je($responseData);
    }
    function SMSInviteCallback(){
            
        $MessageStatus = $this->input->get_post("MessageStatus");
        $MessageSid = $this->input->get_post("MessageSid");
        $ErrorCode = $this->input->get_post("ErrorCode");
        $statusChanged = $this->general_model->getDBDateTime();
        $sql = "INSERT INTO `tbl_sms` (`messageStatus`,`messageSid`, `errorCode`, `statusChanged`) VALUES ('$MessageStatus', '$MessageSid', '$ErrorCode', '$statusChanged');";
            
        $query = $this->db->query($sql);
    
    }

    ///////
        function messageAll() {           
                $rows = $this->sendAll();
                $notificationMsg= $this->input->get_post('allMessage'); 
            foreach($rows as $row) {
                    $this->sendPushNotification($row['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);
//                    echo $rows['vDeviceToken'];
//                echo $row['vDeviceToken'];
                }
            echo "Message(s) sent successfully!";
        }

    
        function sendAll() {
            $sql = "SELECT `vDeviceToken` from tbl_user";
            $query = $this->db->query($sql); 
            $rows = $query->result_array();
            
            return $rows;
            //$ridetoken = $rows['vDeviceToken'];
            //return $ridetoken;
        }
    
    
    //////
    
    
    function updateDriverEarnings(){
            $earnings = $this->input->get_post("earnings");
            $iDriverID = $this->input->get_post("iDriverID");
            
            $sql = "UPDATE  `tbl_user` SET  `vDriverEarnings` =  `vDriverEarnings` + ".$earnings." WHERE  `tbl_user`.`iUserID` = ".$iDriverID.";";
        
		  $query = $this->db->query($sql);
            
			$responseData['status'] = "0";

			$responseData['data'] = "OK!";

			$responseData['message'] = "Added Successfully";
        
            je($responseData);
    
    }
    
    function getDriverEarnings(){
            $iDriverID = $this->input->get_post("iDriverID");
            $sql = "SELECT `vDriverEarnings` from tbl_user where `iUserID` = $iDriverID; ";
            $query = $this->db->query($sql); 
            $rows = $query->result_array();
        foreach($rows as $row) {
                    $this->$row['vDriverEarnings'];
                }
            
            if (!$row['vDriverEarnings']){
            $responseData['status'] = "0";
			$responseData['data'] = "0";
			$responseData['message'] = "Received successfully!";
            }else{
            $responseData['status'] = "0";
			$responseData['data'] = $row['vDriverEarnings'];
			$responseData['message'] = "Received successfully!";
            }
            je($responseData);
    }
    
        function updateDriverBank(){
            $routingnr = $this->input->get_post("routingnr");
            $iDriverID = $this->input->get_post("iDriverID");
            $accountnr = $this->input->get_post("accountnr");
            $nameofbank = $this->input->get_post("nameofbank");
            
            $sql = "UPDATE  `tbl_user` SET  `vDriverRoutingNr` =  ".$routingnr.", `vDriverAccountNr` = ".$accountnr.", `vDriverNameofBank` = '$nameofbank' WHERE  `tbl_user`.`iUserID` =".$iDriverID.";";
            
		  $query = $this->db->query($sql);
            
			$responseData['status'] = "0";

			$responseData['data'] = "OK!";

			$responseData['message'] = "Added Successfully";
        
            je($responseData);
    }
    
    
    function getCurrentPrices(){
         
        $sql = "SELECT * FROM `tbl_tripCost`";
        
        $query = $this->db->query($sql);
        $rows = $query->result_array();
        
        
        $responseData['status'] = "0";

		$responseData['data'] = $rows;

		$responseData['message'] = "Success";
        
        je($responseData);
        
    }
        
        
    
            function updatePaypalAddress(){
            $iDriverID = $this->input->get_post("iDriverID");
            $paypal = $this->input->get_post("paypal");
            
            $sql = "UPDATE  `tbl_user` SET  `vDriverPaypal` = '$paypal' WHERE  `tbl_user`.`iUserID` =".$iDriverID.";";
            
		  $query = $this->db->query($sql);
            
			$responseData['status'] = "0";

			$responseData['data'] = "OK!";

			$responseData['message'] = "Added Successfully";
        
            je($responseData);
    }
    
            function updateApplicationStatusPending() {
            $iUserID = intVal($this->input->get_post("iUserID"));
    
        $sql = "UPDATE  `tbl_user` SET  `vDriverorNot` =  'pending' WHERE  `tbl_user`.`iUserID` = ".$iUserID.";";
        
        $query = $this->db->query($sql);
            
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Added Successfully";
        
        je($responseData);
        
        }
    
    
        function getDeviceTokenForPush($iUserID) {
	$this->db->select('vDeviceToken', false);
	$this->db->from(TBL_USER);
	$this->db->where("iUserID", $iUserID);
 	$query = $this->db->get();
	
	$rows = $query->row_array();
	$token = $rows['vDeviceToken'];

	return $token;
}
    
    
    function SMSVerification(){
      // require_once($_SERVER['DOCUMENT_ROOT'].'/application/ws/Services/Twilio.php');
      require_once(SITE_PATH.'/application/ws/Services/Twilio.php');
      $account_sid = 'ACa87347dbf2a530214ae6c0ce2a94640f'; 
      $auth_token = '07bb4d3e5f4b8549617d77592a8fee9a'; 
      $client = new Services_Twilio($account_sid, $auth_token); 
      $userPhone = $this->input->get_post('userPhone');
              
              
      $randomnumber = $this->generaterandom();
              
      $errorMessage = null;
      try {
        $client->account->messages->create(array( 
        	'To' => $userPhone, 
        	'From' => "+14259708430", 
        	'Body' => "Your verification code is: ".$randomnumber."",   
        ));
      } catch (Exception $e) {
        $errorMessage = $e->getMessage();
      }

      if (empty($errorMessage)) {
        $responseData['status'] = "0";
        
        $responseData['data'] = array();
        
        $responseData['message'] = "$randomnumber";
      } else {
        $responseData['status'] = "1";
        
        $responseData['data'] = array();
        
        $responseData['message'] = $errorMessage;
      }
      
      je($responseData);

    }
    
    function SMSVerificationSweden(){
      // require_once($_SERVER['DOCUMENT_ROOT'].'/application/ws/Services/Twilio.php');
      require_once(SITE_PATH.'/application/ws/Services/Twilio.php');
      $account_sid = 'ACa87347dbf2a530214ae6c0ce2a94640f'; 
      $auth_token = '07bb4d3e5f4b8549617d77592a8fee9a'; 
      $client = new Services_Twilio($account_sid, $auth_token); 
      $userPhone = $this->input->get_post('userPhone');
              
              
      $randomnumber = $this->generaterandom();
              
      try {
        $client->account->messages->create(array( 
        	'To' => $userPhone, 
        	'From' => "+14259708430", 
        	'Body' => "Din verifieringskod är: ".$randomnumber."",   
        ));
      } catch (Exception $e) {
        $errorMessage = $e->getMessage();
      }
              
      if (empty($errorMessage)) {
        $responseData['status'] = "0";
        
        $responseData['data'] = array();
        
        $responseData['message'] = "$randomnumber";
      } else {
        $responseData['status'] = "1";
        
        $responseData['data'] = array();
        
        $responseData['message'] = $errorMessage;
      }
      je($responseData);

    }
    
    function generaterandom(){
    $random = rand(1000,9000);
    return $random;
    }
    
    function SMSVerifiedPhone() {
            $userPhone = $this->input->get_post('userPhone');
            $sql = "UPDATE  `tbl_user` SET  `verifiedPhone` =  '1' WHERE  `userPhone` = ".$userPhone.";";
            
		$query = $this->db->query($sql);
            
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Updated Successfully!";

			je($responseData);

}
    
    function getUserInfoviaPhone(){
     $userPhone = $this->input->get_post('userPhone');
    $sql = "SELECT * FROM `tbl_user` WHERE `userPhone` = ".$userPhone.";";
            
		$query = $this->db->query($sql);
           
        $rows = $query->result_array();
        
        foreach($rows as $row) {
                    $this->$row['vPassword'];
                }
        
        $vPassword = $row['vPassword'];
        
        if ($row == 0){
        $responseData['status'] = "1";

			$responseData['data'] = array();

			$responseData['message'] = "No user registered!";

			je($responseData);
        }else{
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = $vPassword;

			je($responseData);
        }
        
    
    }
    
    function getUserEmailViaPhone(){
        
     $userPhone = $this->input->get_post('userPhone');
    $sql = "SELECT * FROM `tbl_user` WHERE `userPhone` = ".$userPhone.";";
            
		$query = $this->db->query($sql);
           
        $rows = $query->result_array();
        
        foreach($rows as $row) {
                    $this->$row['vEmail'];
                }
        
        $vEmail = $row['vEmail'];
        
        if ($row == 0){
        $responseData['status'] = "1";

			$responseData['data'] = array();

			$responseData['message'] = "No user registered!";

			je($responseData);
        }else{
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = $vEmail;

			je($responseData);
        }
        
        
    }
    
    
    function getDriverFullName($iDriverID) {
	$this->db->select('vFirst, vLast', false);
 
	$this->db->from(TBL_USER);
	$this->db->where("iUserID", $iDriverID);
 	$query = $this->db->get();
	
	$rows = $query->row_array();

        
	$fullname = $rows['vFirst'] . ' ' . $rows['vLast'];


	return $fullname;
}
    
    
        	function driverLocation() {
            
            $sql = "UPDATE `tbl_feed` SET `currentLat` = ".$this->input->get_post("currentLat").", `currentLong` = ".$this->input->get_post("currentLong")." WHERE `tbl_feed`.`iFeedID` = ".$this->input->get_post("iFeedID").";";
            
		$query = $this->db->query($sql);
            
			$responseData['status'] = "0";

			$responseData['data'] = array();

			$responseData['message'] = "Added Successfully";

			je($responseData);

//		}

}
    
    function getFeed()
    {
        je($this->user_model->getFeedByID($this->input->get_post('iFeedID')));
    }
    
    function getDriver()
    {
        $iDriverID = $this->input->get_post('iDriverID');
        $driver_detail = $this->user_model->getUserByID( $iDriverID );
        je( $driver_detail );
    }
    
    function isitcanceled()
    {
        $iFeedID = $this->input->get_post('iFeedID');
        $iUserID = $this->input->get_post('iDriverID');
        $sql = "SELECT `vUserView` FROM  `tbl_feed` WHERE  `iFeedID` = $iFeedID AND `iDriverID` = $iUserID";
            
		  $query = $this->db->query($sql);

        $rows = $query->result_array();

			je($rows);
    }
    

	function doComment() {
		if ($this->input->get_post('iFeedID') !='' && $this->input->get_post('iUserID') != '' && $this->input->get_post('Comment') != '' ) {
			$postData = $_REQUEST;
			$postData['UserId'] = $this->input->get_post('iUserID');
			$postData['FeedId'] = $this->input->get_post('iFeedID');
			$postData['PostedDate'] = $this->general_model->getDBDateTime();
			$getResult = $this->user_model->addComment($postData);
			$feedList['FeedArray'] = $this->user_model->getFeedByID($this->input->get_post('iFeedID'));			
			$feedList['CommentArray'] =	$this->user_model->doGetCommentDetailsByFeedId($this->input->get_post('iFeedID'));
												
			foreach($feedList['CommentArray'] as $key=>$comments) {
				$userArray = $this->user_model->getUserDataByID($comments['UserId']);
				$feedList['CommentArray'][$key]['iUserID']	=	$userArray['iUserID'];
				$feedList['CommentArray'][$key]['vFirst']	=	$userArray['vFirst'];		
				$feedList['CommentArray'][$key]['vLast']	=	$userArray['vLast'];	
				$feedList['CommentArray'][$key]['original']	=	PROFILE_IMAGE_URL . $userArray['vImage'];
				$feedList['CommentArray'][$key]['thumb']	=	PROFILE_THUMB_URL . $userArray['vImage'];				
			}
			je($feedList);
		}
	}

	function get_highlight_post() {	
			
        if ($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') {
            $feedList = $this->user_model->doGetHighlighedPost($this->input->get_post('iUserID'));
			$response	=	array();
			$noRecords 	=	0;
			$records	=	0;
			$k = 0;
            if ($feedList) {
                foreach ($feedList as $v) {
					$likeCnt	=	$this->user_model->doGetTodayLikeCountByFeedId($v['iFeedID']);
					$comments	=	$this->user_model->doGetTodayCommentDetailsByFeedId($v['iFeedID']);
					
					if ($likeCnt > 0 || $comments) {
						$response[$k]['iFeedID'] =	$v['iFeedID'];
						$response[$k]['type'] =	$v['type'];
						$response[$k]['vUserView'] =	$v['vUserView'];
						$response[$k]['videofile'] =	$v['videofile'];
						$response[$k]['vFeedTitle'] =	$v['vFeedTitle'];					
						$response[$k]['tFeedDescription'] =	$v['tFeedDescription'];
						
						$response[$k]['vFeedMapImage'] = (isset($v['vFeedMapImage']) && $v['vFeedMapImage'] != '' && file_exists(FEED_MAP_IMAGE_PATH . $v['vFeedMapImage'])) ? FEED_MAP_IMAGE_URL . $v['vFeedMapImage'] : "";					
						$response[$k]['vFeedItemImage'] = (isset($v['vFeedItemImage']) && $v['vFeedItemImage'] != '' && file_exists(FEED_ITEM_IMAGE_PATH . $v['vFeedItemImage'])) ? FEED_ITEM_IMAGE_URL . $v['vFeedItemImage'] : "";
						$response[$k]['dCreatedDate'] =	$v['dCreatedDate'];
						$response[$k]['iUserID'] =	$v['iUserID'];
						$response[$k]['vUsername'] =	$v['vUsername'];
						$response[$k]['vEmail'] =	$v['vEmail'];
						$response[$k]['vFirst'] =	$v['vFirst'];
						$response[$k]['vLast'] =	$v['vLast'];
						$response[$k]['vImage'] = (isset($v['vImage']) && $v['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $v['vImage'])) ? PROFILE_THUMB_URL . $v['vImage'] : "";
						$response[$k]['eLogin'] =	$v['eLogin'];
						$response[$k]['dLoginDate'] =	$v['dLoginDate'];
						$response[$k]['vFbID'] =	$v['vFbID'];					
						$response[$k]['fLat'] =	$v['fLat'];
						$response[$k]['toLong'] =	$v['toLong'];		
						$response[$k]['LikeStatus'] = $this->user_model->doGetUserFeedLikeStatus($v['iFeedID'],$v['iUserID']);
						$response[$k]['LikeCount']	=	$likeCnt;
						$response[$k]['DislikeCount'] = $this->user_model->doGetDislikeCountByFeedId($v['iFeedID']);
						$response[$k]['CommentArray'] = $this->user_model->doGetTodayCommentDetailsByFeedId($v['iFeedID']);
						foreach($response[$k]['CommentArray'] as $key=>$comments) {
							$userArray = $this->user_model->getUserDataByID($comments['UserId']);
							$response[$k]['CommentArray'][$key]['iUserID']	=	$userArray['iUserID'];
							$response[$k]['CommentArray'][$key]['vFirst']	=	$userArray['vFirst'];		
							$response[$k]['CommentArray'][$key]['vLast']	=	$userArray['vLast'];	
							$response[$k]['CommentArray'][$key]['original']	=	PROFILE_IMAGE_URL . $userArray['vImage'];
							$response[$k]['CommentArray'][$key]['thumb']	=	PROFILE_THUMB_URL . $userArray['vImage'];				
						}
						$tagFriends = $this->user_model->getFeedTagFriendInfo($v['iFeedID']);
						if ($tagFriends > 0) {
							foreach ($tagFriends as $feedKey => $feedVal) {
								$response[$k]['feedTagFriends'][$feedKey]['iUserID'] = $feedVal['iUserID'];
								$response[$k]['feedTagFriends'][$feedKey]['vFirst'] = $feedVal['vFirst'];
								$response[$k]['feedTagFriends'][$feedKey]['vLast'] = $feedVal['vLast'];
								$response[$k]['feedTagFriends'][$feedKey]['vImage'] = (isset($feedVal['vImage']) && $feedVal['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $feedVal['vImage'])) ? PROFILE_THUMB_URL . $feedVal['vImage'] : "";
							}
						} else {
							$response[$k]['feedTagFriends'] = array();
						}
						$records++;
						$k++;
					} else {
						$noRecords++;
					}
					
                }				
				if($records > 0) {
					$responseData['status'] = "0";
					$responseData['data'] = $response;
					$responseData['message'] = "Feed list";
				} else {
					$responseData['status'] = "1";
					$responseData['data'] = array();
					$responseData['message'] = "No Records Found";
				}                    
			} else {
				$responseData['status'] = "1";
				$responseData['data'] = array();
				$responseData['message'] = "Something going wrong, please try again";
			}
//print "<pre>"; print_r($responseData); exit;
       		je($responseData);
		}	
	}
	
    //Swedish Charge Canceled Ride
    
    function chargeCanceledRideSweden($iFeedID,$cancelCost){
// require_once($_SERVER['DOCUMENT_ROOT'].'/application/ws/stripe/lib/Stripe.php');
 require_once(SITE_PATH.'/application/ws/stripe/lib/Stripe.php');
        
    $sql = "SELECT * FROM `tbl_feed` WHERE `iFeedID` = $iFeedID; ";
        $query = $this->db->query($sql);
        
        $rows = $query->result_array();
        
        $iUserID = $rows['0']['iUserID'];
		$iCostID = $cancelCost;
        $iCostID = number_format($iCostID, 2);
        $CheckCash = $rows['0']['vPayment'];
        $vToken = $this->getTokenFromUserTable($iUserID);
        $paymentstatus = $rows['0']['payment_status'];
        
        if ('Cash' == $CheckCash){
$responseData['status'] = "0";
$responseData['data'] = array();
$responseData['message'] = "Cash was selected. Ignorning.";
je($responseData);
        }else if ('Success' == $paymentstatus){
        $responseData['status'] = "0";
            $responseData['data'] = array();
            $responseData['message'] = "Already paid. Ignorning.";
            je($responseData);
        }else{
            
            // check if payment status is equal to "success"###
        Stripe::setApiKey(STR_APIKEY);
        
		$str = strpos($iCostID, '.');
		if($str !== false)
		{		
			$amount	=	str_replace(".","",$iCostID);
		} else {
			$amount	=	$iCostID.'00';
		}
        
		if ($vToken) {
		
			if($iFeedID){
				$strSql	=	"SELECT vCustomerId FROM `tbl_user` WHERE `tbl_user`.`iUserID` = ".$iUserID." ";
			}		
			$query = $this->db->query($strSql);
			$rows = $query->result_array();

			if ($rows[0]['vCustomerId']!='' && $rows[0]['vCustomerId']!='0')  {
				$sql2 = "UPDATE `tbl_user` SET `vToken` = '".$vToken."' WHERE `tbl_user`.`iUserID` = '".$iUserID."' ";
				$query = $this->db->query($sql2);
				$customerId	=	$rows[0]['vCustomerId'];			
			} else {
				// Create a Customer
				$customer = Stripe_Customer::create(array(
                    "card" => $vToken,
                    "email" => $vEmail)
				);
				$sql2 = "UPDATE `tbl_user` SET `vToken` = '".$vToken."', vCustomerId = '".$customer->id."' WHERE `tbl_user`.`iUserID` = '".$iUserID."' ";
				$query = $this->db->query($sql2);
				$customerId	=	$customer->id;	
			}	
			
			try {
				// Charge the Customer instead of the card			
				$charge = Stripe_Charge::create(array(
					"amount" => $amount, // amount in cents, again
					"currency" => "sek",
                    "description" => $iFeedID,
					"customer" => $customerId)
				);						
				
				$sql3  = "UPDATE `tbl_feed` SET `payment_status` = 'Success' WHERE `tbl_feed`.`iFeedID` = '".$iFeedID."' ";
				$query = $this->db->query($sql3);
				$responseData['status'] = "0";
				$responseData['data'] = array();
				$responseData['message'] = "Your payment was successful.";
	
			} catch(Stripe_CardError $e) {
                $this->failedTransaction($iUserID,$iCostID);
				$responseData['status'] = "1";
				$responseData['data'] = array();
				$responseData['message'] = "Payment Error";
			}
	
			$req = array();
			foreach ($responseData as $key => $value) {
				$value = urldecode(stripslashes($value));
				$req[$key] = $value;
				$strcontent.=$key." - ".$value."<BR>";	
			}
		}
         else {
			$responseData['status'] = "1";
			$responseData['data'] = array();
			$responseData['message'] = "No Token found.";
		}
		
        je($responseData);
            
            }
}
    
	// Update Token

       function chargeCanceledRide($iFeedID,$cancelCost){
// require_once($_SERVER['DOCUMENT_ROOT'].'/application/ws/stripe/lib/Stripe.php');
 require_once(SITE_PATH.'/application/ws/stripe/lib/Stripe.php');
        
    $sql = "SELECT * FROM `tbl_feed` WHERE `iFeedID` = $iFeedID; ";
        $query = $this->db->query($sql);
        
        $rows = $query->result_array();
        
        $iUserID = $rows['0']['iUserID'];
		$iCostID = $cancelCost;
        $iCostID = number_format($iCostID, 2);
        $CheckCash = $rows['0']['vPayment'];
        $vToken = $this->getTokenFromUserTable($iUserID);
        $paymentstatus = $rows['0']['payment_status'];
        
        if ('Cash' == $CheckCash){
$responseData['status'] = "0";
$responseData['data'] = array();
$responseData['message'] = "Cash was selected. Ignorning.";
je($responseData);
        }else if ('Success' == $paymentstatus){
        $responseData['status'] = "0";
            $responseData['data'] = array();
            $responseData['message'] = "Already paid. Ignorning.";
            je($responseData);
        }else{
            
            // check if payment status is equal to "success"###
        Stripe::setApiKey(STR_APIKEY);
        
		$str = strpos($iCostID, '.');
		if($str !== false)
		{		
			$amount	=	str_replace(".","",$iCostID);
		} else {
			$amount	=	$iCostID.'00';
		}
        
		if ($vToken) {
		
			if($iFeedID){
				$strSql	=	"SELECT vCustomerId FROM `tbl_user` WHERE `tbl_user`.`iUserID` = ".$iUserID." ";
			}		
			$query = $this->db->query($strSql);
			$rows = $query->result_array();

			if ($rows[0]['vCustomerId']!='' && $rows[0]['vCustomerId']!='0')  {
				$sql2 = "UPDATE `tbl_user` SET `vToken` = '".$vToken."' WHERE `tbl_user`.`iUserID` = '".$iUserID."' ";
				$query = $this->db->query($sql2);
				$customerId	=	$rows[0]['vCustomerId'];			
			} else {
				// Create a Customer
				$customer = Stripe_Customer::create(array(
                    "card" => $vToken,
                    "email" => $vEmail)
				);
				$sql2 = "UPDATE `tbl_user` SET `vToken` = '".$vToken."', vCustomerId = '".$customer->id."' WHERE `tbl_user`.`iUserID` = '".$iUserID."' ";
				$query = $this->db->query($sql2);
				$customerId	=	$customer->id;	
			}	
			
			try {
				// Charge the Customer instead of the card			
				$charge = Stripe_Charge::create(array(
					"amount" => $amount, // amount in cents, again
					"currency" => "usd",
                    "description" => $iFeedID,
					"customer" => $customerId)
				);						
				
				$sql3  = "UPDATE `tbl_feed` SET `payment_status` = 'Success' WHERE `tbl_feed`.`iFeedID` = '".$iFeedID."' ";
				$query = $this->db->query($sql3);
				$responseData['status'] = "0";
				$responseData['data'] = array();
				$responseData['message'] = "Your payment was successful.";
	
			} catch(Stripe_CardError $e) {
                $this->failedTransaction($iUserID,$iCostID);
				$responseData['status'] = "1";
				$responseData['data'] = array();
				$responseData['message'] = "Payment Error";
			}
	
			$req = array();
			foreach ($responseData as $key => $value) {
				$value = urldecode(stripslashes($value));
				$req[$key] = $value;
				$strcontent.=$key." - ".$value."<BR>";	
			}
		}
         else {
			$responseData['status'] = "1";
			$responseData['data'] = array();
			$responseData['message'] = "No Token found.";
		}
		
        je($responseData);
            
            }
}
    
    // Swedish Update Token
    
    function checkifRunningorNot($iFeedID){
    $sql = "SELECT `vUserView` FROM `tbl_feed` WHERE `iFeedID` = ".$iFeedID.";";
    
 	$query = $this->db->query($sql);
	$rows = $query->row_array();

        
	$vUserView = $rows['vUserView'];
        
        
    return $vUserView;
}
    
    function updateTokenSweden () {
        $cancelCost = $this->input->get_post("cost");
        $iFeedID = $this->input->get_post("feedId");
        $vUserView = $this->checkifRunningorNot($iFeedID);
        if ('pickedup' == $vUserView){
            $responseData['status'] = "0";
            $responseData['data'] = array();
            $responseData['message'] = "Can't cancel Ride.. It's in progress.";
            je($responseData);
        }else{
        if ($cancelCost==5){
        $this->chargeCanceledRideSweden($iFeedID,$cancelCost);
        }else{
        $this->getTokenDataSweden($iFeedID);
        }
        }
	}
    
     
	function updateToken () {
        $cancelCost = $this->input->get_post("cost");
        $iFeedID = $this->input->get_post("feedId");
        $vUserView = $this->checkifRunningorNot($iFeedID);
        if ('pickedup' == $vUserView){
            $responseData['status'] = "0";
            $responseData['data'] = array();
            $responseData['message'] = "Can't cancel Ride.. It's in progress.";
            je($responseData);
        }else{
        if ($cancelCost==5){
        $this->chargeCanceledRide($iFeedID,$cancelCost);
        }else{
        $this->getTokenData($iFeedID);
        }
    }
}

    
    function sendEmailInfo () {
        $vEmail = $this->input->get_post("userEmail");
        
        /// here need code that
        /// sends user email...
        
        // need sample email from Lance of what
        /// the email should say
        
        
        
        $responseData['status'] = "0";
		$responseData['data'] = $vEmail;
		$responseData['message'] = "Sent info!";				
        je($responseData);
    }

	function updateUserToken () {
        // require_once($_SERVER['DOCUMENT_ROOT'].'/application/ws/stripe/lib/Stripe.php');
        require_once(SITE_PATH.'/application/ws/stripe/lib/Stripe.php');
            
        $iUserID = intVal($this->input->get_post("iUserID"));
        $vToken = $this->input->get_post("token");
                $vEmail = $this->input->get_post("vEmail");
        Stripe::setApiKey(STR_APIKEY);
        // Get the credit card details submitted by the form
        try {   
        $customer = Stripe_Customer::create(array(
            "source" => $vToken,
            "email" => $vEmail)
        );
        } catch (Exception $e) {
            $responseData['status'] = "1";
            $responseData['data'] = array();
            $responseData['message'] = $e->getMessage();               
            je($responseData);
        }
        $customerId =   $customer->id;
        $sql4 = "UPDATE `tbl_user` SET `vToken` = '".$vToken."', vCustomerId = '".$customer->id."' WHERE `tbl_user`.`iUserID` = '".$iUserID."' ";
        $query = $this->db->query($sql4);

        $req = array();
        foreach ($responseData as $key => $value) {
            $value = urldecode(stripslashes($value));
            $req[$key] = $value;
            $strcontent.=$key." - ".$value."<BR>";  
        }
                
        $responseData['status'] = "0";
        $responseData['data'] = array();
        $responseData['message'] = "Token Added successful.";               
        je($responseData);
    }




}