<?php

class Changepassword extends MY_Controller {

   function __construct() {
        parent::__construct();
        $this->load->model('user_model');
    }

    function index() {
        
        if ($this->input->get_post('vOldPassword') && $this->input->get_post('vNewPassword')) {
            $this->load->model("user_model");
            $postData['iUserID'] = $this->input->get_post('iUserID');
            $postData['vPassword'] = md5($this->input->get_post('vOldPassword'));

            $checkPassword = $this->user_model->checkOldPassword($postData);

            if ($checkPassword) {
                $postData['vPassword'] = md5($this->input->get_post('vNewPassword'));
                $updatePassword = $this->user_model->changePassword($postData);

                if ($updatePassword) {
                    $responseData['status'] = "0";
                    $responseData['data'] = array();
                    $responseData['message'] = $this->lang->line("PASSWORD_CHANGED");
                } else {
                    $responseData['status'] = "1";
                    $responseData['data'] = array();
                    $responseData['message'] = $this->lang->line("PASSWORD_NOT_CHANGED");
                }
            } else {
                $responseData['status'] = "1";
                $responseData['data'] = array();
                $responseData['message'] = $this->lang->line("OLD_PASSWORD_NOT_OK");
            }
        } else {
            $responseData['status'] = "1";
            $responseData['data'] = array();
            $responseData['message'] = "Something going wrong, Please try later";
        }
        je($responseData);
    }

}

/*
      | -------------------------------------------------------------------
      |  END OF CLASS FILE
      | -------------------------------------------------------------------
     */
    