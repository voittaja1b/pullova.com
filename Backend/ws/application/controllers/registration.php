<?php

/*
 * 
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Registration extends MY_Controller {

    var $responseData = array();

    function __construct() {
        parent::__construct();
// parse_str($_SERVER['QUERY_STRING'], $_POST);
        $this->load->library("email");
        $this->load->library('encrypt');
    }

    function index() {
        if (isset($_REQUEST) && $_REQUEST != "") {
            $this->load->model("user_model");
            extract($_REQUEST);

            if (isset($vFbID) && $vFbID != "") {
                $postData['vFbID'] = $vFbID;
                $user_login = $this->user_model->checkLoginByFbIDAndDeviceToken($vFbID, $vDeviceToken);
                
            }
            if (isset($user_login) && !empty($user_login)) {
                $postData['eLogin'] = "1";
                $postData['dLoginDate'] = $this->general_model->getDBDateTime();
                $update_status = $this->user_model->editUserDataByEmailOrUsernameOrfbID($postData);
                if (isset($vFbID) && $vFbID != "") {
                    $user_detail = $this->user_model->getUserDataByFbID($user_login);
                }
            } else {
                if (isset($vEmail) && $vEmail != "") {
                    if (!$this->user_model->checkUserEmailAvailable($vEmail)) {
                        $responseData['status'] = "5";
                        $responseData['data'] = array();
                        $responseData['message'] = "Email address already exists.";
                        je($responseData);
                    }
                }
                if (isset($userPhone) && $userPhone != "") {
                    if (!$this->user_model->checkPhoneNumber($userPhone)) {
                        $responseData['status'] = "6";
                        $responseData['data'] = array();
                        $responseData['message'] = "Phonenumber is already registered.";
                        je($responseData);
                    }
                }
                if (isset($vUsername) && $vUsername != "") {
                    if (!$this->user_model->checkUsernameAvailable($vUsername)) {
                        $responseData['status'] = "4";
                        $responseData['data'] = array();
                        $responseData['message'] = "Username already exists.";
                        je($responseData);
                    }
                }
                $postData = $_REQUEST;
                if ($_FILES && $_FILES['vImage']['name'] != '') {
                    $this->load->library('imageUpload/upload', $_FILES['vImage']);
                    $handleImage = new Upload($_FILES['vImage']);

                    if ($handleImage->uploaded) {
                        $handleImage->allowed = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
                        $handleImage->Process(PROFILE_IMAGE_PATH);

                        $handleImage->allowed = array('image/png', 'image/jpeg', 'image/jpg', 'image/gif');
                        $handleImage->image_resize = true;
                        $handleImage->image_ratio_crop = true;
                        $handleImage->image_x = 190;
                        $handleImage->image_y = 190;
                        $handleImage->Process(PROFILE_THUMB_PATH);
                        if ($handleImage->processed) {
                            $hr_newname = $handleImage->file_dst_name_body;
                            $hr_newname .= '.' . $handleImage->file_dst_name_ext;
                            $postData['vImage'] = $hr_newname;
                        } else {
                            $responseData['status'] = "1";
                            $responseData['data'] = array();
                            $responseData['message'] = $handleImage->error;
                            je($responseData);
                        }
                    } else {
                        $responseData['status'] = "1";
                        $responseData['data'] = array();
                        $responseData['message'] = $handleImage->error;
                        je($responseData);
                    }
                }

                if (isset($postData['vPassword'])) {
                    $postData['vPassword'] = md5($postData['vPassword']);
                }
                $postData['eUserType'] = "User";
                $postData['dCreateDate'] = $this->general_model->getDBDateTime();
                $postData['vActivationKey'] = $key = md5(microtime() . rand());
                //$postData['vActivationKey'] = $this->encrypt->encode($postData['vEmail']);
                
                $userAdd = $this->user_model->addUser($postData);

                if ($userAdd != '') {
                    //$sendMail = $this->sendActivationMailToUser($userAdd);
//                if (!$sendMail) {
//                    $responseData['activation_mail_status'] = "0";
//                }else{
//                    $responseData['activation_mail_status'] = "1";
//                }
                    $to = $postData['vEmail'];
                    $postEditData['eLogin'] = "1";
                    $postEditData['iUserID'] = $userAdd;
                    $postEditData['dLoginDate'] = $this->general_model->getDBDateTime();
                    $update_status = $this->user_model->editUserDataByEmailOrUsernameOrfbID($postEditData);
                    $user_detail = $this->user_model->getUserDataByID($userAdd);
                    
                     if($user_detail['questcount']==NULL)
                        $user_detail['questcount']='0';
                    if($user_detail['followingcount']==NULL)
                        $user_detail['followingcount']='0';
                    if($user_detail['followercount']==NULL)
                        $user_detail['followercount']='0';
                    
                     
                    
                    
                }
            }
            if (isset($user_detail) && !is_null($user_detail)) {
                $user_arr = array();

                 $user_detail['profileImage']=array('original'=>(isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_IMAGE_PATH.$user_detail['vImage']))?PROFILE_IMAGE_URL.$user_detail['vImage']:"",
                        'thumb'=>(isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_THUMB_PATH.$user_detail['vImage']))?PROFILE_THUMB_URL.$user_detail['vImage']:"",
                        );
//                  $userArray['questcount'] = $counts['questcount'];
//                $userArray['followingcount'] = $counts['followingcount'];
//                $userArray['followercount'] = $counts['followercount']; 
                $responseData['status'] = "0";
                $responseData['data'] = $user_detail;
                $responseData['message'] = "You are successfully logged in ";
            } else {
                $responseData['status'] = "1";
                $responseData['data'] = array();
                $responseData['message'] = "Something going wrong, Please try later";
            }

            je($responseData);
        }
    }

    function sendActivationMailToUser($iUserID) {

        $query = $this->db->get_where(TBL_USER, array('iUserID' => $iUserID));

        if ($query->num_rows() > 0) {

            $row = $query->row();

            $getTemplate = $this->general_model->getEmailTemplate('3');

            $strEmail1 = str_replace('\"', '"', $getTemplate->email_body);
            $strEmail1 = str_replace('\r\n', '', $strEmail1);
//            $strEmail1 = str_replace('[header]', '', $strEmail1);

            $strEmail1 = str_replace('&nbsp;', '', $strEmail1);
            $strEmail1 = str_replace('[year]', date('Y'), $strEmail1);
            $strEmail1 = str_replace('[MEMBER_NAME]', $row->vFirst . " " . $row->vLast, $strEmail1);
            $strEmail1 = str_replace('[display verification link]', DOMAIN_URL . "/ws/activate/?key=" . $row->vActivationKey, $strEmail1);
            $strEmail1 = str_replace('[link]', DOMAIN_URL, $strEmail1);
            $strEmail1 = str_replace('[SITE_URL]', DOMAIN_URL, $strEmail1);
            $this->email->initialize(unserialize(EMAIL_CONFIG));
            $this->email->set_newline("\r\n");
            $this->email->from($getTemplate->sender_email, $getTemplate->sender_name);
            $this->email->reply_to($getTemplate->reply_email);
            $this->email->to($row->vEmail);
            $this->email->subject($getTemplate->subject);
            $this->email->message($strEmail1);
            //echo $this->email->print_debugger(); die;


            if ($this->email->send())
                return 1;
            else
                return 0;
        }
    }

}

?>
