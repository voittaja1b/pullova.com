<?php
	/*
	 * To change this template, choose Tools | Templates
	 * and open the template in the editor.
	*/
	
	class Message extends MY_Controller {	
		var $viewData = array();
					
		function __construct() {
			parent::__construct();
			$this->load->model('user_model');
		}
			
		function send_message() {
			$postData['iUserID']		=	$this->input->get_post('iUserID');
			$postData['otherUserId']	=	$this->input->get_post('otherUserId');
			$postData['message']		=	$this->input->get_post('message');
			$postData['addeddate']		=	$this->general_model->getDBDateTime();
			
			$messageID = $this->user_model->addMessage($postData);
            if ($messageID > 0) {
                $responseData['status'] = "0";
                $responseData['message'] = "Message added successfully";
            } else {
                $responseData['status'] = "1";
                $responseData['message'] = "Error";
            }
            je($responseData);					
		}
		
		function get_messages() {
			$responseData1 = $this->user_model->getMessageLists($this->input->get_post('iUserID'), $this->input->get_post('otherUserId'));
			if($responseData1) {
				$display = array();
				$inc = 0;
				foreach ($responseData1 as $response) {
					$display[$inc]['ident']	=	$response['ident'];
					$display[$inc]['iUserID']	=	$response['iUserID'];
					$display[$inc]['otherUserId']	=	$response['otherUserId'];
					$display[$inc]['message']	=	$response['message'];
					$display[$inc]['addeddate']	=	$response['addeddate'];					
					$inc++;
				}						
				$responseData['status'] = "0";
				$responseData['data'] = $display;
				$responseData['message'] = "Message list";
            	je($responseData);
			}	 else {
				$responseData['status'] = "1";
                $responseData['data'] = array();
                $responseData['message'] = "No Records found";
				je($responseData);	
			}			
		}


	}
?>