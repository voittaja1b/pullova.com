<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of connection
 *
 * @author yudiz
 */
class Connection extends MY_Controller {

    //put your code here

    function __construct() {
        parent::__construct();
        $this->load->model("connection_model");
        //$this->load->library("apn");
    }

    function friend_list() {
        if ($this->input->get_post("iUserID") && $this->input->get_post("iUserID") != "") {
            $get_friend = $this->connection_model->getMemberFriendByID($this->input->get_post("iUserID"));
            $friend_arr = array();
            if ($get_friend != "") {
                foreach ($get_friend as $key => $row) {
                    $row['eStatus'] = 'Accepted';

                    $friend_arr[$key] = $row;
                    foreach ($row as $k => $v) {
                        if (in_array($k, unserialize(IMAGE_DICTONARY))) {
                            if (file_exists(PROFILE_IMAGE_PATH . $v) AND $v != "")
                                $friend_arr[$key][$k] = PROFILE_IMAGE_URL . $v;
                            else
                                $friend_arr[$key][$k] = '';
                        }

                        if ($k == 'iThreadID' && $v == '') {
                            $friend_arr[$key]['iThreadID'] = "";
                        }
                    }
                }
            }
            $responseData['status'] = "0";
            $responseData['data'] = $friend_arr;
            $responseData['message'] = "Friend List";
            je($responseData);
        } else {
            $responseData['status'] = "0";
            $responseData['data'] = array();
            $responseData['message'] = "No friends found";
        }
    }

    function pending_list() {
        if ($this->input->get_post("iUserID") && $this->input->get_post("iUserID") != "") {
            $pending_arr = array();
            $pending_list = $this->connection_model->getFriendRequestByID($this->input->get_post("iUserID"));
            if ($pending_list != "") {
                foreach ($pending_list as $key => $row) {
                    $pending_arr[$key] = $row;
                    foreach ($row as $k => $v) {
                        if (in_array($k, unserialize(IMAGE_DICTONARY))) {
                            if (file_exists(PROFILE_IMAGE_PATH . $v) AND $v != "")
                                $pending_arr[$key][$k] = PROFILE_IMAGE_URL . $v;
                            else
                                $pending_arr[$key][$k] = '';
                        }
                    }
                }
                $responseData['status'] = "0";
                $responseData['data'] = $pending_arr;
                $responseData['message'] = "Pending Lists";
                je($responseData);
            }else {
                $responseData['status'] = "0";
                $responseData['data'] = array();
                $responseData['message'] = "No pending list found";
                je($responseData);
            }
        }
    }

    function accept() {
        if ($this->input->get_post("iConnectionID") && $this->input->get_post("iConnectionID") != "" && $this->input->get_post("iFriendID") && $this->input->get_post("iFriendID") != "") {
            $accept = $this->connection_model->acceptRequest($this->input->get_post("iConnectionID"), $this->input->get_post("iFriendID"));
            if ($accept) {
                $get_friend = $this->connection_model->getFriendDetailByID($this->input->get_post("iConnectionID"));

                $friend_arr = array();
                if ($get_friend != "") {
                    foreach ($get_friend as $key => $row) {
                        $friend_arr[$key] = $row;

                        if (in_array($key, unserialize(IMAGE_DICTONARY))) {
                            if (file_exists(PROFILE_IMAGE_PATH . $row) && $row != "")
                                $friend_arr[$key] = PROFILE_IMAGE_URL . $row;
                        }
                    }
                }
                $senderData = $this->connection_model->getSenderDataByConnectionID($this->input->get_post("iConnectionID"));
                $friendData = $this->connection_model->getFriendData($this->input->get_post("iFriendID"));

                $notificationMsg = $friendData['vFirst'] . " " . $friendData['vLast'] . " has accepted your friend request";

                $notificationType = "response";
                $generatePayloadData = '';
                $this->sendPushNotification($senderData['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);
                $this->sendNotificationAndroid(GOOGLE_API_KEY, $senderData['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);
                $responseData['status'] = "0";
                $responseData['data'] = $friend_arr;
                $responseData['message'] = 'Friend Request accepted successfully';
            } else {
                $responseData['status'] = "1";
                $responseData['data'] = array();
                $responseData['message'] = 'Problem accepting the friend request';
            }
        } else {
            $responseData['status'] = "1";
            $responseData['data'] = array();
            $responseData['message'] = "Something going wrong, Please try again";
        }
        je($responseData);
    }

    function unfriend() {
        if ($this->input->get_post("iConnectionID") && $this->input->get_post("iConnectionID") != "" && $this->input->get_post("iUserID") && $this->input->get_post("iUserID") != "") {
            $unfriend = $this->connection_model->unFriendByID($this->input->get_post("iConnectionID"), $this->input->get_post("iUserID"));
            if ($unfriend) {
                $responseData['status'] = "0";
                $responseData['data'] = array();
                $responseData['message'] = "This member no longer part of your connections";
            } else {
                $responseData['status'] = "1";
                $responseData['data'] = array();
                $responseData['message'] = "Unfriend problem with user, please try again";
            }
        } else {
            $responseData['status'] = "1";
            $responseData['data'] = array();
            $responseData['message'] = "Unfriend problem with user, please try again";
        }
        je($responseData);
    }

    function reject() {

        if ($this->input->get_post("iConnectionID") && $this->input->get_post("iConnectionID") != "" && $this->input->get_post("iFriendID") && $this->input->get_post("iFriendID") != "") {
            $reject = $this->connection_model->rejectRequest($this->input->get_post("iConnectionID"), $this->input->get_post("iFriendID"));
            if ($reject) {
                $senderData = $this->connection_model->getSenderDataByConnectionID($this->input->get_post("iConnectionID"));
                $friendData = $this->connection_model->getFriendData($this->input->get_post("iFriendID"));

                $notificationMsg = $friendData['vFirst'] . " " . $friendData['vLast'] . " has rejected your friend request";

                $notificationType = "response";
                $generatePayloadData = '';
                $this->sendPushNotification($senderData['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);
                $this->sendNotificationAndroid(GOOGLE_API_KEY, $senderData['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);

                $responseData['status'] = "0";
                $responseData['data'] = array();
                $responseData['message'] = "Motion dismissed with success";
            } else {
                $responseData['status'] = "1";
                $responseData['data'] = array();
                $responseData['message'] = "Problem in the rejection of the request, please try again";
            }
        } else {
            $responseData['status'] = "1";
            $responseData['data'] = array();
            $responseData['message'] = "Problem in the rejection of the request, please try again";
        }
        je($responseData);
    }

    function send_request() {
        if ($this->input->get_post("iFriendID") && !empty($_REQUEST["iFriendID"]) &&
                $this->input->get_post("iSenderID") && $this->input->get_post("iSenderID") != "") {

            $postData = $_REQUEST;
           // $is_firnd = is_friend($postData['iSenderID'], $postData['iFriendID']);
//            if($is_firnd){
//                
//            }
            $i = 0;
            foreach ($postData['iFriendID'] as $iFriendID) {
                $postData['eStatus'] = "0";
                $postData['iFriendID'] = $iFriendID;
                $postData['dCreatedDate'] = $this->general_model->getDBDateTime();
                $rej_friend = $this->connection_model->rejectedFriend($this->input->get_post("iSenderID"), $iFriendID);
                if ($rej_friend != '') {
                    $send_request = $this->connection_model->editFriendshipStatus($rej_friend['iConnectionID'], $rej_friend['iFriendID']);
                } else {
                    $send_request = $this->connection_model->addFriend($postData);
                }
                $friendData[$i] = $this->connection_model->getFriendData($iFriendID);
                $friendData[$i]['iConnectionID'] = $send_request;
                $senderData = $this->connection_model->getSenderDataByID($this->input->get_post("iSenderID"));
                $i++;
            }
            if ($send_request != "") {
                if (isset($friendData) && !empty($friendData) && isset($senderData) && !empty($senderData)) {
                    foreach ($friendData as $friend) {
                        $notificationMsg = $senderData['vFirst'] . " " . $senderData['vLast'] . " sent you friend request";
                        $generatePayloadData['iFriendID'] = $friend["iUserID"];
                        $generatePayloadData['dCreatedDate'] = $this->general_model->getDBDateTime();
                        $generatePayloadData['isFriend'] = 'yes';
                        $generatePayloadData['iConnectionID'] = $friend['iConnectionID'];
                        $notificationType = "request";
                        $this->sendPushNotification($friend['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);
                        $this->sendNotificationAndroid(GOOGLE_API_KEY, $friend['vDeviceToken'], $notificationMsg, $generatePayloadData, $notificationType);
                    }
                }
                $responseData['status'] = "0";
                $responseData['data'] = array();
                $responseData['message'] = "Your connection request has been sent";
            } else {
                $responseData['status'] = "1";
                $responseData['data'] = array();
                $responseData['message'] = "Problem in sending friend request to the user, please try again";
            }
        } else {
            $responseData['status'] = "1";
            $responseData['data'] = array();
            $responseData['message'] = "Problem in sending friend request to the user, please try again";
        }
        je($responseData);
    }

    function send_fb_request() {
        if ($this->input->get_post("iSenderID") && $this->input->get_post("iSenderID") != "" &&
                $this->input->get_post("vFbID")) {

            $succ = array();
            $cnt = 0;
            foreach ($this->input->get_post("vFbID") as $key => $row) {
                $user_id = $this->connection_model->getUserByFbID($row);
                if ($user_id != "") {
                    if ($user_id['iUserID'] != $this->input->get_post("iSenderID")) {
                        $flag = TRUE;
                        $get_friend = $this->connection_model->getPendingAndAcceptedFriendLists($user_id['iUserID']);
                        if ($get_friend != "") {
                            $user_arr = array();
                            foreach ($get_friend as $key => $value) {
                                $user_arr[] = $value['iUserID'];
                            }
                            if (in_array($this->input->get_post("iSenderID"), $user_arr)) {
                                $flag = FALSE;
                            }
                        }
                        if ($flag) {
                            $postData['iFriendID'] = $user_id['iUserID'];
                            $postData['iSenderID'] = $this->input->get_post("iSenderID");
                            $postData['eStatus'] = "0";
                            $postData['dCreatedDate'] = $this->general_model->getDBDateTime();

                            $send_request = $this->connection_model->addFriend($postData);

                            $friendData = $this->connection_model->getFriendData($user_id['iUserID']);
                            $senderData = $this->connection_model->getSenderDataByID($this->input->get_post("iSenderID"));

                            if ($send_request != "") {
                                if (isset($friendData) && !empty($friendData) && isset($senderData) && !empty($senderData)) {
                                    $notificationMsg = $senderData['vFirst'] . " " . $senderData['vLast'] . " Want to be Your Friend";
                                    $generatePayloardData['iSenderID'] = $senderData['iUserID'];
                                    $generatePayloardData['dCreatedDate'] = $this->general_model->getDBDateTime();
                                    $notificationType = "request";
                                    $this->sendPushNotification($friendData['vDeviceToken'], $notificationMsg, $generatePayloardData, $notificationType);
                                    $succ[] = $cnt++;
                                }
                            } else {
                                $err[] = $cnt++;
                            }
                        }
                    }
                }
            }
            if (!$flag) {
                $responseData['status'] = "0";
                $responseData['data'] = NULL;
                $responseData['message'] = "You have already send connection request ";
            } else {
                if (isset($succ) && !empty($succ)) {
                    $responseData['status'] = "0";
                    $responseData['data'] = NULL;
                    $responseData['message'] = "Your connection request has been sent";
                } else {
                    $responseData['status'] = "1";
                    $responseData['data'] = NULL;
                    $responseData['message'] = "Problem in sending friend request to the user, please try again";
                }
            }
        } else {
            $responseData['status'] = "1";
            $responseData['data'] = NULL;
            $responseData['message'] = "Problem in sending friend request to the user, please try again";
        }
        je($responseData);
    }

	function remove_friend() {
		$postData['iUserID']		=	$this->input->get_post('iUserID');
		$postData['frienduserid']	=	$this->input->get_post('frienduserid');				
		$friend	=	$this->connection_model->doRemoveFriend($postData);
				
		if ($friend > 0) {
			$responseData['status'] = "0";
			$responseData['message'] = "Friend removed successfully";
		} else {
			$responseData['status'] = "1";
			$responseData['message'] = "Error";
		}
		je($responseData);	
	}

}
