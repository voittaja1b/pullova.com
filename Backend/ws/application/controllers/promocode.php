<?php
class PromoCode extends MY_Controller {
    function __construct() {
        parent::__construct();
        $this->load->model('promo_code_model');
    }
	
	function verify() {
        $this->load->model('promo_code_model');
		$vPromoCode = $this->input->get_post("promoCode");
		$vPhoneNumber = $this->input->get_post("phoneNumber");
		if ($vPromoCode != '' && $vPhoneNumber != '') {
			if ($this->promo_code_model->checkCode($vPromoCode, $vPhoneNumber)) {
                $responseData['status'] = "0";
                $responseData['data'] = "";
                $responseData['message'] = "Valite code";
			} else {
                $responseData['status'] = "1";
                $responseData['data'] = "";
                $responseData['message'] = "Invalite code";
			}
		} else {
		  
                $responseData['status'] = "2";
                $responseData['data'] = "";
                $responseData['message'] = "Invalite code";
		}
            je($responseData);
	}
	
	function getData() {
    $this->load->model('promo_code_model');
		$vPromoCode = $this->input->get_post("vPromoCode");
		if ($vPromoCode != '') {
		  $data = $this->promo_code_model->getData($vPromoCode);
		  if (isset($data)) {
                $responseData['status'] = "0";
                $responseData['data'] = $data;
                $responseData['message'] = "Valite code";
			} else {
                $responseData['status'] = "1";
                $responseData['data'] = "";
                $responseData['message'] = "Invalite code";
			}
		} else {
                $responseData['status'] = "2";
                $responseData['data'] = "";
                $responseData['message'] = "Invalite code";
		}
            je($responseData);
	}
}