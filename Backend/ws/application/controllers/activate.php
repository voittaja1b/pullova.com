<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Activate
 *
 * @author yudiz
 */
class Activate extends MY_Controller {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    function index() {
        // CHECK IF URL IS NOT WRONG
        if ($this->input->get("key") != '') {
            $this->load->model("user_model");
            $vActivationKey = $this->input->get("key");

            $check = $this->user_model->checkActivationKey($vActivationKey);
            if ($check != '') {
                $postdata['vActivate'] = $vActivationKey;
                $postdata['eStatus'] = 'Active';
                $postdata['vActivationKey'] = 'expire';
                $activate = $this->user_model->activateUser($postdata);
                if ($activate) {
                    $jsondata['status'] = '0';
                    $jsondata['message'] = 'successfully activated';
                }
            } else {
                $jsondata['status'] = '1';
                $jsondata['message'] = 'Activation key expired / wrong activation key';
            }
            echo je($jsondata);
        }
    }

}

?>
