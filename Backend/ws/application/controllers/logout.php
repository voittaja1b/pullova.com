<?php

class Logout extends MY_Controller {

    var $viewData = array();

    function __construct() {
        parent::__construct();
    }

    function index() {
        $this->session->unset_userdata();
        $this->session->sess_destroy();
    }

}
