<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Login extends MY_Controller {

    var $responseData = array();

    function __construct() {
        parent::__construct();
    }

    function index() {
        if (($this->input->get_post("vPassword") && $this->input->get_post("vPassword") != "") &&
                (
                ($this->input->get_post("vUsername") && $this->input->get_post("vUsername") != "") ||
                ($this->input->get_post("vEmail") && $this->input->get_post("vEmail") != "")
                )
        ) {
            $this->load->model("user_model");
            if (isset($_REQUEST['vUsername']) && $_REQUEST['vUsername'] != "") {
                if (filter_var($_REQUEST['vUsername'], FILTER_VALIDATE_EMAIL)) {

                    $_REQUEST['vEmail'] = $_REQUEST['vUsername'];
                    unset($_REQUEST['vUsername']);
                }
            }
            extract($_REQUEST);
            if (isset($vDeviceToken) && $vDeviceToken != '') {
                if(isset($vUsername) && $vUsername != ""){
                    $loginID['vUsername'] = $vUsername;
                }elseif(isset($vEmail) && $vEmail != ""){
                    $loginID['vEmail'] = $vEmail;
                }elseif(isset($vFbID) && $vFbID != ""){
                    $loginID['vFbID'] = $vFbID;
                }
                $user = $this->user_model->getUserID($loginID);
                $postData['vDeviceToken'] = $vDeviceToken;
                $postData['iUserID'] = $user['iUserID'];
                $this->user_model->addDeviceToken($postData);
            }
            if (isset($vUsername) && $vUsername != "") {
                $postData['vUsername'] = $vUsername;
                $user_login = $this->user_model->checkLoginByUsernameAndDeviceToken($vUsername, $vPassword, $vDeviceToken);
            } else if (isset($vEmail) && $vEmail != "") {
                $postData['vEmail'] = $vEmail;
                $user_login = $this->user_model->checkLoginByEmailAndDeviceToken($vEmail, $vPassword, $vDeviceToken);
            } else if (isset($vFbID) && $vFbID != "") {
                $postData['vFbID'] = $vFbID;
                $user_login = $this->user_model->checkLoginByFbIDAndDeviceToken($vFbID, $vDeviceToken);
            }
            
            if ($user_login) {
                $postData['eLogin'] = "1";

                $postData['dLoginDate'] = $this->general_model->getDBDateTime();
                $update_status = $this->user_model->editUserDataByEmailOrUsernameOrfbID($postData);
                if (isset($vUsername) && $vUsername != "") {
                    $user_detail = $this->user_model->getUserDataByUsername($vUsername);
                } else if (isset($vEmail) && $vEmail != "") {
                    $user_detail = $this->user_model->getUserDataByEmail($vEmail);
                }
                if (isset($user_detail)) {
                    $user_arr = array();

//                    $user_detail['profileImage'] = array('original' => (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_IMAGE_PATH . $user_detail['vImage'])) ? PROFILE_IMAGE_URL . $user_detail['vImage'] : "",
//                        'thumb' => (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $user_detail['vImage'])) ? PROFILE_THUMB_URL . $user_detail['vImage'] : "",
//                    );
                    $user_detail['profileImage']['original'] = (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_IMAGE_PATH . $user_detail['vImage'])) ? PROFILE_IMAGE_URL . $user_detail['vImage'] : "";
                    
                    $user_detail['profileImage']['thumb'] = (isset($user_detail['vImage']) && $user_detail['vImage'] != '' && file_exists(PROFILE_THUMB_PATH . $user_detail['vImage'])) ? PROFILE_THUMB_URL . $user_detail['vImage'] : "";

                    unset($user_detail['vImage']);
                    $responseData['status'] = "0";
                    $responseData['data'] = $user_detail;
                    $responseData['message'] = "You are successfully logged in ";
                } else {
                    $responseData['status'] = "1";
                    $responseData['data'] = array();
                    $responseData['message'] = "Your membership has expired. Please contact support.";
                }
            } else {

                $responseData['status'] = "2";
                $responseData['data'] = array();
                $responseData['message'] = "Your membership has expired. Please contact support.";
            }
        } else {
            $responseData['status'] = "3";
            $responseData['data'] = array();
            $responseData['message'] = "Something going wrong, Please try later";
        }

        je($responseData);
    }

//http://localhost/wizit/ws/login?vUsername=Alpesh&vPassword=admin123    OR
//http://localhost/wizit/ws/login?vEmail=alpesh@yudiz.com&vPassword=admin123
}

?>