<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Resetpassword extends MY_Controller {

    var $viewData = array();

    function __construct() {
        parent::__construct();
        parse_str($_SERVER['QUERY_STRING'], $_POST);
    }

    function index() {
        extract($_REQUEST);
        $resU = $this->admin_model->resetUserPassword($_REQUEST['vEmail'], $_REQUEST['vPassword'], $_REQUEST['vConfirmPassword']);
        if ($resU > 0) {
            $viewData["status"] = "0";
            echo json_encode($viewData);
        } else {
            $viewData["status"] = "1";
            echo json_encode($viewData);
        }
    }
    
}

?>
