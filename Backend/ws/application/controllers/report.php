<?php
class Report extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->library('email');
        $models[] = 'user_model';
        $this->load->model($models);
    }

    function index()
    {
        if (($this->input->get_post('iUserID') && $this->input->get_post('iUserID') != '') && ($this->input->get_post('iFeedID') && $this->input->get_post('iFeedID') != ''))
        {
            $reporterData = $this->user_model->getUserDataByID($this->input->get_post('iUserID'));
            $reportedFeedData = $this->user_model->getFeedByID($this->input->get_post('iFeedID'));
            $posterData = $this->user_model->getUserDataByID($reportedFeedData['iUserID']);
            $message = "User <b>" . $reporterData['vEmail'] . "</b> has reported a post <b>" . $reportedFeedData['vFeedTitle'] . "</b> as inappropriate.<br>"
                    . "<br><strong><u>Post Created By</u></strong> :<br>"
                    . "Name : ".$posterData['vUsername']."<br>"
                    . "Email : ".$posterData['vEmail']."<br>"
                    . "User ID: ".$posterData['iUserID']."<br>"
                    . "Post ID: ".$reportedFeedData['iFeedID']."<br>"
                    ;
            $this->email->initialize(unserialize(EMAIL_CONFIG));

            $this->email->set_newline("\r\n");

            $this->email->from($reporterData['vEmail'], $reporterData['vUsername']);

            $this->email->reply_to($reporterData['vEmail']);

            $this->email->to('youremail@yourhost.com');

            $this->email->subject('Report Abused - Your Host');

            $this->email->message($message);
            if ($this->email->send())
            {
                $jsondata["status"] = "0"; //mail sent
                $jsondata["data"] = array();
                $jsondata["message"] = "Report abuse mail sent";
            }
            else
            {
                $jsondata["status"] = "1"; //mail not sent
                $jsondata["data"] = array();
                $jsondata["message"] = "Report abuse mail not sent";
            }
        }
        else
        {
            $jsondata['status'] = '1';
            $jsondata['message'] = 'Please provide user id and post id';
        }
        echo je($jsondata);
    }

}
