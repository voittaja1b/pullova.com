<?php

class Admin_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->load->helper('cookie');
    }

    /*     * ***********************************************************************
     * *					Check Cookie For Loggedin User
     * *********************************************************************** */

    function chk_cookie() {
        $cookie = md5('loggedin');
        if ($cookie_value = get_cookie($cookie)) {
            $this->db->from(TBL_USER);
            $this->db->where('md5(vEmail)', $cookie_value);
            $this->db->where('eStatus', 'Active');
            $this->db->where('eDelete', '0');
            $this->db->where_in('eUserType', unserialize(wizit_USER_TYPE));
            $query = $this->db->get();

            // Let's check if there are any results

            if ($query->num_rows == 1) {
                // If there is a user, then create session data

                $row = $query->row();
                if ($row != '') {
                    $userdata = array(
                        'ADMINLOGIN' => TRUE,
                        'ADMINID' => $row->iUserID,
                        'ADMINFIRSTNAME' => $row->vFirst,
                        'ADMINLASTNAME' => $row->vLast,
                        'ADMINEMAIL' => $row->vEmail,
                        'ADMINUSERTYPE' => $row->eUserType
                    );

                    // STORE SESSION
                    $this->session->set_userdata($userdata);
                    return true;
                }
            }
            return false;
        }
    }

    /*     * ***********************************************************************
     * *					Check User Login
     * *********************************************************************** */

    function checkLogin($postData) {
        $this->db->from(TBL_USER);
        $this->db->where('vEmail', $postData['vEmail']);
        $this->db->where('vPassword', $postData['vPassword']);
        $this->db->where('eStatus', 'Active');
        $this->db->where('eDelete', '0');
        $this->db->where_in('eUserType', unserialize(wizit_USER_TYPE));

        // Query
        $query = $this->db->get();

        // Let's check if there are any results

        if ($query->num_rows == 1) {
            // If there is a user, then create session data

            $row = $query->row();
            // USER NOT EXISTS IN DATABASE
            if ($row != '') {
                $userdata = array(
                    'ADMINLOGIN' => TRUE,
                    'ADMINID' => $row->iUserID,
                    'ADMINFIRSTNAME' => $row->vFirst,
                    'ADMINLASTNAME' => $row->vLast,
                    'ADMINEMAIL' => $row->vEmail,
                    'ADMINUSERTYPE' => $row->eUserType
                );

                // STORE SESSION
                $this->session->set_userdata($userdata);

                // LOGGEDIN COOKIE ARGUMENTS

                if ($this->input->post('chkRemember')) {
                    $cookie = array(
                        'name' => md5('loggedin'),
                        'value' => md5($row->vEmail),
                        'expire' => '86500',
                        'secure' => TRUE
                    );
                    // SET LOOGEDIN COOKIE
                    set_cookie($cookie);
                }
                return true;
            }
        } else {
            return false;
        }
    }

    /*
      | -------------------------------------------------------------------
      |  RESET ADMIN PASSWORD
      | -------------------------------------------------------------------
     */

    function resetUserPassword($vEmail, $vPassword) {
        $this->db->from(TBL_USER);
        $this->db->where('vEmail', $vEmail);
        $this->db->where('eStatus', 'Active');
        $this->db->where('eDelete', '0');
        $this->db->where_in('eUserType', unserialize(social_events_GENERAL_USER_TYPE));

        $query = $this->db->get();

        if ($query->num_rows() > 0) {
            echo 'update tbl_user set vPassword=' . md5($vPassword) . '  where vEmail=' . $vEmail;
            exit;
            $update = $this->db->update(TBL_USER, array('vPassword' => md5($vPassword)), array('vEmail ' => $vEmail));
            if ($this->db->affected_rows() >= 0) {
                $row = $query->row();

                $userdata = array(
                    'ADMINLOGIN' => TRUE,
                    'ADMINID' => $row->iUserID,
                    'ADMINFIRSTNAME' => $row->vFirst,
                    'ADMINLASTNAME' => $row->vLast,
                    'ADMINEMAIL' => $row->vEmail,
                    'ADMINUSERTYPE' => $row->eUserType
                );


                // STORE SESSION

                $this->session->set_userdata($userdata);

                $this->load->library('email');
                $this->load->library('encrypt');

                $getTemplate = $this->general_model->getEmailTemplate('2');
                $strEmail1 = str_replace('\"', '"', $getTemplate->email_body);
                $strEmail1 = str_replace('\r\n', '', $strEmail1);
                $strEmail1 = str_replace('[header]', '', $strEmail1);
                //$strEmail1 = str_replace('[logo]', FRONT_IMAGE_URL . "logo-vin-social-club.png", $strEmail1);
                $strEmail1 = str_replace('[logo]', "", $strEmail1);
                $strEmail1 = str_replace('&nbsp;', '', $strEmail1);
                $strEmail1 = str_replace('[year]', date('Y'), $strEmail1);
                $strEmail1 = str_replace('[LINK]', WEBSERVICE_URL . "login", $strEmail1);
                $strEmail1 = str_replace('[USER_NAME]', $row->vFirst . " " . $row->vLast, $strEmail1);
                $strEmail1 = str_replace('[NEWPASSWORD]', $vPassword, $strEmail1);
                $strEmail1 = str_replace('[SITE_URL]', DOMAIN_URL, $strEmail1);

                $this->email->initialize(unserialize(EMAIL_CONFIG));
                $this->email->set_newline("\r\n");
                $this->email->from($getTemplate->sender_email, $getTemplate->sender_name);
                $this->email->reply_to($getTemplate->reply_email);
                $this->email->to($row->vEmail);
                $this->email->subject($getTemplate->subject);
                $this->email->message($strEmail1);
                $this->email->send();

                return true;
            }
            else
                return $this->db->_error_message();
        }
        else
            return 0;
    }

    function checkOldPassword($postData) {

        $this->db->from(TBL_USER);
        $this->db->where('vEmail', $postData['vEmail']);
        $this->db->where('vPassword', $postData['vPassword']);
        $this->db->where('eStatus', 'Active');
        $this->db->where('eDelete', '0');
        $this->db->where_in('eUserType', $this->session->userdata('ADMINUSERTYPE'));

        $query = $this->db->get();

        if ($query->num_rows() >= 1)
            return 1;

        return 0;
    }

    function changePassword($postData) {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);
        $query = $this->db->update(TBL_USER, $postArray, array('vEmail ' => $postData['vEmail']));

        if ($this->db->affected_rows() >= 0)
            return true;
        else
            return $this->db->_error_message();
    }

    /*     * ***********************************************
     *   
     *   GET ALL USER DATA
     * 
     * ********************************************** */

    function getUsersDataAll() {
        $this->db->from(TBL_USER);
        $this->db->where('eDelete', '0');
        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return '';
    }

    /*     * ***********************************************
     *   
     *   GET ALL USER DATA
     * 
     * ********************************************** */

    function getUserDataAll() {
        $this->db->select("vEmail");
        $this->db->from(TBL_USER);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   GET ALL ADMIN DATA
     *
     * ********************************************** */

    function getAdminDataAll() {

        $this->db->from(TBL_USER);
        $this->db->where('eDelete', '0');
        $this->db->where_in('eUserType', 'Admin');
        $query = $this->db->get();

        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   CHANGE USER STATUS
     *
     * ********************************************** */

    function changeUserStatus($iUserID) {

        $query = $this->db->query("UPDATE " . TBL_USER . " SET eStatus = IF (eStatus = 'Active', 'Inactive','Active') WHERE iUserID = $iUserID");

        if ($this->db->affected_rows() > 0)
            return $query;
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   DELETE USER ACCOUNT
     *
     * ********************************************** */

    function deleteUserByID($iUserID) {

        $query = $this->db->query("UPDATE " . TBL_USER . " SET eDelete = '1' WHERE iUserID = $iUserID");

        if ($this->db->affected_rows() > 0)
            return $query;
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   GET USER DETAILS BY ID
     *
     * ********************************************** */

    function getUserDataById($iUserID) {

        $result = $this->db->get_where(TBL_USER, array("iUserID" => $iUserID));

        if ($result->num_rows() > 0)
            return $result->row_array();
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   GET USER DETAILS BY ID
     *
     * ********************************************** */

    function addUser($postData) {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);
        $query = $this->db->insert(TBL_USER, $postArray);

        if ($this->db->affected_rows() > 0)
            return $this->db->insert_id();
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   EDIT USER DATA
     *
     * ********************************************** */

    function editUserByID($postData) {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);

        $query = $this->db->update(TBL_USER, $postArray, array('iUserID ' => $postData['iUserID']));

        if ($this->db->affected_rows() > 0)
            return true;
        else
            return $this->db->_error_message();

        //return $query;
    }

    /*     * ***********************************************
     *
     *   CHECK EMAIL ADDRESS EXISTS OR NOT
     *
     * ********************************************** */

    function checkUserEmailAvailable($vEmail, $iUserID = '') {
        if (isset($iUserID) && $iUserID != '')
            $ucheck = array('vEmail' => $vEmail, 'iUserID <>' => $iUserID, "eDelete" => "0");
        else
            $check = array('vEmail' => $vEmail, "eDelete" => "0");

        $result = $this->db->get_where(TBL_USER, (isset($ucheck) && $ucheck != '') ? $ucheck : $check);

        if ($result->num_rows() >= 1)
            return 0;
        else
            return 1;
    }

    /*     * ***********************************************
     *   CHECK USERNAME EXISTS OR NOT
     * **************************************************** */

    function checkUsernameAvailable($vUsername, $iUserID = '') {
        if (isset($iUserID) && $iUserID != '')
            $ucheck = array('vUsername' => $vUsername, 'iUserID <>' => $iUserID, "eDelete" => "0");
        else
            $check = array('vUsername' => $vUsername, "eDelete" => "0");

        $result = $this->db->get_where(TBL_USER, (isset($ucheck) && $ucheck != '') ? $ucheck : $check);

        if ($result->num_rows() >= 1)
            return 0;
        else
            return 1;
    }

    /*     * ***********************************************
     *
     *   GET SETTING DATA BY ID 
     *   SETTING ID MUST BE ALWAYS : 1
     *
     * ********************************************** */

    function getSettingDetailByID() {

        $query = $this->db->get_where(TBL_SETTING, array('iSettingId ' => '1'));

        if ($query->num_rows() > 0)
            return $query->row_array();
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   EDIT SETTING
     *
     * ********************************************** */

    function editAdminSetting($postData) {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_SETTING);

        $query = $this->db->update(TBL_SETTING, $postArray, array('iSettingID ' => $postData['iSettingID']));

        if ($this->db->affected_rows() >= 0)
            return true;
        else
            return $this->db->_error_message();
    }

    /*     * ***********************************************
     *
     *   GET ALL EMAIL TEMPLATES
     *
     * ********************************************** */

    function getEmailTemplateDataAll() {

        $result = $this->db->get(TBL_EMAIL_TEMPLATE);

        if ($result->num_rows() > 0)
            return $result->result_array();
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   GET ALL EMAIL TEMPLATES BY ID
     *
     * ********************************************** */

    function getEmailTemplateDataById($iTemplateID) {

        $result = $this->db->get_where(TBL_EMAIL_TEMPLATE, array("iTemplateID" => $iTemplateID));

        if ($result->num_rows() > 0)
            return $result->row_array();
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   EDIT EMAIL TEMPLATE DATA
     *
     * ********************************************** */

    function editEmailTemplateByID($postData) {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_EMAIL_TEMPLATE);

        $query = $this->db->update(TBL_EMAIL_TEMPLATE, $postArray, array('iTemplateID ' => $postData['iTemplateID']));

        if ($this->db->affected_rows() >= 0)
            return true;
        else
            return $this->db->_error_message();
    }

    /*     * ***********************************************
     *
     *   CHANGE SESSION DATA/VALUE FUNCTION 
     *
     * ********************************************** */

    function changeSession($postData) {

        $query = $this->db->get_where(TBL_USER, $postData);

        if ($query->num_rows == 1) {

            // If there is a user, then create session data

            $row = $query->row();

            // USER NOT EXISTS IN DATABASE

            if ($row != '') {

                $userdata = array(
                    'ADMINFIRSTNAME' => $row->vFirst,
                    'ADMINLASTNAME' => $row->vLast,
                    'ADMINEMAIL' => $row->vEmail,
                );

                // STORE SESSION

                $this->session->set_userdata($userdata);

                // LOGGEDIN COOKIE ARGUMENTS

                return true;
            }
        }
    }

    /*     * ***********************************************
     * *   CHANGE SUBSCRIBER STATUS
     * * ********************************************** */

    function changeUserSubscriberStatus($vEmail) {
        $query = $this->db->query("UPDATE " . TBL_USER . " SET eSubscriber = '1' WHERE vEmail ='" . $vEmail . "' ");

        if ($this->db->affected_rows() > 0)
            return $query;
        else
            return '';
    }

    function sendProfessionalMailToUser($iUserID) {

        $query = $this->db->get_where(TBL_USER, array('iUserID' => $iUserID, 'eDelete' => '0'));

        if ($query->num_rows() > 0) {

            $row = $query->row();

            $this->load->library('email');
            $this->load->library('encrypt');

            $getTemplate = $this->general_model->getEmailTemplate('20');

            $strEmail1 = str_replace('\"', '"', $getTemplate->email_body);
            $strEmail1 = str_replace('\r\n', '', $strEmail1);
            $strEmail1 = str_replace('[header]', '', $strEmail1);
            $strEmail1 = str_replace('[logo]', FRONT_IMAGE_URL . "logo-vin-social-club.png", $strEmail1);
            $strEmail1 = str_replace('&nbsp;', '', $strEmail1);
            $strEmail1 = str_replace('[year]', date('Y'), $strEmail1);
            $strEmail1 = str_replace('[FirstName]', $row->vFirst . " " . $row->vLast, $strEmail1);
            $strEmail1 = str_replace('[link]', DOMAIN_URL, $strEmail1);
            $strEmail1 = str_replace('[SITE_URL]', DOMAIN_URL, $strEmail1);
            $strEmail1 = str_replace('[PROFILE_LINK]', DOMAIN_URL . "/profile/edit#monenterprise", $strEmail1);

            $config['charset'] = 'utf-8';
            $config['wordwrap'] = TRUE;
            $config['mailtype'] = 'html';
//            $config['protocol'] = 'sendmail';

            $this->email->initialize($config);

            $this->email->set_newline("\r\n");

            $this->email->from($getTemplate->sender_email, $getTemplate->sender_name);

            $this->email->reply_to($getTemplate->reply_email);

            $this->email->to($row->vEmail);

            $this->email->subject($getTemplate->subject);

            $this->email->message($strEmail1);

            if ($this->email->send())
                return 1;
            else
                return 0;
        }
    }

    function addUserAccessPermission($postData) {


        //$postArray = $this->general_model->getDatabseFields($postData, TBL_ACCESS_PERMISSION);
        $query = $this->db->insert(TBL_ACCESS_PERMISSION, $postData);

        /* if ($this->db->affected_rows() > 0)
          return $this->db->insert_id();
          else
          return ''; */
    }

    function getUserDetailByEmail($email) {
        $this->db->from(TBL_USER);
        $this->db->where('eDelete', '0');
        $this->db->where('vEmail', $email);

        $query = $this->db->get();
        if ($query->num_rows() > 0)
            return $query->row_array();
        else
            return '';
    }

    function addUserDataBatch($postData) {
        $query = $this->db->insert_batch(TBL_USER, $postData);
        return TRUE;
    }

    function sendPasswordChangeRequestMail($vEmail) {
        // Query
        $this->db->from(TBL_USER);
        $this->db->where('vEmail', $vEmail);
        $this->db->where('eStatus', 'Active');
        $this->db->where('eDelete', '0');
        $this->db->where_in('eUserType', unserialize(social_events_GENERAL_USER_TYPE));
        $query = $this->db->get();


        if ($query->num_rows == 0) {
            // USER NOT EXISTS IN DATABASE
            return '1'; // User not found
        } else {
            $row = $query->row();
            $this->load->library('email');
            $this->load->library('encrypt');

            $encrypted_id = $this->encrypt->encode($row->vEmail);
            //$encrypted_id = substr_replace('/','_',$encrypted_id);
            $this->db->update(TBL_USER, array('vActivationKey' => $encrypted_id), array('vEmail ' => $vEmail));
            $getTemplate = $this->general_model->getEmailTemplate('1');

            $strEmail1 = str_replace('\"', '"', $getTemplate->email_body);

            $strEmail1 = str_replace('\r\n', '', $strEmail1);

            $strEmail1 = str_replace('[header]', '', $strEmail1);

            //$strEmail1 = str_replace('[logo]', FRONT_IMAGE_URL . "logo-vin-social-club.png", $strEmail1);

            $strEmail1 = str_replace('[logo]', "", $strEmail1);

            $strEmail1 = str_replace('&nbsp;', '', $strEmail1);

            $strEmail1 = str_replace('[year]', date('Y'), $strEmail1);

            $strEmail1 = str_replace('[USER_NAME]', $row->vFirst . " " . $row->vLast, $strEmail1);

            $strEmail1 = str_replace('[RESET_LINK]', WEBSERVICE_URL . "forgotpassword/reset?key=" . $encrypted_id, $strEmail1);

            $strEmail1 = str_replace('[SITE_URL]', DOMAIN_URL, $strEmail1);

//            $config['charset'] = 'utf-8';
//            $config['wordwrap'] = TRUE;
//            $config['mailtype'] = 'html';
//            $config['protocol'] = 'sendmail';


            $this->email->initialize(unserialize(EMAIL_CONFIG));
            $this->email->set_newline("\r\n");
            $this->email->from($getTemplate->sender_email, $getTemplate->sender_name);
            $this->email->reply_to($getTemplate->reply_email);
            $this->email->to($row->vEmail);
            $this->email->subject($getTemplate->subject);
            $this->email->message($strEmail1);


            if ($this->email->send()) {
                return '0'; //email found and mail sent (no error)
            } else {
                return '2'; // email found but mail not sent 
            }
            //return true;
        }
    }

    function checkActivationKey($key) {
        $this->db->from(TBL_USER);
        $this->db->where('vActivationKey', $key);
        $this->db->where('eStatus', 'Active');
        $this->db->where('eDelete', '0');
        $this->db->where_in('eUserType', unserialize(social_events_GENERAL_USER_TYPE));
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            return $query->row_array();
        } else {
            return array();
        }
    }

    function resetPassword($postData) {
        //pre($postData);
        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);

        $query = $this->db->update(TBL_USER, array('vPassword' => md5($postData['newpassword'])), array('iUserID ' => $postArray['iUserID']));
        if ($this->db->affected_rows() > 0) {
            return $this->getUserDataById($postArray['iUserID']);
        } else {
            return '1';
        }
    }

}

?>