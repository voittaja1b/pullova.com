<?php
header('Content-Type: application/json');
if (!defined('BASEPATH'))
    exit('No direct script access allowed');
// error_reporting(E_ALL ^ E_NOTICE);
// ini_set('display_errors', 1);
/*
  |--------------------------------------------------------------------------
  | File and Directory Modes
  |--------------------------------------------------------------------------
  |
  | These prefs are used when checking and setting modes when working
  | with the file system.  The defaults are fine on servers with proper
  | security, but you may wish (or even need) to change the values in
  | certain environments (Apache running a separate process for each
  | user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
  | always be used to set the mode correctly.
  |
 */
define('FILE_READ_MODE', 0644);
define('FILE_WRITE_MODE', 0666);
define('DIR_READ_MODE', 0755);
define('DIR_WRITE_MODE', 0777);

/*
  |--------------------------------------------------------------------------
  | File Stream Modes
  |--------------------------------------------------------------------------
  |
  | These modes are used when working with fopen()/popen()
  |
 */

define('FOPEN_READ', 'rb');
define('FOPEN_READ_WRITE', 'r+b');
define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
define('FOPEN_WRITE_CREATE', 'ab');
define('FOPEN_READ_WRITE_CREATE', 'a+b');
define('FOPEN_WRITE_CREATE_STRICT', 'xb');
define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

define('_PATH', substr(dirname(__FILE__), 0, -22));

define('_URL', substr($_SERVER['PHP_SELF'], 0, - (strlen($_SERVER['SCRIPT_FILENAME']) - strlen(_PATH))));

define('SITE_PATH', _PATH . "/");
define('SITE_URL', _URL . "/");

define('DOMAIN_URL', 'http://' . $_SERVER['HTTP_HOST']);

define('WEBSERVICE_URL', DOMAIN_URL . "/ws/");

define('gmapkey', '');

define('CERTIFICATE_FILE',SITE_PATH.'ws/SocialEventsPush.pem');
define('GOOGLE_API_KEY','AIzaSyCpum8ZiZ-3cJf34fD1zqCeQDBr9TIfS9I');


define('STR_APIKEY', 'sk_test_XDTDLHI1OnbfaMiK3WGpNx2v');


//
//define('ADMIN_CSS_URL', ADMIN_URL . "css/");
//define('ADMIN_PLUGIN_URL', ADMIN_URL . "plugins/");
//define('ADMIN_JS_URL', ADMIN_URL . "js/");

define('WS_IMAGE_URL', WEBSERVICE_URL . "images/");
define('WS_IMAGE_PATH', SITE_PATH . "ws/images/");
/* * ***************************************************************
 *  DEFINE CONSTANTS FOR WEBSERVICE IMAGES
 * *************************************************************** */
define('PROFILE_IMAGE_URL', WS_IMAGE_URL . "profile/original/");
define('PROFILE_IMAGE_PATH', WS_IMAGE_PATH . "profile/original/");
define('PROFILE_THUMB_URL', WS_IMAGE_URL . "profile/thumb/");
define('PROFILE_THUMB_PATH', WS_IMAGE_PATH . "profile/thumb/");

define('FEED_MAP_IMAGE_URL', WS_IMAGE_URL . "feed/map/");
define('FEED_MAP_IMAGE_PATH', WS_IMAGE_PATH . "feed/map/");

define('FEED_ITEM_IMAGE_URL', WS_IMAGE_URL . "feed/item/");
define('FEED_ITEM_IMAGE_PATH', WS_IMAGE_PATH . "feed/item/");
define('FEED_ITEM_VIDEO_PATH', WS_IMAGE_PATH . "feed/video/");
define('FEED_ITEM_VIDEO_URL', WS_IMAGE_URL . "feed/video/");

/*
 * 
******************* TABLE CONSTANTS ************************
 * 
 */

define("TBL_USER", "tbl_user");
define("TBL_SETTING", "tbl_setting");
define("TBL_FOLLOW", "tbl_follow");
define('TBL_EMAIL_TEMPLATE', 'tbl_emailtemplate');
define('TBL_QUESTION', 'tbl_question');
define('TBL_VOTE', 'tbl_vote');
define('TBL_ANSWER', 'tbl_answer');
define('TBL_FRIEND', 'tbl_friend');
define("TBL_INVITE", "tbl_invite");
define("TBL_LOCATION_REQUEST", "tbl_location_request");
define("TBL_FEED", "tbl_feed");
define("TBL_FEED_TAG_FRIEND", "tbl_feed_tag_friend");
define('TBL_FEEDLIKES', 'tbl_feedlikes');
define('TBL_FEEDCOMMENTS', 'tbl_feedcomments');
define("TBL_MESSAGE", "tbl_messages");
define("TBL_NEAREST_DRIVERS", "tbl_nearest_drivers");
define("TBL_PROMO_CODE", "tbl_promo_code");


/*
 * 
******************* TABLE CONSTANTS OVER ************************
 * 
 */


define("social_events_USER_TYPE", serialize(array('Super', 'Admin')));
define("social_events_GENERAL_USER_TYPE", serialize(array('User')));
if (defined('ENVIRONMENT')) {
    switch (ENVIRONMENT) {
        case 'development':
            define("EMAIL_CONFIG", serialize(array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.googlemail.com',
                'smtp_port' => '465',
                'smtp_user' => 'alpesh@yudiz.com',
                'smtp_pass' => 'Yudiz@321',
                'charset' => 'utf-8',
                'wordwrap' => TRUE,
                'mailtype' => 'html'
                            )
                    )
            );
            break;
        case 'production':
            define("EMAIL_CONFIG", serialize(array(
                'protocol' => 'smtp',
                'smtp_host' => 'ssl://smtp.mail.yahoo.com',
                'smtp_port' => '465',
                'smtp_user' => '﻿﻿﻿pullovatest@yahoo.com',
                'smtp_pass' => 'qw3rtyuiop',
                'charset' => 'utf-8',
                'wordwrap' => TRUE,
                'mailtype' => 'html'
                            )
                    )
            );
            break;
    }
    
    define("IMAGE_DICTONARY", serialize(array('vImage')));
    define("IMAGE_ACCEPT", serialize(array(IMAGETYPE_JPEG, IMAGETYPE_JPC, IMAGETYPE_JP2, IMAGETYPE_JPX, IMAGETYPE_JB2)));
}


function printArray($obj) {
	echo "<pre>"; print_r($obj); echo "</pre>";
}
        /* End of file constants.php */
        /* Location: ./application/config/constants.php */