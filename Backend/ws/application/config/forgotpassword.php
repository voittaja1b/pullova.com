<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Forgotpassword extends MY_Controller {

    var $viewData = array();

    function __construct() {
        parent::__construct();
        parse_str($_SERVER['QUERY_STRING'], $_POST);
        $this->load->library('email');
        $this->load->library('encrypt');
    }

    function index() {
        extract($_REQUEST);

        $resU = $this->admin_model->sendPasswordChangeRequestMail($vEmail);
        
        
        if ($resU == '0') {
            $viewData["status"] = "0"; //insert and mail sent to user
            $viewData["data"] = array();
            $viewData["message"] = "mail sent";
        } elseif ($resU == '1') {
            $viewData["status"] = "1"; //no such email found
            $viewData["data"] = array();
            $viewData["message"] = "no such email found";
        } elseif ($resU == '2') {
            $viewData["status"] = "2"; //email found but mail not sent
            $viewData["data"] = array();
            $viewData["message"] = "User with email found, but mail not sent";
        }
        echo je($viewData);
    }

    function reset() {
        $vActivationKey = urlencode($this->input->get("key"));
        $vActivationKey = str_replace("+", "%2B", $vActivationKey);
        $vActivationKey = urldecode($vActivationKey);
        $res = $this->admin_model->checkActivationKey($vActivationKey);
        if (isset($res) && !empty($res)) {
            $postData['iUserID'] = $res['iUserID'];
            $newPassword = substr(str_shuffle(implode('', array_merge(range(0, 9), range('a', 'z'), range('A', 'Z')))), 0, 8);
            $postData['newpassword'] = $newPassword;
            $reset = $this->admin_model->resetPassword($postData);
            if (isset($reset) && $reset !== '1') {

                $getTemplate = $this->general_model->getEmailTemplate('2');

                $strEmail1 = str_replace('\"', '"', $getTemplate->email_body);

                $strEmail1 = str_replace('\r\n', '', $strEmail1);

                $strEmail1 = str_replace('[header]', '', $strEmail1);

                $strEmail1 = str_replace('[logo]', '', $strEmail1);

                $strEmail1 = str_replace('&nbsp;', '', $strEmail1);

                $strEmail1 = str_replace('[year]', date('Y'), $strEmail1);

                $strEmail1 = str_replace('[LINK]', DOMAIN_URL . "/ws/index.php/login", $strEmail1);

                $strEmail1 = str_replace('[USER_NAME]', $reset['vUsername'] . " ", $strEmail1);

                $strEmail1 = str_replace('[NEWPASSWORD]', $newPassword, $strEmail1);

                $this->email->initialize(unserialize(EMAIL_CONFIG));

                $this->email->set_newline("\r\n");

                $this->email->from($getTemplate->sender_email, $getTemplate->sender_name);

                $this->email->reply_to($getTemplate->reply_email);

                $this->email->to($reset['vEmail']);

                $this->email->subject($getTemplate->subject);

                $this->email->message($strEmail1);

                if ($this->email->send()) {
                    $viewData["status"] = "0"; //insert and mail sent to user
                    $viewData["data"] = array();
                    $viewData["message"] = "password reset and mail sent";
                } else {
                    $viewData["status"] = "1"; //insert and mail sent to user
                    $viewData["data"] = array();
                    $viewData["message"] = "password reset but mail not sent";
                }
            } else {
                $viewData["status"] = "2"; //password not updated 
                $viewData["data"] = array();
                $viewData["message"] = "Password not updated";
            }
        } else {
            $viewData["status"] = "3"; //invalid activation key
            $viewData["data"] = array();
            $viewData["message"] = "Invalid activation key";
        }
        echo je($viewData);
    }

//http://isa-nu.org/wizit/ws/forgotpassword/reset/G4kkEoY+48GeMHi9eqNOMP2khIOtNrCFEPGJH79lTWfssj835sIZ21jukkSoyl2ikqpioXo2jVp+9BLC5y5kKw==
//http://isa-nu.org/wizit/ws/forgotpassword/reset/Y72V+4arnPKXScKjiwsj2tr93xOisfy4P+5iHFAHhcSw822JGeacOocg2JkAUFhcNuRaA4UoKz8TrmILc73t3A==
}

?>
