<?php

//  application/core/MY_Controller.php
class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();

        $this->load->model('admin_model');
        $this->getSettingDetail();
        $this->lang->switch_to('en');
        $this->currentLang = $this->getLang();
        $this->config->set_item('language', $this->currentLang);
    }

    function getLang() {

        $lang = $this->session->userdata('userLang');

        if ($this->session->userdata('userLang') && in_array($this->session->userdata('userLang'), $this->config->item('avail_languages')))
            return $this->session->userdata('userLang');

        return $this->config->item('language');
    }

    function getSettingDetail() {
        $query = $this->db->get_where('tbl_setting', array('iSettingID ' => '1'));

        if ($query->num_rows() > 0) {
            $arr = $query->row_array();
            foreach ($arr as $key => $row) {
                define($key, $row);
            }
        } else
            return '';
    }

    function curl_load($url) {
        curl_setopt($ch = curl_init(), CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    function sendPushNotification($token, $message, $generatePayloardData, $notificationType) {

        /* Code Starts for sending Push Notification */
        $this->load->library('apn');
        $this->apn->payloadMethod = 'enhance'; // ???????? ???? ????? ??? ???????
        $this->apn->connectToPush();

        // ?????????? ??????????? ?????????? ? notification
        $this->apn->setData(array('someKey' => true));

        $send_result = $this->apn->sendMessage($token, $generatePayloardData, $message, $notificationType, /* badge */ 1, /* sound */ 'default');

        if ($send_result)
            log_message('debug', '?????????? ???????');
        else
            log_message('error', $this->apn->error);

        $this->apn->disconnectPush();
        /* Code Ends for sending Push Notification */
    }

//push notification for android
    function sendNotificationAndroid($apiKey, $registrationIdsArray, $messageData, $generatePayloardData, $notificationType) {
        $msg = array();
        if (isset($messageData))
            $msg = array("message" => $messageData);

        if (isset($generatePayloardData) && !empty($generatePayloardData)) {
            foreach ($generatePayloardData as $key => $val) {
                $msg[$key] = $val;
            }
        }
        $registatoin_ids = array($registrationIdsArray);
        //$msg['message'] = $messageData;
        $msg['notificationType'] = $notificationType;
        $url = 'https://android.googleapis.com/gcm/send';
        $fields = array
            (
            'registration_ids' => $registatoin_ids,
            'data' => $msg
        );
        $headers = array
            (
            "Authorization: key=$apiKey",
            'Content-Type: application/json'
        );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }

}
