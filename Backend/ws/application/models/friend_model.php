<?php

class Friend_model extends Model {

    function __construct() {
        parent::Model();
        $this->load->helper('cookie');
    }

    function getUserTokens($iUserID) {
        $this->db->where('iUserID', $iUserID);
        $query = $this->db->get('tbl_devicetokens');
        return $query->result();
    }

    function addRequest($userId, $friendId) {
        $data = array(
            'iUserID' => $userId,
            'iFriendId' => $friendId,
            'iChoice' => 'Accepted',
            'eDeleted' => 0,
            'iStatusUser' => $userId,
            'eStatus' => 'Active',
            'dCreatedDate' => $this->general_model->getDBDateTime(),
            'iDeletedBy' => '0'
        );
        $this->db->insert('tbl_friend', $data);
    }

    function changeFriendStatus($postData) {
        $postArray = $this->general_model->getDatabseFields($postData, TBL_FRIEND);
        
        if(isset($postData['eFriendStatus']) && $postData['eFriendStatus'] != '')
            $query=$this->db->update(TBL_FRIEND, $postArray, array('iSenderID' => $postData['iSenderID'],'iFriendID'=>$postData['iFriendID']));
        else
            $query=$this->db->update(TBL_FRIEND, $postArray, array('iSenderID' => $postData['iSenderID'],'iFriendID'=>$postData['iFriendID']));
        
        if ($this->db->affected_rows() > 0)
            return $query;
        else
            return '';
    }

    function inviteFriendUsingSocialMedia($postData) {
        $postArray = $this->general_model->getDatabseFields($postData, TBL_SOCIAL_FRIENDS);
        $query = $this->db->insert(TBL_SOCIAL_FRIENDS, $postArray);

        if ($this->db->affected_rows() > 0)
            return $this->db->insert_id();
        else
            return 0;
    }

    function addInviteFriend($userEmailData) {
        $query = $this->db->insert_batch(TBL_INVITEE, $userEmailData);

        if ($this->db->affected_rows() > 0)
            return 1;
        else
            return 0;
    }

    function checkInviteFriendByEmailOrFbID($vEmailOrFbID) {

        $this->db->select('*');
        $this->db->where("vEmailOrFbID", $vEmailOrFbID);
        $this->db->from(TBL_INVITEE);
        $query = $this->db->get();

        if ($query->num_rows > 0)
            return $query->row_array();
        else
            return '';
    }

    function addFriend($postData) {
        $postArray = $this->general_model->getDatabseFields($postData, TBL_FRIEND);
        $query = $this->db->insert(TBL_FRIEND, $postArray);

        if ($this->db->affected_rows() > 0)
            return 1;
        else
            return 0;
    }

    function deleteFriendByUserIDAndEmail($friendData){
        
        $this->db->query('delete from '.TBL_INVITEE.' where iUserID="'.$friendData['iUserID'].'"  and vEmailOrFbID="'.$friendData['vEmailOrFbID'].'"');
        
        
    }
}

?>
