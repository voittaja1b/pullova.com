<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Question_model extends CI_Model {

    var $viewData = array();

    function __construct() {
        parent::__construct();
    }

    function questionListByUserID($iUserID) {
        $query = $this->db->query("SELECT q.vQuestion,q.iQuestionID,q.iUserID,q.vLocation,q.vQuestionImage,q.dCreatedDate,q.vQuestionThumbImage,u.vImage,u.vFirst,u.vLast,u.vUsername,IF(ISNULL(iVote),0,iVote) AS iVote,IF(ISNULL(sum(iVote)),0,sum(iVote)) AS votes 
                            FROM tbl_question q
                            LEFT JOIN tbl_vote v ON (v.iItemID=q.iQuestionID AND v.eType='Question')
                            LEFT JOIN tbl_user u ON (u.iUserID = q.iUserID)
                            WHERE q.eStatus='Active'
                            AND q.eDelete = '0'
                            AND q.iUserID = $iUserID GROUP BY q.iQuestionID ORDER BY q.dCreatedDate DESC");
        if ($query->num_rows > 0)
            return $query->result_array();
        else
            return 0;
    }

    function getQuestionByID($iQuestionID) {
        $query = $this->db->query("SELECT q.vQuestion,q.iQuestionID,q.iUserID,q.vLocation,q.vQuestionImage,q.vQuestionThumbImage,u.vFirst,u.vLast,u.vUsername,IF(ISNULL(iVote),0,iVote) AS iVote 
                            FROM tbl_question q
                            LEFT JOIN tbl_vote v ON (v.iItemID=q.iQuestionID AND v.eType='Question')
                            LEFT JOIN tbl_user u ON (u.iUserID = q.iUserID)
                            WHERE q.eStatus='Active'
                            AND q.eDelete = '0'
                            AND q.iQuestionID = $iQuestionID");
        if ($query->num_rows > 0)
            return $query->row_array();
        else
            return 0;
    }

//    function followQuestionList($iUserID) {
//
//        $query = $this->db->query("SELECT  distinct(q.iQuestionID),q.vQuestion,q.iUserID,q.vLocation,q.vQuestionImage,q.dCreatedDate,q.vQuestionThumbImage,u.vImage,u.vFirst,u.vLast,u.vUsername,IF(ISNULL(iVote),0,iVote) AS iVote,IF(ISNULL(sum(iVote)),0,sum(iVote)) AS votes 
//                            FROM tbl_user u
//                            LEFT JOIN tbl_question q ON (q.iUserID=u.iUserID)
//                            LEFT JOIN tbl_vote v ON (v.iItemID=q.iQuestionID AND v.eType='Question')                            
//                            WHERE q.eStatus='Active'
//                            AND q.eDelete = '0'
//                            AND u.eStatus = 'Active'
//                            AND u.eDelete = '0'
//                            or u.iUserID IN ( SELECT iFollowingID FROM tbl_follow WHERE iFollowerID = '$iUserID' )
//                            AND u.iUserID='$iUserID'
//                            GROUP BY q.iQuestionID ORDER BY q.dCreatedDate DESC
//                            ");
//        if ($query->num_rows > 0)
//            return $query->result_array();
//        else
//            return 0;
//            
//   
//     }

    function followQuestionList($iUserID) {
         $query = $this->db->query("SELECT  q.iQuestionID,q.vQuestion,q.iUserID,q.vLocation,q.vQuestionImage,q.dCreatedDate,q.vQuestionThumbImage,u.vImage,u.vFirst,u.vLast,u.vUsername,IFNULL(votes,0) as votes
                        FROM tbl_question q
                                LEFT JOIN tbl_user u ON (u.iUserID = q.iUserID)
                                LEFT JOIN (SELECT iVoteID,iUserID,eType,iItemID,IF(ISNULL(iVote),0,iVote) AS iVote,IF(ISNULL(sum(iVote)),0,sum(iVote)) AS votes  FROM tbl_vote WHERE eType='Question' GROUP BY iItemID) as tbl_vote ON (q.iQuestionID=tbl_vote.iItemID) 
                                WHERE q.iUserID IN (
                                                        SELECT iFollowingID FROM  tbl_follow
                                                        WHERE iFollowerID = '$iUserID'
                                                    UNION
                                                        SELECT iUserID FROM tbl_user 
                                                        WHERE iUserID = '$iUserID' AND eStatus='Active' AND eDelete='0'
                                                    )
                                AND q.eStatus = 'Active'
                                AND q.eDelete = '0'
                                AND u.eStatus = 'Active'
                                AND u.eDelete = '0'
                                ORDER BY q.dCreatedDate DESC
                            ");

        /*$this->db->select("iQuestionID,vQuestion," . TBL_USER . ".iUserID,vLocation,vQuestionImage," . TBL_QUESTION . ".dCreatedDate,vQuestionThumbImage," . TBL_USER . ".vImage,vFirst,vLast,vUsername,IFNULL(votes,0) as votes", FALSE);
        $this->db->from(TBL_QUESTION);
        $this->db->join(TBL_FOLLOW, "iFollowingID=iUserID", "", FALSE);
        $this->db->join("(SELECT iVoteID,iUserID,eType,iItemID,IF(ISNULL(iVote),0,iVote) AS iVote,IF(ISNULL(sum(iVote)),0,sum(iVote)) AS votes  FROM " . TBL_VOTE . " WHERE eType='Question' GROUP BY iItemID) as " . TBL_VOTE, TBL_QUESTION . ".iQuestionID=" . TBL_VOTE . ".iItemID", "LEFT", FALSE);
        $this->db->join(TBL_USER, TBL_USER . ".iUserID=" . TBL_QUESTION . ".iUserID", "", FALSE);
        $this->db->where(TBL_FOLLOW . '.iFollowerID', $iUserID);
        $this->db->where(TBL_QUESTION . '.eStatus', 'Active');
        $this->db->where(TBL_QUESTION . '.eDelete', '0');
        $this->db->where(TBL_USER . '.eStatus', 'Active');
        $this->db->where(TBL_USER . '.eDelete', '0');
        $this->db->order_by(TBL_QUESTION . '.dCreatedDate', 'DESC');

        $query = $this->db->get();*/
        
        if ($query->num_rows > 0){
            return $query->result_array();
        }
        else{
            return 0;
        }
    }

    function addQuestionRecord($postData) {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_QUESTION);
        $query = $this->db->insert(TBL_QUESTION, $postArray);
        if ($this->db->affected_rows() > 0)
            return $this->db->insert_id();
        else
            return '';
    }

    /*     * ******************************************** 
      List of questions with maximum number of UP votes (Up votes to a question)] 1
     * ******************************************* */

    function top_voted_question($start, $dateFilter, $iUserID) {
        $this->db->select("u.vFirst,
                            u.vLast,
                            q.iQuestionID,
                            q.iCategoryID,
                            q.iUserID,
                            q.vAdditionalDetail,
                            q.vImage,
                            q.bQuestion,
                            q.dCreateDate,
                            (select IF(sum(iVote) IS NULL,0, sum(iVote)) from tbl_vote where iItemID = q.iQuestionID and eType = 'Question') as votes,IF(ISNULL(iVote),0,iVote) AS iVote", false);
        $this->db->from(TBL_QUESTION . ' q');
        $this->db->join_using(TBL_USER . ' u', "iUserID");
        $this->db->join("(SELECT * FROM " . TBL_VOTE . " WHERE iUserID = $iUserID AND eType = 'Question') as " . TBL_VOTE, TBL_VOTE . ".iItemID=" . TBL_QUESTION . ".iQuestionID", "LEFT", FALSE);

        $this->db->where(TBL_QUESTION . ".eDelete", "0");
        $this->db->where(TBL_QUESTION . ".eStatus", "Active");
        $this->db->where(TBL_USER . ".eDelete", "0");
        $this->db->where(TBL_USER . ".eStatus", "Active");

        if (isset($dateFilter) && $dateFilter != 'All')
            $this->db->where("DATE_FORMAT(" . TBL_QUESTION . ".dCreateDate,'%Y-%m-%d') >= DATE_ADD(NOW(),INTERVAL $dateFilter) AND DATE_FORMAT(" . TBL_QUESTION . ".dCreateDate,'%Y-%m-%d') <= DATE_FORMAT(NOW(),'%Y-%m-%d')", "", FALSE);


        $this->db->group_by(TBL_QUESTION . '.iQuestionID');
        $this->db->order_by('votes', 'DESC');
        $this->db->limit(10, $start);
        $query = $this->db->get();

        if ($query->num_rows > 0) {
            return $query->result();
        } else {
            return 0;
        }
    }

    /*     * *******************************************
      List of Questions based on most number of answers to a question   3
     * ***************************************************** */

    function mostAnsweredQuestion($start, $dateFilter, $iUserID) {

        $this->db->select(TBL_USER . ".vFirst," . TBL_USER . ".vLast,
                            COUNT(" . TBL_ANSWER . ".iQuestionID) as total_answers,
                            IF(ISNULL(iVote),0,iVote) AS iVote,
                            " . TBL_CATEGORY . ".iCategoryID,
                            " . TBL_CATEGORY . ".vCategory,
                            " . TBL_QUESTION . ".iQuestionID,
                            " . TBL_QUESTION . ".bQuestion,
                            " . TBL_QUESTION . ".eStatus,
                            " . TBL_QUESTION . ".eDelete,
							(select IF(sum(iVote) IS NULL,0, sum(iVote)) from tbl_vote where iItemID = tbl_question.iQuestionID and eType='Answer') as votes,
                            " . TBL_QUESTION . ".dCreateDate,
                            " . TBL_QUESTION . ".tModifyDate", false);

        $this->db->from(TBL_QUESTION);
        $this->db->join_using(TBL_USER, "iUserID");
        $this->db->join("(SELECT * FROM " . TBL_ANSWER . " WHERE eDelete = '0' AND eStatus = 'Accepted') as " . TBL_ANSWER, TBL_ANSWER . ".iQuestionID=" . TBL_QUESTION . ".iQuestionID", "LEFT", FALSE);
        $this->db->join("(SELECT * FROM " . TBL_VOTE . " WHERE iUserID = $iUserID AND eType = 'Question') as " . TBL_VOTE, TBL_VOTE . ".iItemID=" . TBL_QUESTION . ".iQuestionID", "LEFT", FALSE);

        $this->db->where(TBL_QUESTION . ".eDelete", "0");
        $this->db->where(TBL_QUESTION . ".eStatus", "Active");
        $this->db->where(TBL_USER . ".eDelete", "0");
        $this->db->where(TBL_USER . ".eStatus", "Active");

        if (isset($dateFilter) && $dateFilter != 'All') {
            $this->db->where("DATE_FORMAT(" . TBL_QUESTION . ".dCreateDate,'%Y-%m-%d') >= DATE_ADD(NOW(),INTERVAL $dateFilter)", "", FALSE);
            $this->db->where("DATE_FORMAT(" . TBL_QUESTION . ".dCreateDate,'%Y-%m-%d') <= DATE_FORMAT(NOW(),'%Y-%m-%d')", "", FALSE);
        }

        $this->db->group_by(TBL_QUESTION . '.iQuestionID');
        $this->db->order_by('total_answers', 'desc');
        $this->db->limit(10, $start);
        $query = $this->db->get();

        if ($query->num_rows > 0)
            return $query->result_array();
        else
            return 0;
    }

    function unAnsweredQuestion($iUserID, $index = "") {
//SELECT * FROM tbl_question WHERE iQuestionID NOT IN ( SELECT iQuestionID FROM tbl_answer GROUP BY iQuestionID)
        $this->db->select(TBL_QUESTION . ".*,
                            " . TBL_USER . ".vFirst,
                                vLast,
                                IF(ISNULL(iVote),0,iVote) AS iVote,
                                (select IF(sum(iVote) IS NULL,0, sum(iVote)) 
                                    from tbl_vote 
                                    where iItemID = tbl_question.iQuestionID 
                                    and eType = 'Question') as votes", FALSE);
        $this->db->from(TBL_QUESTION);
        $this->db->join_using("(SELECT iQuestionID FROM " . TBL_ANSWER . " WHERE iUserId <> " . $iUserID . ") as " . TBL_ANSWER, "iQuestionID", FALSE);
        $this->db->join("(SELECT * FROM " . TBL_VOTE . " WHERE " . TBL_VOTE . ".iUserID = " . $iUserID . " AND eType = 'Question') as " . TBL_VOTE, TBL_VOTE . ".iItemID=" . TBL_QUESTION . ".iQuestionID", "LEFT", FALSE);
        $this->db->join(TBL_USER, TBL_USER . ".iUserID = " . TBL_QUESTION . ".iUserID");
        $this->db->where(TBL_QUESTION . ".eStatus", "Active");
        $this->db->where(TBL_QUESTION . ".eDelete", "0");
        $this->db->limit(10, $index);

        $query = $this->db->get();

        if ($query->num_rows > 0)
            return $query->result_array();
        else
            return 0;
    }

    /*     * ******************************************** 
      List of questions with maximum number of UP votes (Up votes to a question)] 1
     * ******************************************* */

    function getTopQuestion($start, $dateFilter, $iUserID, $cid) {
        $this->db->select(TBL_USER . ".vFirst," . TBL_USER . ".vLast,
                                COUNT(" . TBL_ANSWER . ".iQuestionID) as total_answers,
                                (select IF(sum(iVote) IS NULL,0, sum(iVote)) from tbl_vote where iItemID = tbl_question.iQuestionID and eType='Question') as votes,
                                IF(ISNULL(iVote),0,iVote) AS iVote,
                                " . TBL_CATEGORY . ".iCategoryID,
                                " . TBL_CATEGORY . ".vCategory,
                                " . TBL_QUESTION . ".iQuestionID,
                                " . TBL_QUESTION . ".bQuestion,
                                " . TBL_QUESTION . ".dCreateDate", false);

        $this->db->from(TBL_QUESTION);
        $this->db->join_using(TBL_USER, "iUserID");
        $this->db->join("(SELECT * FROM " . TBL_CATEGORY . " WHERE eDelete = '0' AND eStatus = 'Active') as " . TBL_CATEGORY, TBL_CATEGORY . ".iCategoryID=" . TBL_QUESTION . ".iCategoryID", "LEFT", FALSE);
        $this->db->join("(SELECT * FROM " . TBL_ANSWER . " WHERE eDelete = '0' AND eStatus = 'Accepted') as " . TBL_ANSWER, TBL_ANSWER . ".iQuestionID=" . TBL_QUESTION . ".iQuestionID", "LEFT", FALSE);
        if ($iUserID != '') {
            $this->db->join("(SELECT * FROM " . TBL_VOTE . " WHERE iUserID = $iUserID AND eType = 'Question') as " . TBL_VOTE, TBL_VOTE . ".iItemID=" . TBL_QUESTION . ".iQuestionID", "LEFT", FALSE);
        } else {
            $this->db->join("(SELECT * FROM " . TBL_VOTE . " WHERE eType = 'Question') as " . TBL_VOTE, TBL_VOTE . ".iItemID=" . TBL_QUESTION . ".iQuestionID", "LEFT", FALSE);
        }
        $this->db->where(TBL_QUESTION . ".eDelete", "0");
        $this->db->where(TBL_QUESTION . ".eStatus", "Active");
        $this->db->where(TBL_USER . ".eDelete", "0");
        $this->db->where(TBL_USER . ".eStatus", "Active");
        if ($cid != '') {
            $this->db->where(TBL_QUESTION . ".iCategoryID", $cid);
        }
        if ($dateFilter != 'All' && $dateFilter != '') {
            $this->db->where("DATE_FORMAT(" . TBL_QUESTION . ".dCreateDate,'%Y-%m-%d') >= DATE_ADD(NOW(),INTERVAL $dateFilter)", "", FALSE);
            $this->db->where("DATE_FORMAT(" . TBL_QUESTION . ".dCreateDate,'%Y-%m-%d') <= DATE_FORMAT(NOW(),'%Y-%m-%d')", "", FALSE);
        }

        $this->db->group_by(TBL_QUESTION . '.iQuestionID');
        $this->db->order_by('total_answers', 'desc');
        if ($cid == '') {
            $this->db->order_by('votes', 'desc');
            $this->db->limit(10, $start);
        }
        $query = $this->db->get();

        if ($query->num_rows > 0)
            return $query->result_array();
        else
            return 0;
    }

    function getUserVoteStatus($iUserID, $iQuestionID) {
        $this->db->select("iVote");
        $this->db->from(TBL_VOTE);
        $this->db->where('iUserID', $iUserID);
        $this->db->where('eType', 'Question');
        $this->db->where('iItemID', $iQuestionID);

        $query = $this->db->get();
        if ($query->num_rows > 0) {
            return $query->row_array();
        } else {
            return '';
        }
    }
    
    function likeQuestionByUserID($iUserID){
        $this->db->select(TBL_QUESTION.".iQuestionID,vQuestion,vLocation,vQuestionImage,vQuestionThumbImage,".TBL_QUESTION.".dCreatedDate,"."vFirst,vLast,vUsername,vImage,".TBL_USER.".iUserID,".TBL_QUESTION.".dCreatedDate, iVote, votes,",FALSE);
        //$this->db->select(TBL_QUESTION.".iQuestionID,".TBL_USER.".iUserID,",FALSE);
        $this->db->from(TBL_QUESTION);
        $this->db->join("(SELECT iVoteID,iUserID,eType,iItemID,IF(ISNULL(iVote),0,iVote) AS iVote,IF(ISNULL(sum(iVote)),0,sum(iVote)) AS votes  FROM tbl_vote WHERE eType='Question'   GROUP BY iItemID ) as tbl_vote","tbl_question.iQuestionID=tbl_vote.iItemID","",FALSE);
        $this->db->join(TBL_USER,"tbl_question.iUserID=tbl_user.iUserID","",FALSE);
        $this->db->where(TBL_QUESTION.".eStatus",'Active');
        $this->db->where(TBL_QUESTION.".eDelete",'0');
        $this->db->where(TBL_VOTE.".iUserID",$iUserID);
        
        
        $this->db->where(TBL_USER.".eStatus",'Active');
        $this->db->where(TBL_USER.".eDelete",'0');
        $this->db->order_by(TBL_QUESTION.".dCreatedDate","DESC");
        
        $query = $this->db->get();
        if ($query->num_rows > 0) {
            return $query->result_array();
        } else {
            return '';
        }
        
        
//        $query=$this->db->query("select * from tbl_question 
//            join(SELECT iVoteID,iUserID,eType,iItemID,IF(ISNULL(iVote),0,iVote) AS iVote,
//            IF(ISNULL(sum(iVote)),0,sum(iVote)) AS votes  
//            FROM tbl_vote WHERE eType='Question'    GROUP BY iItemID) as tbl_vote 
//            on tbl_question.iQuestionID=tbl_vote.iItemID join 
//            tbl_user on tbl_question.iUserID=tbl_user.iUserID where tbl_vote.iUserID=".$iUserID."");
//        
//        if ($query->num_rows > 0)
//            return $query->result_array();
//        else
//            return 0;
        
    }

}
?>

