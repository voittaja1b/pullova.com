<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class answer_model extends CI_Model {

    var $viewData = array();

    function __construct() {
        parent::__construct();
    }

    function addAnswerRecord($postData) {
        $postArray = $this->general_model->getDatabseFields($postData, TBL_ANSWER);
        $query = $this->db->insert(TBL_ANSWER, $postArray);
        $total_rows = $this->db->affected_rows();
        if ($total_rows > 0)
            return $this->db->insert_id();
        else
            return '';
    }

    function getAnswerRecord($iQuestionID, $iUserID = '') {
//        $query = $this->db->query("SELECT u.vUsername,u.vFirst,u.vLast,a.iAnswerId,a.iUserId,a.iQuestionID,a.tAnswer,a.dCreatedDate,IF(ISNULL(iVote),0,iVote) AS iVote,IF(ISNULL(sum(iVote)),0,sum(iVote)) AS votes"
//                . " FROM " . TBL_ANSWER . " a"
//                . " LEFT JOIN  (SELECT * FROM " . TBL_VOTE . " WHERE eType = 'Answer' ) as " . TBL_VOTE . " ON (" . TBL_VOTE . ".iItemID=a.iAnswerID)"
//                . " LEFT JOIN " . TBL_USER . " u ON (a.iUserID = u.iUserID )"
//                . " WHERE a.iQuestionID = '$iQuestionID'"
//                . " GROUP BY u.iUserID");
        $query = $this->db->query("SELECT u.vUsername,u.vImage,u.vFirst,u.vLast,a.iAnswerID,a.iUserID,a.iQuestionID,a.tAnswer,a.dCreatedDate,IF(ISNULL(iVote),0,iVote) AS iVote,IF(ISNULL(sum(iVote)),0,sum(iVote)) AS votes"
                . " FROM " . TBL_ANSWER . " a"
                . " LEFT JOIN  (SELECT * FROM " . TBL_VOTE . " WHERE eType = 'Answer' ) as " . TBL_VOTE . " ON (" . TBL_VOTE . ".iItemID=a.iAnswerID)"
                . " LEFT JOIN " . TBL_USER . " u ON (a.iUserID = u.iUserID )"
                . " WHERE a.iQuestionID = '$iQuestionID'"
                . " GROUP BY a.iAnswerID");     
        if ($query->num_rows > 0)
            return $query->result_array();
        else
            return array();
    }

    function getAnswerByID($iAnswerID) {

        $query = $this->db->query("SELECT u.vUsername,u.vFirst,u.vLast,a.iAnswerId,a.iUserId,a.iQuestionID,a.tAnswer,u.vImage,a.dCreatedDate,IF(ISNULL(iVote),0,iVote) AS iVote,IF(ISNULL(sum(iVote)),0,sum(iVote)) AS votes"
                . " FROM " . TBL_ANSWER . " a"
                . " LEFT JOIN  (SELECT * FROM " . TBL_VOTE . " WHERE eType = 'Answer') as " . TBL_VOTE . " ON (" . TBL_VOTE . ".iItemID=a.iAnswerID)"
                . " LEFT JOIN " . TBL_USER . " u ON (a.iUserID = u.iUserID )"
                . " WHERE a.iAnswerID = '$iAnswerID'"
                . " GROUP BY u.iUserID");
        if ($query->num_rows > 0)
            return $query->row_array();
        else
            return 0;
    }

    function topVotedAnswer($start, $dateFilter, $iUserID) {
        $this->db->select(TBL_USER . ".vFirst,
                            " . TBL_USER . ".vLast,
                            " . TBL_QUESTION . ".iQuestionID,
                            " . TBL_QUESTION . ".vQuestion,
                            IF(ISNULL(iVote),0,iVote) AS iVote,
                            " . TBL_ANSWER . ".*,
                            (select IF(sum(iVote) IS NULL,0, sum(iVote)) from tbl_vote where iItemID = tbl_question.iQuestionID and eType = 'Answer') as votes,", false);
        $this->db->from(TBL_ANSWER);
        $this->db->join_using(TBL_USER, "iUserID");
        $this->db->join_using(TBL_QUESTION, "iQuestionID");
        $this->db->join("(SELECT * FROM " . TBL_VOTE . " WHERE iUserID = $iUserID AND eType = 'Answer') as " . TBL_VOTE, TBL_VOTE . ".iItemID=" . TBL_ANSWER . ".iAnswerID", "LEFT", FALSE);
        $this->db->where(TBL_QUESTION . ".eDelete", "0");
        $this->db->where(TBL_QUESTION . ".eDelete", "0");
//        $this->db->where(TBL_ANSWER . ".eStatus", "Accepted");
//        $this->db->where(TBL_ANSWER . ".eDelete", "0");
        $this->db->where(TBL_USER . ".eDelete", "0");
        $this->db->where(TBL_USER . ".eStatus", "Active");

        if (isset($dateFilter) && $dateFilter != 'All')
            $this->db->where("DATE_FORMAT(" . TBL_ANSWER . ".dCreateDate,'%Y-%m-%d') >= DATE_ADD(NOW(),INTERVAL $dateFilter) AND DATE_FORMAT(" . TBL_ANSWER . ".dCreateDate,'%Y-%m-%d') <= DATE_FORMAT(NOW(),'%Y-%m-%d')", "", FALSE);

        $this->db->group_by(TBL_ANSWER . '.iAnswerID');
        $this->db->order_by('votes', 'desc');
        $this->db->limit(10, $start);

        $query = $this->db->get();
        if ($query->num_rows > 0)
            return $query->result_array();
        else
            return 0;
    }
    function getUserVoteStatus($iUserID,$iAnswerID){
        $this->db->select("iVote");
        $this->db->from(TBL_VOTE);
        $this->db->where('iUserID',$iUserID);
        $this->db->where('eType','Answer');
        $this->db->where('iItemID',$iAnswerID);        
        
        $query=$this->db->get();
        if($query->num_rows > 0){
           return $query->row_array(); 
        }else{
            return '';
        }
    }
}   
?>


