<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of agenda_model
 *
 * @author Dhava Patel
 */
class Country_model extends CI_Model {

    //put your code here
    function __construct() {
        parent::__construct();
    }

    /*     * ***********************************************
     *
     *   GET ALL COUNTRY DATA
     *
     * ********************************************** */

    function getCountryAllData($offset,$postnumbers) {
        $this->db->select('iCountryID ,vCountryCode,vCountryName');
        $this->db->from(TBL_COUNTRY);
        $this->db->limit($postnumbers,$offset);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return '';
    }

}

?>
