<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Vote_model extends CI_Model {

    function __construct() {
        parent::__construct();
    }

    var $viewData = array();

    function getVote($iID = '', $flag = '') {
        if ($flag == 'a') {
            $result_vote = $this->db->get_where(TBL_VOTE, array('iItemID' => $iID, 'eType' => 'Answer'));
        }else{
            $result_vote = $this->db->get_where(TBL_VOTE, array('iItemID' => $iID, 'eType' => 'Question'));
        }
        if ($result_vote->num_rows > 0) {
            $row = $result_vote->result_array();
            return $row;
        } else {
            return 0;
        }
    }

    function getUserVote($iItemID, $iUserID, $eType) {
        
        $result_vote = $this->db->get_where(TBL_VOTE, array('iItemID' => $iItemID, 'iUserID' => $iUserID, 'eType' => $eType));
        if ($result_vote->num_rows > 0) {
            return $result_vote->row_array();
        } else {
            return "";
        }
    }

    function updateVote($postData, $iVoteID) {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_VOTE);
        $query = $this->db->update(TBL_VOTE, $postArray, array('iVoteID' => $iVoteID));

        if ($this->db->affected_rows() >= 0)
            return $iVoteID;
        else
            return 0;
    }

    function addVote($postData) {
        $postArray = $this->general_model->getDatabseFields($postData, TBL_VOTE);
        $query = $this->db->insert(TBL_VOTE, $postArray);
        $total_rows = $this->db->affected_rows();
        if ($total_rows > 0)
            return $this->db->insert_id();
        else
            return 0;
    }

}
?>


