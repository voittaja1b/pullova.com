<?php



/*

 * To change this template, choose Tools | Templates

 * and open the template in the editor.

 */



class Promo_code_model extends CI_Model

{



    function __construct()

    {

        parent::__construct();

    }

	function checkCode($vPromoCode, $vPhoneNumber) {
		$this->db->select("*");
		$this->db->from(TBL_PROMO_CODE);
		$this->db->where("vCode", $vPromoCode);

    $query = $this->db->get();
    if ($query->num_rows() < 1) {
        return 0;
    }
    $row = $query->row();
		if (isset($row->vPhoneNumberList) && $row->vPhoneNumberList != '') {
			if (strpos($row->vPhoneNumberList, $vPhoneNumber) === false) {
				return 0;
			}
		}
		return 1;
	}
	

	function getData($vPromoCode) {
		$this->db->select("*");
		$this->db->from(TBL_PROMO_CODE);
		$this->db->where("vCode", $vPromoCode);

    $query = $this->db->get();
    if ($query->num_rows() < 1) {
        return null;
    }
    return $query->row();
	}
	


}



?>