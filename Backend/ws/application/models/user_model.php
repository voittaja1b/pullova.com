<?php



/*

 * To change this template, choose Tools | Templates

 * and open the template in the editor.

 */



class User_model extends CI_Model

{



    function __construct()

    {

        parent::__construct();

    }



    function checkLoginByUsernameAndDeviceToken($vUsername, $vPassword, $vDeviceToken)

    {

        $this->db->select("*");

        $this->db->where("vUsername", $vUsername);

        $this->db->where("vPassword", md5($vPassword));

        if (isset($vDeviceToken) && $vDeviceToken != '')

            $this->db->where("vDeviceToken", $vDeviceToken);

        $this->db->where("eStatus", 'Active');

        $this->db->where("eDelete", '0');

        $this->db->where("eUserType", 'User');

        $this->db->from(TBL_USER);



        $query = $this->db->get();

        $row = $query->row();

        if ($query->num_rows() > 0)

        {

            return 1;

        }

        else

            return 0;

    }



    function checkLoginByEmailAndDeviceToken($vEmail, $vPassword, $vDeviceToken)

    {



        $this->db->select("*");

        $this->db->where("vEmail", $vEmail);

        $this->db->where("vPassword", md5($vPassword));

        if (isset($vDeviceToken) && $vDeviceToken != '')

            $this->db->where("vDeviceToken", $vDeviceToken);

        $this->db->where("eStatus", 'Active');

        $this->db->where("eDelete", '0');

        $this->db->where("eUserType", 'User');



        $this->db->from(TBL_USER);

        $query = $this->db->get();

        if ($query->num_rows() > 0)

            return TRUE;

        else

            return FALSE;

    }



    function checkvEmailExistOrNot($vEmail)

    {

        $this->db->select("vEmail");

        $this->db->from(TBL_USER);

        $this->db->where("vEmail", $vEmail);

        $query = $this->db->get();

        if ($query->num_rows() > 0)

        {

            return 1;

        }

        else

        {

            return 0;

        }

    }



    function addUser($postData)

    {



        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);

        $query = $this->db->insert(TBL_USER, $postArray);



        if ($this->db->affected_rows() > 0)

            return $this->db->insert_id();

        else

            return 0;

    }



    function editUserDataByID($postData)

    {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);



        $query = $this->db->update(TBL_USER, $postArray, array('vEmail' => $postData['vEmail']));



        if ($this->db->affected_rows() >= 0)

            return 1;

        else

            return '';

    }


    function getUserProfile($iUserID)

    {

        $this->db->from(TBL_USER);

        $this->db->where("iUserID", $iUserID);

        $query = $this->db->get();

        if ($query->num_rows == 1)

        {

            $rows = $query->row_array();

            //pre($rows);



            $return_data['vFirst'] = $rows['vFirst'];

            $return_data['vLast'] = $rows['vLast'];

            $return_data['bAboutMe'] = $rows['bAboutMe'];

            if ($rows['vImage'] != '')

            {

                $return_data['vImage'] = PROFILE_THUMB_URL . $rows['vImage'];

            }

            else

            {

                $return_data['vImage'] = "";

            }

            //pre($return_data);

            return $return_data;

        }

        else

        {

            return 0;

        }

    }



    function checkLoginByFbIDAndDeviceToken($vFbID, $vDeviceToken)

    {



        $this->db->select("*");

        $this->db->where("vFbID", $vFbID);

        if (isset($vDeviceToken) && $vDeviceToken != '')

            $this->db->where("vDeviceToken", $vDeviceToken);

        $this->db->where("eStatus", 'Active');

        $this->db->where("eDelete", '0');

        $this->db->where("eUserType", 'User');



        $this->db->from(TBL_USER);

        $query = $this->db->get();

        if ($query->num_rows() > 0)

            return $query->row_array();

        else

            return FALSE;

    }



    function editUserDataByEmailOrUsernameOrfbID($postData)

    {



        if (isset($postData['vUsername']) && $postData['vUsername'] != "")

            $where = array(

                'vUsername' => $postData['vUsername']

            );

        else if (isset($postData['vEmail']) && $postData['vEmail'] != "")

            $where = array(

                'vEmail' => $postData['vEmail']

            );

        else if (isset($postData['vFbID']) && $postData['vFbID'] != "")

            $where = array(

                'vFbID' => $postData['vFbID']

            );

        else if (isset($postData['iUserID']) && $postData['iUserID'] != "")

            $where = array(

                'iUserID' => $postData['iUserID']

            );



        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);

        array_filter($postArray);

        $query = $this->db->update(TBL_USER, $postArray, $where);

        if ($this->db->affected_rows() >= 0)

            return 1;

        else

            return '';

    }



    function getUserDataByFbID($postData)

    {



        $this->db->select(TBL_USER . ".iUserID,IFNULL(vUsername,'') as vUsername,vEmail,eLogin,vFirst,vLast,vImage,dLoginDate,vFbID,userPhone,codefordiscount,version,typeofcard,last4,vToken,verifiedPhone,vDriverorNot,driverLicensePlate,vCarType,vDriverEarnings,vDriverRoutingNr,vDriverAccountNr,vDriverValue", FALSE);

        $this->db->from(TBL_USER);



        if (isset($postData['vFbID']) && $postData['vFbID'] != "")

            $this->db->where(TBL_USER . ".vFbID", $postData['vFbID']);



        $this->db->where(TBL_USER . ".eStatus", 'Active');

        $this->db->where(TBL_USER . ".eDelete", '0');

        $this->db->where("eUserType", 'User');



        $query = $this->db->get();



        if ($query->num_rows > 0)

            return $query->row_array();

        else

            return '';

    }



    function checkUserEmailAvailable($vEmail, $iUserID = '')

    {

        if (isset($iUserID) && $iUserID != '')

            $ucheck = array('vEmail' => $vEmail, 'iUserID <>' => $iUserID);

        else

            $check = array('vEmail' => $vEmail);

        $result = $this->db->get_where(TBL_USER, (isset($ucheck) && $ucheck != '') ? $ucheck : $check);

        if ($result->num_rows() >= 1)

            return 0;

        else

            return 1;

    }

        function checkPhoneNumber($userPhone, $iUserID = '')

    {

        if (isset($iUserID) && $iUserID != '')

            $ucheck = array('userPhone' => $userPhone, 'iUserID <>' => $iUserID);

        else

            $check = array('userPhone' => $userPhone);

        $result = $this->db->get_where(TBL_USER, (isset($ucheck) && $ucheck != '') ? $ucheck : $check);

        if ($result->num_rows() >= 1)

            return 0;

        else

            return 1;

    }


    function checkUsernameAvailable($vUsername, $iUserID = '')

    {

        if (isset($iUserID) && $iUserID != '')

            $ucheck = array('vUsername' => $vUsername, 'iUserID <>' => $iUserID, "eDelete" => "0");

        else

            $check = array('vUsername' => $vUsername, "eDelete" => "0");



        $result = $this->db->get_where(TBL_USER, (isset($ucheck) && $ucheck != '') ? $ucheck : $check);



        if ($result->num_rows() >= 1)

            return 0;

        else

            return 1;

    }



    function getUserDataByUsername($vUsername)

    {

        $this->db->select(TBL_USER . ".iUserID,IFNULL(vUsername,'') as vUsername,vEmail,eLogin,vFirst,vLast,vImage,dLoginDate,vFbID,userPhone,codefordiscount,version,typeofcard,last4,vToken,verifiedPhone,vDriverorNot,driverLicensePlate,vCarType,vDriverEarnings,vDriverRoutingNr,vDriverAccountNr,vDriverValue", FALSE);

        $this->db->from(TBL_USER);

        $this->db->where("vUsername", $vUsername);

        $this->db->where(TBL_USER . ".eStatus", 'Active');

        $this->db->where(TBL_USER . ".eDelete", '0');

        $this->db->where("eUserType", 'User');

        $query = $this->db->get();

        if ($query->num_rows > 0)

            return $query->row_array();

        else

            return '';

    }



    function getUserDataByEmail($vEmail)

    {

        
        //$this->db->select(TBL_USER . ".iUserID,IFNULL(vUsername,'') as vUsername,vEmail,eLogin,vFirst,vLast,vImage,dLoginDate,vFbID,userPhone,codefordiscount,vToken", FALSE);
        
        $this->db->select("tu.iUserID,IFNULL(vUsername,'') as vUsername,vEmail,eLogin,vFirst,vLast,vImage,dLoginDate,vFbID,userPhone,codefordiscount,version,typeofcard,last4,vToken,verifiedPhone,vDriverorNot,driverLicensePlate,vCarType,vDriverEarnings,vDriverRoutingNr,vDriverAccountNr,vDriverValue", FALSE);

        $this->db->from(TBL_USER . ' tu');

        $this->db->where("vEmail", $vEmail);

        $this->db->where("tu.eStatus", 'Active');

        $this->db->where("tu.eDelete", '0');

        $this->db->where("tu.eUserType", 'User');

        $query = $this->db->get();

        if ($query->num_rows > 0)

            return $query->row_array();

        else

            return '';

    }



    function getUserDataByID($iUserID)

    {

        $this->db->select("iUserID,IFNULL(vUsername,'') as vUsername,vEmail,vFirst,vLast,vImage,eLogin,dLoginDate,vFbID,vDeviceToken,userPhone,verifiedPhone,codefordiscount,version,vDriverorNot,driverLicensePlate,vCarType,vDriverEarnings,vDriverRoutingNr,vDriverAccountNr,vDriverValue", FALSE);
            
        $this->db->from(TBL_USER);

        $this->db->where("iUserID", $iUserID);

        $this->db->where("eStatus", 'Active');

        $this->db->where("eDelete", '0');

        $this->db->where("eUserType", 'User');

        $query = $this->db->get();

        if ($query->num_rows > 0)

            return $query->row_array();

        else

            return '';

    }



    function getUserData($vEmail)

    {

        $this->db->from(TBL_USER);

        $this->db->where("vEmail", $vEmail);

        $query = $this->db->get();

        if ($query->num_rows == 1)

        {

            $row = $query->row();

            return $row;

        }

        else

        {

            return 0;

        }

    }



    function searchuser($q, $iUserID)

    {

        $query = $this->db->query("SELECT tu.iUserID,IFNULL(vUsername,'') as vUsername,tu.vEmail,tu.vFirst,tu.vLast,tu.vImage,tu.eLogin,tu.dLoginDate,tu.vFbID

                FROM tbl_user tu

                    WHERE (tu.vUsername LIKE '%" . $q . "%' 

                            OR tu.vFirst LIKE '%" . $q . "%' 

                            OR tu.vLast LIKE '%" . $q . "%' 

                            OR tu.vEmail LIKE '%" . $q . "%' )

                    AND tu.iUserID != '" . $iUserID . "'    

                    AND tu.eStatus = 'Active'    

                    AND tu.eDelete = '0'    

                    AND tu.eUserType = 'User'

                    GROUP BY iUserID

                ");

//        $this->db->select("iUserID,IFNULL(vUsername,'') as vUsername,vEmail,vFirst,vLast,vImage,eLogin,dLoginDate,vFbID", FALSE);

//        $this->db->from(TBL_USER);

//        $this->db->like('vFirst', $q, 'both');

//        $this->db->or_like('vLast', $q, 'both');

//        $this->db->or_like('vEmail', $q, 'both');

//        $this->db->where("iUserID !=", $iUserID);

//        $this->db->where("eStatus", 'Active');

//        $this->db->where("eDelete", '0');

//        $this->db->where("eUserType", 'User');

//        $query = $this->db->get();

        if ($query->num_rows > 0)

            return $query->result_array();

        else

            return '';

    }



    function getFriendshipStatus($iSenderID, $iFriendID)

    {

        $this->db->select("eStatus,iFriendID", FALSE);

        $this->db->from(TBL_FRIEND);

        $where = "(iSenderID = '$iSenderID' AND iFriendID = '$iFriendID') OR (iSenderID = '$iFriendID`' AND iFriendID = '$iSenderID') ";

        $this->db->where($where);

        $this->db->group_by("iSenderID");

        $query = $this->db->get();

        if ($query->num_rows > 0)

            return $query->row_array();

        else

            return '';

    }



    function checkUsernameExistence($vUsername, $iUserID)

    {

        $this->db->select("vUsername", FALSE);

        $this->db->from(TBL_USER);

        $this->db->where("vUsername", $vUsername);

        $this->db->where("iUserID <>", $iUserID);

        $this->db->where("eStatus", 'Active');

        $this->db->where("eDelete", '0');

        $this->db->where("eUserType", 'User');

        $query = $this->db->get();

        if ($query->num_rows > 0)

            return 1;

        else

            return 0;

    }



    function editProfile($postData)

    {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);

        if ($this->db->update(TBL_USER, $postArray, array('iUserID' => $postData['iUserID'])))

            return 1;

        else

            return 0;

    }


    function addPromoCode($postData)

    {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);

        if ($this->db->update(TBL_USER, $postArray, array('iUserID' => $postData['iUserID'])))

            return 1;

        else

            return 0;

    }


    function checkActivationKey($vActivationKey)

    {



        $query = $this->db->get_where(TBL_USER, array("vActivationKey" => $vActivationKey));



        if ($query->num_rows() > 0)

            return $query->row_array();

        else

            return '';

    }



    /*     * ***********************************************

     *   ACTIVATE USER ACCOUNT

     * **************************************************** */



    function activateUser($postData)

    {



        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);



        $query = $this->db->update(TBL_USER, $postArray, array('vActivationKey' => $postData['vActivate']));



        if ($query)

            return $query;

        else

            return $this->db->_error_message();

    }



    function addDeviceToken($postData)

    {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);

        $query = $this->db->update(TBL_USER, $postArray, array('iUserID' => $postData['iUserID']));

        if ($query)

            return $query;

        else

            return $this->db->_error_message();

    }



    function removeDeviceToken($postData)

    {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);

        $query = $this->db->update(TBL_USER, $postArray, array('iUserID' => $postData['iUserID']));

        if ($query)

            return $query;

        else

            return $this->db->_error_message();

    }



    function getUserByID($iUserID)

    {

        $this->db->from(TBL_USER);

        $this->db->where("iUserID", $iUserID);

        $this->db->where("eStatus", 'Active');

        $this->db->where("eDelete", '0');

        $query = $this->db->get();

        if ($query->num_rows > 0)

            return $query->row_array();

        else

            return 0;

    }



    function checkOldPassword($postData)

    {



        $this->db->from(TBL_USER);

        $this->db->where('iUserID', $postData['iUserID']);

        $this->db->where('vPassword', $postData['vPassword']);

        $this->db->where('eStatus', 'Active');

        $this->db->where('eDelete', '0');



        $query = $this->db->get();



        if ($query->num_rows() >= 1)

            return 1;



        return 0;

    }



    function changePassword($postData)

    {



        $postArray = $this->general_model->getDatabseFields($postData, TBL_USER);

        $query = $this->db->update(TBL_USER, $postArray, array('iUserID ' => $postData['iUserID']));



        if ($this->db->affected_rows() >= 0)

            return true;

        else

            return $this->db->_error_message();

    }



    function getUserID($postData)

    {

        $this->db->select('iUserID');

        $this->db->from(TBL_USER);

        if ($postData['vUsername'])

        {

            $this->db->where('vUsername', $postData['vUsername']);

        }

        elseif ($postData['vEmail'])

        {

            $this->db->where('vEmail', $postData['vEmail']);

        }

        elseif ($postData['vFbID'])

        {

            $this->db->where('vFbID', $postData['vFbID']);

        }

        $query = $this->db->get();

        if ($query->num_rows > 0)

            return $query->row_array();

        else

            return 0;

    }



    function setFriendRequest($postData)

    {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_FRIENDS);

        $query = $this->db->insert(TBL_FRIENDS, $postArray);



        if ($this->db->affected_rows() > 0)

            return $this->db->insert_id();

        else

            return 0;

    }



    function checkFriendship($friends)

    {

        $iFriendRequestBy = $friends['iFriendRequestBy'];

        $iFriendRequestTo = $friends['iFriendRequestTo'];

        $query = $this->db->query("SELECT * FROM tbl_friends "

                . "WHERE ((iFriendRequestBy ='" . $iFriendRequestBy . "' AND iFriendRequestTo ='" . $iFriendRequestTo . "') "

                . "|| (iFriendRequestBy ='" . $iFriendRequestTo . "' AND iFriendRequestTo ='" . $iFriendRequestBy . "')) "

                . "AND eRequestStatus != 'Allowed' ");

        if ($query->num_rows() >= 1)

            return 1;

        else

            return 0;

    }



    function checkLocationRequestStatus($iSenderID, $iRecieverID)

    {

        $this->db->from(TBL_LOCATION_REQUEST);

        $this->db->where('iSenderID', $iSenderID);

        $this->db->where('iRecieverID', $iRecieverID);

        $query = $this->db->get();



        if ($query->num_rows() >= 1)

            return $query->row_array();



        return 0;

    }



    function addLocationRequest($postData)

    {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_LOCATION_REQUEST);

        $query = $this->db->insert(TBL_LOCATION_REQUEST, $postArray);



        if ($this->db->affected_rows() > 0)

            return $this->db->insert_id();

        else

            return 0;

    }



    function updateLocationRequest($postData)

    {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_LOCATION_REQUEST);

        $query = $this->db->update(TBL_LOCATION_REQUEST, $postArray, array('iLocationRequestID ' => $postData['iLocationRequestID']));



        if ($this->db->affected_rows() >= 0)

            return true;

        else

            return $this->db->_error_message();

    }



    function getLocationRequestPendingList($iUserID)

    {

        $this->db->select('tr.iSenderID,tr.iRecieverID,tu.iUserID,tu.vUsername,tu.vEmail,tu.vFirst,tu.vLast,tu.vImage,tu.eLogin,tu.dLoginDate,tu.vFbID');

        $this->db->from(TBL_LOCATION_REQUEST . ' tr');

        $this->db->join(TBL_USER . ' tu', 'tr.iSenderID = tu.iUserID', "LEFT");

        $this->db->where('iRecieverID', $iUserID);

        $this->db->where('tr.eStatus', '0');

        $query = $this->db->get();



        if ($query->num_rows() > 0)

            return $query->result_array();

        else

            return 0;

    }



    function getSentLocationRequestList($iUserID)

    {

        $this->db->select('tr.iSenderID,tr.iRecieverID,tr.eStatus,tu.iUserID,tu.vUsername,tu.vEmail,tu.vFirst,tu.vLast,tu.vImage,tu.eLogin,tu.dLoginDate,tu.vFbID');

        $this->db->from(TBL_LOCATION_REQUEST . ' tr');

        $this->db->join(TBL_USER . ' tu', 'tr.iRecieverID = tu.iUserID', "LEFT");

        $this->db->where('iSenderID', $iUserID);

        $query = $this->db->get();



        if ($query->num_rows() > 0)

            return $query->result_array();

        else

            return 0;

    }



    function updateRequestStatus($iSenderID, $iRecieverID, $postData)

    {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_LOCATION_REQUEST);

        $this->db->where('iSenderID', $iSenderID);

        $this->db->where('iRecieverID', $iRecieverID);

        $this->db->update(TBL_LOCATION_REQUEST, $postArray);

//        pre($this->db->affected_rows());

        if ($this->db->affected_rows() >= 0)

            return true;

        else

            return $this->db->_error_message();

    }



    function getUserLocationRequestResponseList($iUserID)

    {

        $this->db->select('tr.iSenderID,tr.iRecieverID,tr.eStatus,tu.iUserID,tu.vUsername,tu.vEmail,tu.vFirst,tu.vLast,tu.vImage,tu.eLogin,tu.dLoginDate,tu.vFbID,tr.fLat,tr.fLong');

        $this->db->from(TBL_LOCATION_REQUEST . ' tr');

        $this->db->join(TBL_USER . ' tu', 'tr.iRecieverID = tu.iUserID', "LEFT");

        $this->db->where('iSenderID', $iUserID);

        $where = "(tr.eStatus='1' OR tr.eStatus='2')";

        $this->db->where($where, '', FALSE);

        $query = $this->db->get();



        if ($query->num_rows() > 0)

            return $query->result_array();

        else

            return 0;

    }



    function addFeed($postData)

    {

        $postFeedArray = $this->general_model->getDatabseFields($postData, TBL_FEED);

        $this->db->insert(TBL_FEED, $postFeedArray);

        $postData['iFeedID'] = $this->db->insert_id();

        $postArray = $this->general_model->getDatabseFields($postData, TBL_FEED_TAG_FRIEND);

        if(isset($postArray['iFriendID']) && !empty($postArray['iFriendID']))

        {

            foreach ($postArray['iFriendID'] as $k => $v)

            {

                $postTagArray[$k]['iFeedID'] = $this->db->insert_id();

                $postTagArray[$k]['iFriendID'] = $v;

                $postTagArray[$k]['dCreatedDate'] = $postArray['dCreatedDate'];

            }

            $this->db->insert_batch(TBL_FEED_TAG_FRIEND, $postTagArray);

        }

        if ($this->db->affected_rows() > 0)

            return $postData['iFeedID'];

        else

            return 0;

    }
	function doInsertFeedWithDrivers($postData)
    {
        $this->db->insert_batch(TBL_NEAREST_DRIVERS, $postData);
        if ($this->db->affected_rows() > 0)
            return $this->db->insert_id();
        else
            return 0;
    }
	function getLatestFeedId()
	{
	$sql="SELECT iFeedID,`iUserID`, `fLat`, `fLong`,vCost FROM `tbl_feed` ORDER BY iFeedID limit 1";
	
	$query = $this->db->query($sql);
	
	if ($query->num_rows > 0)
	{
		return $query->result_array();
	} else {
		return 0;
	}
	}
	
	function getDriverDistanceByUserID($iUserID,$fLat,$fLong)
	{
	$sql="SELECT `iUserID`, `fLat`, `fLong`,(((acos(sin((".$fLat."*pi()/180)) * 
            sin((`fLat`*pi()/180))+cos((".$fLat."*pi()/180)) * 
            cos((`fLat`*pi()/180)) * cos(((".$fLong."- `fLong`)* 
            pi()/180))))*180/pi())*60*1.1515
        ) as distance FROM `tbl_user` WHERE vDriverorNot = 'driver' AND onlineornot = 'online' ORDER BY distance ASC LIMIT 0,1";
		
	$query = $this->db->query($sql);
	
	if ($query->num_rows > 0)
	{
		return $query->result_array();
	} else {
		return 0;
	}
	
	}
	function getDriversWithDistance($fLat,$fLong)
	{
	$sql="SELECT `iUserID`,(((acos(sin((".$fLat."*pi()/180)) * 
            sin((`fLat`*pi()/180))+cos((".$fLat."*pi()/180)) * 
            cos((`fLat`*pi()/180)) * cos(((".$fLong."- `fLong`)* 
            pi()/180))))*180/pi())*60*1.1515
        ) as distance FROM `tbl_user` WHERE vDriverorNot = 'driver' AND onlineornot = 'online' ORDER BY distance ASC";
		
	$query = $this->db->query($sql);
	
	if ($query->num_rows > 0)
	{
		return $query->result_array();
	} else {
		return 0;
	}
	
	}
	function getFeedInfoByDriverID($driver_id)
	{
		/*$iFeedID = '';
		$this->db->select('iFeedID');
        $this->db->from(TBL_NEAREST_DRIVERS);
		$this->db->order_by("iFeedID","DESC");
		$this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() > 0) {
       		$topMostFeed	= $query->result_array();
			$iFeedID		= $topMostFeed[0]['iFeedID'];	
		}*/
			
		$this->db->select('td.ID,td.iFeedID,td.iUserID,td.fDistance,tf.vCost');
        $this->db->from(TBL_NEAREST_DRIVERS . ' td');
        $this->db->join(TBL_FEED . ' tf', 'td.iFeedID = tf.iFeedID', "LEFT");
        $this->db->where('td.iUserID', $driver_id);
		//$this->db->where('td.iFeedID', $iFeedID);
		$this->db->where('td.status', "send");
		$this->db->order_by("td.fDistance","ASC");
		$this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return 0;
	}
	function doUpdateBookingStatus($postData,$Id='',$iFeedID='')
	{
		$postArray = $this->general_model->getDatabseFields($postData, TBL_NEAREST_DRIVERS);
        $this->db->where('ID', $Id);
        $this->db->update(TBL_NEAREST_DRIVERS, $postArray);
		
		if($postData["status"]=="accepted" && $iFeedID!=''){
			$postData2 = array();
			$postData2["status"] = "rejected";
			$postArray = $this->general_model->getDatabseFields($postData2, TBL_NEAREST_DRIVERS);
			$this->db->where('ID !=', $Id);
			$this->db->where('iFeedID', $iFeedID);
			$this->db->update(TBL_NEAREST_DRIVERS, $postArray);
		}
		if ($this->db->affected_rows() >= 0)
			return 1;
		else
			return '';
	}
	
	/*function getLatestInfoByUserID($iUserID,$fLat,$fLong)
	{
	echo $sql="SELECT `iUserID`, `fLat`, `fLong`,(((acos(sin((".$fLat."*pi()/180)) * 
            sin((`fLat`*pi()/180))+cos((".$fLat."*pi()/180)) * 
            cos((`fLat`*pi()/180)) * cos(((".$fLong."- `fLong`)* 
            pi()/180))))*180/pi())*60*1.1515
        ) as distance FROM `tbl_user` ORDER BY distance ASC LIMIT 0,1"; exit;	
		
	$query = $this->db->query($sql);
	
	if ($query->num_rows > 0)
	{
		return $query->result_array();
	} else {
		return 0;
	}
	
	}*/


	function array_msort($array, $key, $order = "ASC"){
		$tmp = array();
		foreach($array as $akey => $array2){
			$tmp[$akey] = $array2[$key];
		}
	   
		if($order == "DESC"){
			arsort($tmp , SORT_NUMERIC );
		} else {
			asort($tmp , SORT_NUMERIC );
		}
	
		$tmp2 = array();  
		$cnt  = 0;     
		foreach($tmp as $key => $value){
			$tmp2[$cnt++] = $array[$key];
		}       
		return $tmp2;
	}		

	function getImplodeArrays($strDetails,$strFieldName) {
		for($k=0;$k<count($strDetails);$k++) {
			$strDetails[$k] = "'".$strDetails[$k][$strFieldName]."'";
		}
		if($strDetails) {
			$strDetailsIDS = implode(",",$strDetails);
			return $strDetailsIDS;
		} else {
			return 0;
		}	
	}


    function getFeedList($iUserID)
    {
		$this->load->model("connection_model");
		$get_friend = $this->connection_model->getMemberFriendByID($iUserID);

		if ($get_friend) {
			$results	=	$this->getImplodeArrays($get_friend,'iUserID');	
			if($results) {
				$strWhere	= " AND (f.iUserID IN($results) OR f.iUserID = '".$iUserID."') ";
			}
            
	        $sql	=	"SELECT * FROM `tbl_feed` WHERE `vUserView` =  'public' OR `vUserView` = 'food' OR `vUserView` = 'courier' AND `tbl_feed`.`iUserID` = ('".$iUserID."') OR `vUserView` = 'pickedup' OR `vUserView` = 'friends' AND `tbl_feed`.`iUserID` = ('".$iUserID."') ORDER BY  `tbl_feed`.`iFeedID` DESC LIMIT 1"; 
		} else {
			$sql	=	"SELECT * FROM `tbl_feed` WHERE `vUserView` =  'public' OR `vUserView` = 'food' OR `vUserView` = 'courier' AND `tbl_feed`.`iUserID` = ('".$iUserID."') OR `vUserView` = 'pickedup' OR `vUserView` = 'friends' AND `tbl_feed`.`iUserID` = ('".$iUserID."') ORDER BY  `tbl_feed`.`iFeedID` DESC LIMIT 1";	
		}

		//print $sql; exit;
		
		$query = $this->db->query($sql);
        if ($query->num_rows > 0)
        {
            return $query->result_array();
        } else {
            return 0;
        }
    }

//    SELECT * FROM `tbl_feed` WHERE `iDriverID` = 519 NOT IN (`vUserView` = 'complete',`vUserView` = 'delete') OR SELECT * FROM `tbl_feed` WHERE `vUserView` NOT IN (`vUserView` = 'complete',`vUserView` = 'delete')
    
 function driverGetFeed($iUserID)
    {
		$this->load->model("connection_model");
		$get_friend = $this->connection_model->getMemberFriendByID($iUserID);

			$results	=	$this->getImplodeArrays($get_friend,'iUserID');	
			if($results) {
				$strWhere	= " AND (f.iUserID IN($results) OR f.iUserID = '".$iUserID."') ";
			}
            
	   $sql	=	"SELECT * FROM `tbl_feed` WHERE `iDriverID` = $iUserID NOT IN (`vUserView` = 'complete',`vUserView` = 'delete')";


		//print $sql; exit;
		
		$query = $this->db->query($sql);
        if ($query->num_rows > 0)
        {
            return $query->result_array();
        } else {
            
            $sql	=	"SELECT * FROM `tbl_feed` WHERE `vUserView` = 'public' OR `vUserView` = 'courier' OR `vUserView` = 'food'";
            
            $query = $this->db->query($sql);
        if ($query->num_rows > 0)
        {
            return $query->result_array();
        }else{
            return 0;
        }
        }
    }
    
    

    function getFeedListByUserID($iUserID)

    {

        $query = $this->db->query("SELECT  f.iFeedID,f.vFeedTitle,f.tFeedDescription,f.vFeedMapImage,f.vFeedItemImage,u.iUserID,u.vUsername,u.vEmail,u.vFirst,u.vLast,u.vImage,u.eLogin,u.dLoginDate,u.vFbID,f.fLat,f.fLong

                        FROM tbl_feed f

                                LEFT JOIN tbl_user u ON (u.iUserID = f.iUserID)

                                LEFT JOIN tbl_feed_tag_friend ff ON (ff.iFeedID = f.iFeedID)

                                WHERE f.iUserID = '$iUserID'

                                AND u.eStatus = 'Active'

                                AND u.eDelete = '0'

                                GROUP BY f.iFeedID

                                ORDER BY f.dCreatedDate DESC

                            ");

        if ($query->num_rows > 0)

        {

            return $query->result_array();

        }

        else

        {

            return 0;

        }

    }



    function getFeedTagFriendInfo($iFeedID)

    {

        $this->db->select('tu.iUserID,tu.vFirst,tu.vLast,tu.vImage');

        $this->db->from(TBL_FEED_TAG_FRIEND . ' ff');

        $this->db->join(TBL_USER . ' tu', 'ff.iFriendID = tu.iUserID', "LEFT");

        $this->db->where('ff.iFeedID', $iFeedID);

        $this->db->where('tu.eStatus', 'Active');

        $this->db->where('tu.eDelete', '0');

        $query = $this->db->get();



        if ($query->num_rows() > 0)

            return $query->result_array();

        else

            return 0;

    }

    function getFeedByID($iFeedID){

        $this->db->select('*');

        $this->db->from(TBL_FEED);

        $this->db->where('iFeedID', $iFeedID);

        $query = $this->db->get();

        if ($query->num_rows() > 0)

            return $query->row_array();

        else

            return 0;

    }

	

    function addLike($postData) {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_FEEDLIKES);

        $query = $this->db->insert(TBL_FEEDLIKES, $postArray);

        $total_rows = $this->db->affected_rows();

        if ($total_rows > 0)

            return $this->db->insert_id();

        else

            return 0;

    }

	
    function acceptedRide($postData, $feedAcceptID) {



        $postArray = $this->general_model->getDatabseFields($postData, tbl_feed);

        $query = $this->db->update(tbl_feed, $postArray, array('iFeedID' => feedAcceptID));



        if ($this->db->affected_rows() >= 0)

            return $feedAcceptID;

        else

            return 0;

    }
    
    

    function updateLike($postData, $feedLikeId) {



        $postArray = $this->general_model->getDatabseFields($postData, TBL_FEEDLIKES);

        $query = $this->db->update(TBL_FEEDLIKES, $postArray, array('Id' => $feedLikeId));



        if ($this->db->affected_rows() >= 0)

            return $feedLikeId;

        else

            return 0;

    }



    function addComment($postData) {

        $postArray = $this->general_model->getDatabseFields($postData, TBL_FEEDCOMMENTS);

        $query = $this->db->insert(TBL_FEEDCOMMENTS, $postArray);

        $total_rows = $this->db->affected_rows();

        if ($total_rows > 0)

            return $this->db->insert_id();

        else

            return 0;

    }

	

	function getCountFeedLike($iUserID,$iFeedID) {

        $this->db->select('Id');

        $this->db->from(TBL_FEEDLIKES);

        $this->db->where('UserId', $iUserID);

        $this->db->where('FeedId', $iFeedID);

        $query = $this->db->get();

        if ($query->num_rows() > 0)

            return $query->row_array();

        else

            return 0;

    }
    

	function doGetUserFeedLikeStatus($iFeedID,$iUserID) {

        $this->db->select('LikeStatus');

        $this->db->from(TBL_FEEDLIKES);

        $this->db->where('UserId', $iUserID);

        $this->db->where('FeedId', $iFeedID);

        $query = $this->db->get();

        if ($query->num_rows() > 0) {

			$strStatus	=	$query->row_array();  

			return $strStatus['LikeStatus'];

        } else {

            return 'No';

		}		

	}

	

	function doGetCommentByFeedId($iFeedID) {

        $query = $this->db->query("SELECT Comment FROM ".TBL_FEEDCOMMENTS." 

								WHERE FeedID = '$iFeedID'

                                ORDER BY PostedDate DESC

                            ");

        if ($query->num_rows > 0)

        {

            return $query->result_array();

        }

        else

        {

            return 0;

        }

	}

	function doGetCommentDetailsByFeedId($iFeedID) {
        $query = $this->db->query("SELECT Id as CommentId,UserId,Comment FROM ".TBL_FEEDCOMMENTS." 
								WHERE FeedID = '$iFeedID'
                                ORDER BY Id ASC
                            ");
        if ($query->num_rows > 0)
        {
            return $query->result_array();
        }
        else
        {
            return 0;
        }
	}

		

	function doGetLikeCountByFeedId($iFeedID) {

        $query = $this->db->query("SELECT COUNT(`LikeStatus`) AS LikeCount FROM ".TBL_FEEDLIKES." 

								WHERE LikeStatus = 'Like'

								AND FeedID = '$iFeedID'

                                ORDER BY AddedDate DESC

                            ");

        if ($query->num_rows > 0)

        {

			$strLikeCount	=	$query->row_array();

			return $strLikeCount['LikeCount'];

        }

        else

        {

            return 0;

        }

	}

	

	function doGetDislikeCountByFeedId($iFeedID) {

        $query = $this->db->query("SELECT COUNT(`LikeStatus`) AS DislikeCount FROM ".TBL_FEEDLIKES." 

								WHERE LikeStatus = 'Dislike'

								AND FeedID = '$iFeedID'

                                ORDER BY AddedDate DESC

                            ");

        if ($query->num_rows > 0)

        {

			$strDislikeCount	=	$query->row_array();

			return $strDislikeCount['DislikeCount'];

        }

        else

        {

            return 0;

        }

	}


	function addMessage($postData)
    {
		$postFeedArray = $this->general_model->getDatabseFields($postData, TBL_MESSAGE);
		$this->db->insert(TBL_MESSAGE, $postFeedArray);
		return $this->db->insert_id();
	}
	
	function getMessageLists($iUserID, $otherUserID)
    {
		$sql	=	"SELECT  * from ".TBL_MESSAGE." Where (iUserID = '$iUserID' AND otherUserId = '$otherUserID') OR (iUserID = '$otherUserID' AND otherUserId = '$iUserID') ";
		$query = $this->db->query($sql);
        if ($query->num_rows > 0)
			 return $query->result_array();
        else
            return 0;
    }	
	
	function doGetHighlighedPost($iUserID)
    {
        $query = $this->db->query("SELECT  f.iFeedID,f.type,f.vUserView,f.videofile,f.vFeedTitle,f.tFeedDescription,f.vFeedMapImage,f.vFeedItemImage,f.dCreatedDate,u.iUserID,u.vUsername,u.vEmail,u.vFirst,u.vLast,u.vImage,u.eLogin,u.dLoginDate,u.vFbID,f.fLat,f.fLong
                        FROM tbl_feed f
                                LEFT JOIN tbl_user u ON (u.iUserID = f.iUserID)
                                LEFT JOIN tbl_feed_tag_friend ff ON (ff.iFeedID = f.iFeedID)
								LEFT JOIN tbl_feedcomments fc ON (fc.FeedId = f.iFeedID AND fc.UserId = '" . $iUserID . "')
								LEFT JOIN tbl_feedlikes fl ON (fl.FeedId = f.iFeedID AND fc.UserId = '" . $iUserID . "')
                                WHERE f.iUserID IN (
                                                        SELECT  IF(iFriendID = '" . $iUserID . "',iSenderID,iFriendID) as connected_id
                                                        FROM tbl_friend tf 
                                                        LEFT JOIN tbl_user tu ON (IF(iFriendID = '" . $iUserID . "',iSenderID,iFriendID) = iUserID)
                                                            WHERE (iSenderID = '" . $iUserID . "' OR iFriendID = '" . $iUserID . "')
                                                                AND `tf`.`eStatus` =  '1'
                                                                AND `tu`.`eUserType` =  'User'
                                                                AND `tu`.`eStatus` =  'Active'
                                                                AND `tu`.`eDelete` =  '0'
                                                                GROUP BY `tu`.`iUserID`                       
                                                    ) OR f.iUserID=" . $iUserID . "
                                AND u.eStatus = 'Active'
                                AND u.eDelete = '0'
                                GROUP BY f.iFeedID
                                ORDER BY f.dCreatedDate DESC
                            ");
        if ($query->num_rows > 0)
        {
        	return $query->result_array();
        } else {
            return 0;
        }    
	}
	
	function doGetTodayLikeCountByFeedId($iFeedID) {
        $query = $this->db->query("SELECT COUNT(`LikeStatus`) AS LikeCount FROM ".TBL_FEEDLIKES." 
								WHERE LikeStatus = 'Like'
								AND FeedID = '$iFeedID' AND AddedDate >= (NOW() - INTERVAL 24 HOUR)
                                ORDER BY AddedDate DESC
                            ");
        if ($query->num_rows > 0)
        {
			$strLikeCount	=	$query->row_array();
			return $strLikeCount['LikeCount'];
        } else {
            return 0;
        }
	}
	
	function doGetTodayCommentDetailsByFeedId($iFeedID) {
        $query = $this->db->query("SELECT Id as CommentId,UserId,Comment FROM ".TBL_FEEDCOMMENTS." 
								WHERE FeedID = '$iFeedID' AND PostedDate >= (NOW() - INTERVAL 24 HOUR)
                                ORDER BY Id ASC
                            ");
        if ($query->num_rows > 0)
        {
        	return $query->result_array();
        } else {
        	return 0;
        }
	}



}



?>