<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of connection_model
 *
 * @author yudiz
 */
class Connection_model extends CI_Model {

    function __construct() {
        parent::__construct();
        $this->db->query("SET SQL_BIG_SELECTS=1");
    }

    /*     * ***********************************************
     *
     *   ADD CONNECTION
     *
     * ********************************************** */

    function addFriend($postData) {
        $postArray = $this->general_model->getDatabseFields($postData, TBL_FRIEND);

        $query = $this->db->insert(TBL_FRIEND, $postArray);

        if ($this->db->affected_rows() > 0)
            return $this->db->insert_id();
        else
            return '';
    }

    function editFriendshipStatus($iConnectionID, $iFriendID) {
        $query = $this->db->query("UPDATE " . TBL_FRIEND . " SET eStatus = '0' WHERE iConnectionID = '$iConnectionID' AND iFriendID = '$iFriendID'");

        if ($this->db->affected_rows() > 0)
            return $iConnectionID;
        else
            return '';
    }

    function rejectedFriend($iSenderID, $iFriendID) {
        $this->db->select("*", FALSE);
        $this->db->from(TBL_FRIEND);
        $this->db->where("iSenderID", $iSenderID);
        $this->db->where("iFriendID", $iFriendID);
        $this->db->where("eStatus", "2");

        $query = $this->db->get();

        if ($query->num_rows() > 0)
            return $query->row_array();
        else
            return '';
    }

    function getFriendRequestByID($iUserID) {

        $this->db->select("iConnectionID,iSenderID,iFriendID,iUserID,vFirst,vLast,fLat,fLong,IFNULL(vUsername,'') as vUsername,vEmail,vImage", FALSE);
        $this->db->from(TBL_FRIEND . ' tf');
        $this->db->join(TBL_USER . ' tu', "iUserID = iSenderID");
        $this->db->where("iFriendID", $iUserID);
        $this->db->where("tf.eStatus", "0");
        $this->db->where("tu.eUserType", 'User');
        $this->db->where("tu.eStatus", "Active");
        $this->db->where("tu.eDelete", "0");

        $query = $this->db->get();

        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return '';
    }

    function getMemberFriendByID($iUserID) {
        $query = $this->db->query("SELECT iConnectionID, IF(iFriendID = '" . $iUserID . "',iSenderID,iFriendID) as connected_id,
                            iUserID,iFriendID,iSenderID,                            
                            IFNULL(vUsername,'') as vUsername,
                            vFirst,
                            vLast,
                            vEmail,
                            vImage,
                            fLat,fLong,
                            tf.eStatus
                            FROM tbl_friend tf 
                            LEFT JOIN tbl_user tu ON (IF(`iFriendID` = '" . $iUserID . "',iSenderID,iFriendID) = iUserID)
                                WHERE (iSenderID = '" . $iUserID . "' OR iFriendID = '" . $iUserID . "')
                                    AND `tf`.`eStatus` =  '1'
                                    AND `tu`.`eUserType` =  'User'
                                    AND `tu`.`eStatus` =  'Active'
                                    AND `tu`.`eDelete` =  '0'
                                    GROUP BY `tu`.`iUserID`
                            ");
        /* $this->db->select("iConnectionID,IF(iFriendID = " . $iUserID . ",iSenderID,iFriendID) as connected_id,
          iUserID,iFriendID,iSenderID,
          IFNULL(vUsername,'') as vUsername,
          vFirst,
          vLast,
          vEmail,
          vImage,
          fLat,fLong,
          tf.eStatus", FALSE);
          $this->db->from(TBL_FRIEND . ' tf');
          $this->db->join(TBL_USER . ' tu', "IF(iFriendID = " . $iUserID . ",iSenderID,iFriendID) = iUserID", "", FALSE);
          $this->db->where("(iSenderID = " . $iUserID . " OR iFriendID = " . $iUserID . ")", NULL, FALSE);
          $this->db->group_by("tu.iUserID");
          $this->db->where("tf.eStatus", "1");
          $this->db->where("tu.eUserType", 'User');
          $this->db->where("tu.eStatus", "Active");
          $this->db->where("tu.eDelete", "0"); */


        //$query = $this->db->get();

        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return '';
    }
    function getPendingAndAcceptedFriendLists($iUserID) {
        $query = $this->db->query("SELECT iUserID
                            FROM tbl_friend tf 
                            LEFT JOIN tbl_user tu ON (IF(`iFriendID` = '" . $iUserID . "',iSenderID,iFriendID) = iUserID)
                                WHERE (iSenderID = '" . $iUserID . "' OR iFriendID = '" . $iUserID . "')
                                    AND `tf`.`eStatus` IN ('0','1')
                                    AND `tu`.`eUserType` =  'User'
                                    AND `tu`.`eStatus` =  'Active'
                                    AND `tu`.`eDelete` =  '0'
                                    GROUP BY `tu`.`iUserID`
                            ");
        
        /* $this->db->select("iConnectionID,IF(iFriendID = " . $iUserID . ",iSenderID,iFriendID) as connected_id,
          iUserID,iFriendID,iSenderID,
          IFNULL(vUsername,'') as vUsername,
          vFirst,
          vLast,
          vEmail,
          vImage,
          fLat,fLong,
          tf.eStatus", FALSE);
          $this->db->from(TBL_FRIEND . ' tf');
          $this->db->join(TBL_USER . ' tu', "IF(iFriendID = " . $iUserID . ",iSenderID,iFriendID) = iUserID", "", FALSE);
          $this->db->where("(iSenderID = " . $iUserID . " OR iFriendID = " . $iUserID . ")", NULL, FALSE);
          $this->db->group_by("tu.iUserID");
          $this->db->where("tf.eStatus", "1");
          $this->db->where("tu.eUserType", 'User');
          $this->db->where("tu.eStatus", "Active");
          $this->db->where("tu.eDelete", "0"); */


        //$query = $this->db->get();

        if ($query->num_rows() > 0)
            return $query->result_array();
        else
            return '';
    }

    function getFriendDetailByID($iConnectionID) {
        $this->db->select("iConnectionID,tf.eStatus,tu.iUserID,IFNULL(vUsername,'') as vUsername,fLat,fLong,vFirst,vLast,vEmail,vImage,tf.eStatus,", FALSE);
        $this->db->from(TBL_FRIEND . ' tf');
        $this->db->join(TBL_USER . ' tu', 'tu.iUserID = tf.iSenderID');
        $this->db->where('tf.iConnectionID', $iConnectionID);
        $this->db->where("tf.eStatus", "1");
        $this->db->where("tu.eUserType", 'User');
        $this->db->where("tu.eStatus", "Active");
        $this->db->where("tu.eDelete", "0");

        $query = $this->db->get();

        if ($query->num_rows() > 0)
            return $query->row_array();
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   ACCEPT REQUEST FUNCTION
     *
     * ********************************************** */

    function acceptRequest($iConnectionID, $iFriendID) {
        $query = $this->db->query("UPDATE " . TBL_FRIEND . " SET eStatus = '1' WHERE iConnectionID = $iConnectionID AND iFriendID = $iFriendID");

        if ($this->db->affected_rows() > 0)
            return $query;
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   UN FRIEND FUNCTION
     *
     * ********************************************** */

    function unFriendByID($iConnectionID, $iUserID) {

        $query = $this->db->query("UPDATE " . TBL_FRIEND . " SET eStatus = '3' WHERE iConnectionID = $iConnectionID AND (iSenderID = $iUserID OR iFriendID = $iUserID)");

        if ($this->db->affected_rows() > 0)
            return $query;
        else
            return '';
    }

    function rejectRequest($iConnectionID, $iFriendID) {


        $query = $this->db->query("UPDATE " . TBL_FRIEND . " SET eStatus = '2' WHERE iConnectionID = $iConnectionID AND iFriendID = $iFriendID");

        if ($this->db->affected_rows() > 0)
            return $query;
        else
            return '';
    }

    /*     * ***********************************************
     *
     *   ALL FRIEND
     *
     * ********************************************** */

    /*    function getAllFriendByID($iUserID) {
      //echo $iUserID;exit;
      $this->db->select("IF(iFriendID = " . $iUserID . ",iSenderID,iFriendID) as connected_id", FALSE);
      $this->db->from(TBL_FRIEND);
      $this->db->where("(iSenderID = " . $iUserID . " OR iFriendID = " . $iUserID . ")", NULL, FALSE);
      $this->db->where(TBL_FRIEND . ".eStatus", "1");

      $query = $this->db->get();

      if ($query->num_rows() > 0)
      return $query->result_array();
      else
      return '';
      }

      function getConnectionRequestByFriendID($iUserID) {
      $this->db->select("IF(iFriendID = " . $iUserID . ",iSenderID,iFriendID) as connected_id", FALSE);
      $this->db->from(TBL_FRIEND);
      $this->db->join(TBL_USER, "iUserID = iSenderID");
      $this->db->where("iFriendID", $iUserID);
      $this->db->where(TBL_FRIEND . ".eStatus", "0");
      $this->db->where(TBL_USER . ".eStatus", "Active");
      $this->db->where(TBL_USER . ".eDelete", "0");

      $query = $this->db->get();

      if ($query->num_rows() > 0)
      return $query->result_array();
      else
      return '';
      }

      function getConnectionRequestBySenderID($iUserID) {
      $this->db->select("IF(iFriendID = " . $iUserID . ",iSenderID,iFriendID) as connected_id", FALSE);
      $this->db->from(TBL_FRIEND);
      $this->db->join(TBL_USER, "iUserID = iSenderID");
      $this->db->where("iSenderID", $iUserID);
      $this->db->where(TBL_FRIEND . ".eStatus", "0");
      $this->db->where(TBL_USER . ".eStatus", "Active");
      $this->db->where(TBL_USER . ".eDelete", "0");

      $query = $this->db->get();

      if ($query->num_rows() > 0)
      return $query->result_array();
      else
      return '';
      }

      function getFriendRequestPendingByID($iUserID) {
      $this->db->select("IF(iFriendID = " . $iUserID . ",iSenderID,iFriendID) as connected_id", FALSE);
      $this->db->from(TBL_FRIEND);
      $this->db->where("(iSenderID = " . $iUserID . " OR iFriendID = " . $iUserID . ")", NULL, FALSE);
      $this->db->where(TBL_FRIEND . ".eStatus", "0");

      $query = $this->db->get();

      if ($query->num_rows() > 0)
      return $query->result_array();
      else
      return '';
      }

      function getPendingAcceptedFriendListsByID($iUserID) {
      $this->db->select("IF(iFriendID = " . $iUserID . ",iSenderID,iFriendID) as connected_id", FALSE);
      $this->db->from(TBL_FRIEND);
      $this->db->where("(iSenderID = " . $iUserID . " OR iFriendID = " . $iUserID . ")", NULL, FALSE);
      $this->db->where_in(TBL_FRIEND . ".eStatus", array("0", "1"));

      $query = $this->db->get();

      if ($query->num_rows() > 0)
      return $query->result_array();
      else
      return '';
      }
     */

    function getFriendData($iFriendID) {
        $this->db->select("iUserID,vFirst,vLast,vDeviceToken");
        $this->db->from(TBL_USER);
        $this->db->where('iUserID', $iFriendID);
        $query = $this->db->get();

        if ($query->num_rows > 0)
            return $query->row_array();
        else
            return NULL;
    }

    function getSenderDataByID($iSenderID) {
        $this->db->select("vFirst,vLast,iUserID,vDeviceToken");
        $this->db->from(TBL_USER . ' tu');
        $this->db->join(TBL_FRIEND . ' tf', 'tf.iSenderID = tu.iUserID');
        $this->db->where('tf.iSenderID', $iSenderID);
        $query = $this->db->get();

        if ($query->num_rows > 0)
            return $query->row_array();
        else
            return '';
    }

    function getSenderDataByConnectionID($iConnectionID) {
        $this->db->select("vFirst,vLast,iUserID,vDeviceToken");
        $this->db->from(TBL_USER . ' tu');
        $this->db->join(TBL_FRIEND . ' tf', 'tf.iSenderID = tu.iUserID');
        $this->db->where('tf.iConnectionID', $iConnectionID);
        $query = $this->db->get();

        if ($query->num_rows > 0)
            return $query->row_array();
        else
            return '';
    }

    function is_friend($iSenderID, $iFriendID) {
        $this->db->select("iUserID");
        $this->db->from(TBL_USER . ' tu');
        $this->db->join(TBL_FRIEND . ' tf', 'tf.iSenderID = tu.iUserID');
        $where = "((tf.iSenderID = $iSenderID && tf.iFriendID = $iFriendID) || (tf.iSenderID = $iFriendID && tf.iFriendID = $iSenderID)) && tf.eStatus = '1'";
        $this->db->where($where);
        $query = $this->db->get();

        if ($query->num_rows > 0)
            return 1;
        else
            return 0;
    }

    function getUserByFbID($vFbID) {
        $this->db->select("iUserID");
        $this->db->from(TBL_USER);
        $this->db->where("vFbID", $vFbID);
        $this->db->where("eDelete", "0");
        $query = $this->db->get();

        if ($query->num_rows > 0)
            return $query->row_array();
        else
            return "";
    }

	function doRemoveFriend($postData) {
		$strWhere	=	" WHERE (iSenderID = '".$postData['iUserID']."' AND iFriendID = '".$postData['frienduserid']."') || (iSenderID = '".$postData['frienduserid']."' && iFriendID ='".$postData['iUserID']."')";		
		$query = $this->db->query("UPDATE " . TBL_FRIEND . " SET eStatus = '3' $strWhere ");
        if ($this->db->affected_rows() > 0)
            return 1;
        else
            return '0';			
	}

}

?>
