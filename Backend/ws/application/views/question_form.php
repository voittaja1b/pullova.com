<?php 
$FORM_ATTRIBUTE = array(
    'name' => 'questionForm',
    'id' => 'questionForm',
    'method' => 'get',
);
 
echo form_open_multipart('question/post',$FORM_ATTRIBUTE) 
        ?>
<label for="vQuestion">Question <textarea name="vQuestion" id="vQuestion"></textarea></label>
    <br>
    <br>
    <label for="vQuestionImage">Question Image <input type="file" name="vQuestionImage" id="vQuestionImage"></label>
    <br>
    <br>
    <label for="vQuestionThumbImage">Question Thumb Image <input type="file" name="vQuestionThumbImage" id="vQuestionThumbImage"></label>
    <br>
    <br>
    <label for="vLocation">Location <input type="text" name="vLocation" id="vLocation"></label>
    <br>
    <br>
    <input type="submit" name="addBtn" id="addBtn">
<?php echo form_close() ?>