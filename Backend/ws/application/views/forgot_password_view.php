<?php
//$headerData['doctype'] = 'Transitional';
$headerData = $this->headerlib->data();

/* * ***************************************************
 * *			DEFINE FORM ATTRIBUTES
 * *************************************************** */

$FORM_ATTRIBUTE = array(
    'name' => 'forgotPasswordForm',
    'id' => 'forgotPasswordForm',
);

$EMAIL_ADDRESS = array(
    'name' => 'vEmail',
    'id' => 'vEmail',
    "placeholder" => $this->lang->line("ENTER_EMAIL_ADDRESS")
);

$FORM_BUTTON = array(
    'id' => 'forgotPasswordBtn',
    'value' => 'true',
    'type' => 'submit',
    'name' => 'forgotPasswordbtn',
    'content' => $this->lang->line("SUBMIT_BTN") . ' <i class=" icon-long-arrow-right"></i>',
    'class' => "btn login-btn"
);
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>

        <title><?php echo ADMIN_WEBSITE_TITLE . "-" . $title ?></title>
        <?= $headerData['meta_tags']; ?>
        <?= $headerData['stylesheets']; ?>


    </head>

    <!-- BEGIN BODY -->
    <body class="lock">
        <div class="lock-header">
            <!-- BEGIN LOGO -->
            <?php echo anchor("login", $this->lang->line("LOGO"), array("class" => "center", "id" => "logo")); ?>
            <!-- END LOGO -->
        </div>
        <div class="login-wrap">
        <h3><?php echo $this->general_model->getMessages(); ?> </h3>
            <div class="metro single-size red">
                <div class="locked">
                    <i class="icon-lock"></i>
                    <span><?php echo $this->lang->line("FORGOT_PASSWORD"); ?></span>
                </div>
            </div>

            <?php echo form_open('forgotpassword', $FORM_ATTRIBUTE); ?>
            <div class="metro tripal-size green">
                <div class="input-append lock-input">
                    <?php echo form_input($EMAIL_ADDRESS); ?>
                </div>
            </div>
            <div class="metro single-size terques login">
                <?php echo form_button($FORM_BUTTON); ?>
            </div>
            <div class="login-footer">
                <div class="forgot-hint pull-right">
                    <?php echo anchor("login", $this->lang->line("BACK_BTN")." ".$this->lang->line("LOGIN"), array("id" => "forget-password")); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </body>
    <!-- END BODY -->

</html>