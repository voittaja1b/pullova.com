<?php
$headerData = $this->headerlib->data();

$FORM_ATTRIBUTE = array(
    'name' => 'loginForm',
    'id' => 'loginForm',
    'class' => 'form-horizontal'
);
$EMAIL_ADDRESS = array(
    'name' => 'vEmail',
    'id' => 'vEmail',
    "class" => 'input-large span10',
    "autofocus" => '',
    "placeholder" => $this->lang->line("ENTER_EMAIL_ADDRESS"),
);

$PASSWORD = array(
    'name' => 'vPassword',
    'id' => 'vPassword',
    'class' => 'input-large span10',
    "placeholder" => $this->lang->line("ENTER_PASSWORD"),
);

$REMEMBER_ME = array("id" => "keepLoged",
    "value" => "TRUE",
    "class" => "styled",
    "name" => "chkRemember"
);

$FORM_BUTTON = array(
    'id' => 'loginBtn',
    'value' => 'true',
    'type' => 'submit',
    'name' => 'loginbtn',
    'content' => $this->lang->line("LOGIN"),
    'class' => "btn btn-primary"
);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <title><?php echo $title ?></title>
        <?php echo $headerData['meta_tags']; ?>
        <?php //echo $headerData['stylesheets']; ?>
        <link id="bs-css" href="<?php echo ADMIN_CSS_URL ?>bootstrap-classic.css" rel="stylesheet">
        <style type="text/css">
            body {
                padding-bottom: 40px;
            }
            .sidebar-nav {
                padding: 9px 0;
            }
        </style>
        <link href="<?php echo ADMIN_CSS_URL ?>bootstrap-responsive.css" rel="stylesheet">
        <link href="<?php echo ADMIN_CSS_URL ?>charisma-app.css" rel="stylesheet">
        <link href="<?php echo ADMIN_CSS_URL ?>jquery-ui-1.8.21.custom.css" rel="stylesheet">
        <link href='<?php echo ADMIN_CSS_URL ?>fullcalendar.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>fullcalendar.print.css' rel='stylesheet'  media='print'>
        <link href='<?php echo ADMIN_CSS_URL ?>chosen.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>uniform.default.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>colorbox.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>jquery.cleditor.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>jquery.noty.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>noty_theme_default.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>elfinder.min.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>elfinder.theme.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>jquery.iphone.toggle.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>opa-icons.css' rel='stylesheet'>
        <link href='<?php echo ADMIN_CSS_URL ?>uploadify.css' rel='stylesheet'>
    </head>

    <!-- BEGIN BODY -->
    <body class="lock">
        <div class="container-fluid">
            <div class="row-fluid">

                <div class="row-fluid">
                    <div class="span12 center login-header">
                        <h2><?php echo ADMIN_WEBSITE_TITLE ?></h2>
                    </div><!--/span-->
                </div><!--/row-->

                <div class="row-fluid">
                    <div class="well span5 center login-box">
                        <?php
                            if($this->session->userdata('ERROR')){
                                echo $this->general_model->getMessages();
                            }else{
                        ?>
                        <div class="alert alert-info">
                            Please login with your Username and Password.
                        </div>
                            <?php } 
                            echo form_open('login', $FORM_ATTRIBUTE) ?>
                        <!--                        <form class="form-horizontal" action="index.html" method="post">-->
                        <fieldset>
                            <div class="input-prepend" title="Username" data-rel="tooltip">
                                <span class="add-on"><i class="icon-envelope"></i></span>
                                <?php echo form_input($EMAIL_ADDRESS) ?>
<!--                                    <input autofocus class="input-large span10" name="username" id="username" type="text" value="admin" />-->
                            </div>
                            <div class="clearfix"></div>

                            <div class="input-prepend" title="Password" data-rel="tooltip">
                                <span class="add-on"><i class="icon-lock"></i></span>
                                <?php echo form_password($PASSWORD) ?>
<!--                                    <input class="input-large span10" name="password" id="password" type="password" value="admin123456" />-->
                            </div>
                            <div class="clearfix"></div>

                            <div class="input-prepend" style="width: 80%">
                                <div style="float: left">
                                <?php echo form_label(form_checkbox($REMEMBER_ME) . " " . $this->lang->line('REMEMBER_ME'), 'remember', 'class="remember"'); ?>
                                </div>
                                <div style="float: right">
                                <?php echo anchor("forgotpassword", $this->lang->line("FORGOT_PASSWORD"), array("id" => "forget-password")); ?>
                                </div>
<!--                                    <label class="remember" for="remember"><input type="checkbox" id="remember" />Remember me</label>-->
                            </div>
                            <div class="clearfix"></div>

                            <p class="center span5">
                                <?php echo form_button($FORM_BUTTON); ?>
<!--                                <button type="submit" class="btn btn-primary">Login</button>-->
                            </p>
                        </fieldset>
                        </form>
                    </div><!--/span-->
                </div><!--/row-->
            </div><!--/fluid-row-->

        </div><!--/.fluid-container-->
        <script src="<?php echo ADMIN_JS_URL ?>jquery-1.7.2.min.js"></script>
        <!-- jQuery UI -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery-ui-1.8.21.custom.min.js"></script>
        <!-- transition / effect library -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-transition.js"></script>
        <!-- alert enhancer library -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-alert.js"></script>
        <!-- modal / dialog library -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-modal.js"></script>
        <!-- custom dropdown library -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-dropdown.js"></script>
        <!-- scrolspy library -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-scrollspy.js"></script>
        <!-- library for creating tabs -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-tab.js"></script>
        <!-- library for advanced tooltip -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-tooltip.js"></script>
        <!-- popover effect library -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-popover.js"></script>
        <!-- button enhancer library -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-button.js"></script>
        <!-- accordion library (optional, not used in demo) -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-collapse.js"></script>
        <!-- carousel slideshow library (optional, not used in demo) -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-carousel.js"></script>
        <!-- autocomplete library -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-typeahead.js"></script>
        <!-- tour library -->
        <script src="<?php echo ADMIN_JS_URL ?>bootstrap-tour.js"></script>
        <!-- library for cookie management -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.cookie.js"></script>
        <!-- calander plugin -->
        <script src='<?php echo ADMIN_JS_URL ?>fullcalendar.min.js'></script>
        <!-- data table plugin -->
        <script src='<?php echo ADMIN_JS_URL ?>jquery.dataTables.min.js'></script>

        <!-- chart libraries start -->
        <script src="<?php echo ADMIN_JS_URL ?>excanvas.js"></script>
        <script src="<?php echo ADMIN_JS_URL ?>jquery.flot.min.js"></script>
        <script src="<?php echo ADMIN_JS_URL ?>jquery.flot.pie.min.js"></script>
        <script src="<?php echo ADMIN_JS_URL ?>jquery.flot.stack.js"></script>
        <script src="<?php echo ADMIN_JS_URL ?>jquery.flot.resize.min.js"></script>
        <!-- chart libraries end -->

        <!-- select or dropdown enhancer -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.chosen.min.js"></script>
        <!-- checkbox, radio, and file input styler -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.uniform.min.js"></script>
        <!-- plugin for gallery image view -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.colorbox.min.js"></script>
        <!-- rich text editor library -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.cleditor.min.js"></script>
        <!-- notification plugin -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.noty.js"></script>
        <!-- file manager library -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.elfinder.min.js"></script>
        <!-- star rating plugin -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.raty.min.js"></script>
        <!-- for iOS style toggle switch -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.iphone.toggle.js"></script>
        <!-- autogrowing textarea plugin -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.autogrow-textarea.js"></script>
        <!-- multiple file upload plugin -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.uploadify-3.1.min.js"></script>
        <!-- history.js for cross-browser state change on ajax -->
        <script src="<?php echo ADMIN_JS_URL ?>jquery.history.js"></script>
        <!-- application script for Charisma demo -->
        <script src="<?php echo ADMIN_JS_URL ?>charisma.js"></script>
        <script src="<?php echo ADMIN_JS_URL ?>jquery-validation-1.12.0/dist/jquery.validate.min.js"></script>
        <script type="text/javascript">
            $(function() {
                $('#loginForm').validate({
                    rules: {
                        vEmail: {
                            email : true,
                            required: true
                        },
                        vPassword: {
                            required: true
                        }
                    },
                    messages: {
                        vEmail: {
                            required: 'Email field can not be empty'
                        },
                        vPassword: {
                            required: 'Password field can not be empty'
                        }
                    }
                });
            });
        </script>
    </body>

    <!-- END BODY -->

</html>