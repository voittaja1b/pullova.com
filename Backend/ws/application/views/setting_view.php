<?php
$headerData = $this->headerlib->data();

/* * ***************************************************
 * *			DEFINE FORM ATTRIBUTES
 * *************************************************** */

if ($this->session->userdata('SETTINGS_DETAIL') && $this->session->userdata('SETTINGS_DETAIL') != "") {
    extract($this->session->userdata('SETTINGS_DETAIL'));
    $this->session->unset_userdata('SETTINGS_DETAIL');
} else {
    if (isset($getSetting) && $getSetting != '')
        extract($getSetting);
}

$FORM_ATTR = array(
    "name" => "settingForm",
    "id" => "settingForm",
    "class" => "cmxform form-horizontal",
    "method" => "post"
);

$SETTING_ID = array(
    'iSettingID' => (isset($iSettingID) && $iSettingID != '') ? $iSettingID : '',
);
$WEBSITE_TITLE = array(
    'name' => "WEBSITE_TITLE",
    'id' => "WEBSITE_TITLE",
    "class" => "span6",
    "required" => TRUE,
    "title" => $this->lang->line("TEXT_WEBSITE_TITLE_TIP_SETTING_PAGE"),
    'value' => (isset($WEBSITE_TITLE) && $WEBSITE_TITLE != '') ? $WEBSITE_TITLE : ''
);

$ADMIN_WEBSITE_TITLE = array(
    'name' => "ADMIN_WEBSITE_TITLE",
    'id' => "ADMIN_WEBSITE_TITLE",
    "class" => "span6",
    "required" => TRUE,
    "title" => $this->lang->line("TEXT_ADMIN_WEBSITE_TITLE_TIP_SETTING_PAGE"),
    'value' => (isset($ADMIN_WEBSITE_TITLE) && $ADMIN_WEBSITE_TITLE != '') ? $ADMIN_WEBSITE_TITLE : ''
);
$INFO_EMAIL = array(
    'name' => "INFO_EMAIL",
    'id' => "INFO_EMAIL",
    "class" => "span6",
    "required" => TRUE,
    "title" => $this->lang->line("TEXT_INFO_EMAIL_TIP_SETTING_PAGE"),
    'value' => (isset($INFO_EMAIL) && $INFO_EMAIL != '') ? $INFO_EMAIL : ''
);
$ADMIN_EMAIl = array(
    'name' => "ADMIN_EMAIL",
    'id' => "ADMIN_EMAIL",
    "class" => "span6",
    "required" => TRUE,
    "title" => $this->lang->line("TEXT_ADMIN_EMAIl_TIP_SETTING_PAGE"),
    'value' => (isset($ADMIN_EMAIL) && $ADMIN_EMAIL != '') ? $ADMIN_EMAIL : ''
);
$PHONE_NO = array(
    'name' => "PHONE",
    'id' => "PHONE",
    "class" => "span5",
    "placeholder" => "Enter phone number",
    "value" => (isset($PHONE) && $PHONE != '') ? $PHONE : ''
);

$WEBSITE_URL_FIELD = array(
    'name' => "WEBSITE_URL",
    'id' => "WEBSITE_URL",
    "class" => "span5",
    "placeholder" => "Enter website url",
    "value" => (isset($WEBSITE_URL) && $WEBSITE_URL != '') ? $WEBSITE_URL : ''
);


$FOOTER_SEO_TEXT = array(
    'name' => "FOOTER_SEO_TEXT",
    'id' => "FOOTER_SEO_TEXT",
    "class" => "span6",
    "title" => $this->lang->line("TEXT_FOOTER_SEO_TEXT_TIP_SETTING_PAGE"),
    'value' => (isset($FOOTER_SEO_TEXT) && $FOOTER_SEO_TEXT != '') ? $FOOTER_SEO_TEXT : ''
);

$FACEBOOK_URL = array(
    'name' => "FACEBOOK_URL",
    'id' => "FACEBOOK_URL",
    "class" => "span6",
    "title" => $this->lang->line("LABEL_FACEBOOK_URL"),
    'value' => (isset($FACEBOOK_URL) && $FACEBOOK_URL != '') ? $FACEBOOK_URL : ''
);
$TWITTER_URL = array(
    'name' => "TWITTER_URL",
    'id' => "TWITTER_URL",
    "class" => "span6",
    "title" => $this->lang->line("LABEL_TWITTER_URL"),
    'value' => (isset($TWITTER_URL) && $TWITTER_URL != '') ? $TWITTER_URL : ''
);
$GOOGLE_PLUS_URL = array(
    'name' => "GOOGLE_PLUS_URL",
    'id' => "GOOGLE_PLUS_URL",
    "class" => "span6",
    "title" => $this->lang->line("LABEL_GOOGLE_URL"),
    'value' => (isset($GOOGLE_PLUS_URL) && $GOOGLE_PLUS_URL != '') ? $GOOGLE_PLUS_URL : ''
);
$GOOGLE_ANALITICS_ID = array(
    'name' => "GOOGLE_ANALITICS_ID",
    'id' => "GOOGLE_ANALITICS_ID",
    "title" => $this->lang->line("TEXT_GOOGLE_ANALITICS_ID_SETTING_PAGE"),
    'value' => (isset($GOOGLE_ANALITICS_ID) && $GOOGLE_ANALITICS_ID != '') ? $GOOGLE_ANALITICS_ID : ''
);
$BACKGROUNG_IMAGE = array(
    'id' => "SCREEN_BACKGROUND_IMAGE",
    'name' => "SCREEN_BACKGROUND_IMAGE",
    "value" => (isset($SCREEN_BACKGROUND_IMAGE) && $SCREEN_BACKGROUND_IMAGE != '') ? $SCREEN_BACKGROUND_IMAGE : '',
    "class"=>"default"
);

$OFFERED_TEXT = array(
    'name' => "SCREEN_TEXT",
    'id' => "SCREEN_TEXT",
    "class" => "span6",
    "required" => TRUE,
    "placeholder" => "Enter Offer Text",
    "value" => (isset($SCREEN_TEXT) && $SCREEN_TEXT != '') ? $SCREEN_TEXT : ''
);
$FORM_BUTTON = array(
    'id' => 'settingBtn',
    'value' => 'true',
    'type' => 'submit',
    'name' => 'settingbtn',
    'content' => $this->lang->line("SUBMIT_BTN"),
    'class' => "btn btn-success"
);
$RESET_BUTTON = array(
    "name" => "resetBtn",
    "class" => "btn",
    "type" => "reset",
    "content" => $this->lang->line("RESET_BTN"),
    "style" => "margin-left:10px"
);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>

        <title><?php echo ADMIN_WEBSITE_TITLE . "-" . $title ?></title>
        <?= $headerData['meta_tags']; ?>
        <?= $headerData['stylesheets']; ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="fixed-top">
        <!-- BEGIN HEADER -->
        <?php $this->load->view("include/header_view"); ?>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div id="container" class="row-fluid">
            <!-- BEGIN SIDEBAR -->
            <?php $this->load->view("include/sidebar_view"); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div id="main-content">
                <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                <?php echo $title; ?>
                            </h3>
                            <ul class="breadcrumb">
                                <li>
                                    <?php echo anchor("dashboard", $this->lang->line("LABEL_DASHBOARD")); ?>
                                    <span class="divider">/</span>
                                </li>
                                <li class="active">
                                    <?php echo $title; ?>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="widget blue">
                                <div class="widget-title">
                                    <h4><i class="icon-reorder"></i> <?php echo $title; ?></h4>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="widget-body form">
                                    <!-- BEGIN FORM-->

                                    <?php
                                    echo $this->general_model->getMessages();
                                    echo form_open_multipart("setting", $FORM_ATTR);
                                    echo form_hidden($SETTING_ID);
                                    ?>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_WEBSITE_TITLE_SETTING_PAGE") . " (" . $this->lang->line("REQUIRED") . ")", $this->lang->line("LABEL_WEBSITE_TITLE_SETTING_PAGE"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($WEBSITE_TITLE);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_ADMIN_WEBSITE_TITLE_SETTING_PAGE") . " (" . $this->lang->line("REQUIRED") . ")", $this->lang->line("LABEL_ADMIN_WEBSITE_TITLE_SETTING_PAGE"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($ADMIN_WEBSITE_TITLE);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("PHONE_NUMBER"), $this->lang->line("PHONE_NUMBER"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($PHONE_NO);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_INFO_EMAIL_SETTING_PAGE") . " (" . $this->lang->line("REQUIRED") . ")", $this->lang->line("LABEL_INFO_EMAIL_SETTING_PAGE"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($INFO_EMAIL);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_ADMIN_EMAIl_SETTING_PAGE") . " (" . $this->lang->line("REQUIRED") . ")", $this->lang->line("LABEL_ADMIN_EMAIl_SETTING_PAGE"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($ADMIN_EMAIl);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_FOOTER_SEO_TEXT_SETTING_PAGE"), $this->lang->line("LABEL_FOOTER_SEO_TEXT_SETTING_PAGE"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_textarea($FOOTER_SEO_TEXT);
                                            ?>
                                        </div>
                                    </div>

                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LBL_WEBSITE_URL") . " (" . $this->lang->line("REQUIRED") . ")", $this->lang->line("LBL_WEBSITE_URL"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($WEBSITE_URL_FIELD);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_FACEBOOK_URL"), $this->lang->line("LABEL_FACEBOOK_URL"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($FACEBOOK_URL);
                                            ?>
                                        </div>
                                    </div>

                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_TWITTER_URL"), $this->lang->line("LABEL_TWITTER_URL"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($TWITTER_URL);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_GOOGLE_URL"), $this->lang->line("LABEL_GOOGLE_URL"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($GOOGLE_PLUS_URL);
                                            ?>
                                        </div>
                                    </div>
                                    
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_PHONE_NO"), $this->lang->line("LABEL_PHONE_NO"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($PHONE_NO);
                                            ?>
                                        </div>
                                    </div>
                                    

                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_GOOGLE_ANALITICS_ID_SETTING_PAGE"), $this->lang->line("LABEL_GOOGLE_ANALITICS_ID_SETTING_PAGE"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($GOOGLE_ANALITICS_ID);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <label class="control-label">Screen Background Image</label>
                                        <div class="controls">
                                            <div data-provides="fileupload" class="fileupload fileupload-new">
                                                <div style="width: 200px; height: 150px;" class="fileupload-new thumbnail">
                                                    
                                                    <?php
                                                        $image = (isset($SCREEN_BACKGROUND_IMAGE) && $SCREEN_BACKGROUND_IMAGE!='' ? BACKGROUND_IMAGE_URL.$SCREEN_BACKGROUND_IMAGE : 'http://www.placehold.it/200x150/EFEFEF/AAAAAA&amp;text=no+image');
                                                        echo img($image) 
                                                    ?>
                                                </div>
                                                <div style="max-width: 200px; max-height: 150px; line-height: 20px;" class="fileupload-preview fileupload-exists thumbnail"></div>
                                                <div style="margin-left: 80px">
                                                    <span class="btn btn-file"><span class="fileupload-new">Select image</span>
                                                        <span class="fileupload-exists">Change</span>
                                                        <?php echo form_upload($BACKGROUNG_IMAGE); ?></span>
                                                        <?php echo anchor('#', 'Remove', 'data-dismiss="fileupload" class="btn fileupload-exists"') ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_OFFERE_TEXT") . " (" . $this->lang->line("REQUIRED") . ")", $this->lang->line("FIRST_NAME"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_input($OFFERED_TEXT);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <?php
                                        echo form_button($FORM_BUTTON);
                                        echo form_button($RESET_BUTTON);
                                        ?>
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!-- END FORM-->
                                </div>
                            </div>
                            <!-- END VALIDATION STATES-->
                        </div>
                    </div>
                    <!-- END PAGE CONTENT-->
                </div>
                <!-- END PAGE CONTAINER-->
            </div>
            <!-- END PAGE -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <?php $this->load->view("include/footer_view"); ?>
        <!-- END FOOTER -->
        <!-- BEGIN JAVASCRIPTS -->
        <?php echo $headerData['javascript']; ?>
        <script type="text/javascript">
            $(function(){
                $('#settingForm').validate({
                    focusInvalid: true,
                    rules: {
                        WEBSITE_URL: {
                            required:true,
                            url: true
                        },
                        PHONE:{
                            number:true
                        }
                    }
                });
            });
        </script>
        <!-- END JAVASCRIPTS -->

    </body>
    <!-- END BODY -->

</html>