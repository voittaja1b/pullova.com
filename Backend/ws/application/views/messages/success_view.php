<?php
if ($this->session->userdata('SUCCESS')) {
    $success = $this->session->userdata('SUCCESS');
    $this->session->unset_userdata('SUCCESS');
    foreach ($success as $key => $val) {
        echo "<div class='alert alert-success'>
                <button class='close' data-dismiss='alert'>&times;</button>
                " . $val . "
            </div>";
    }
}