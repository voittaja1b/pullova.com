<?php
if ($this->session->userdata('ERROR')) {
    $errors = $this->session->userdata('ERROR');
    $this->session->unset_userdata('ERROR');
    foreach ($errors as $key => $val) {
        echo "<div class='alert alert-error'>
                <button class='close' data-dismiss='alert'>&times;</button>
                    <span>" . $val . "</span>
            </div>";
    }
}