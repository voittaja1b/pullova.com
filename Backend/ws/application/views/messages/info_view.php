<?php
if ($this->session->userdata('INFO')) {
    $info = $this->session->userdata('INFO');
    $this->session->unset_userdata('INFO');
    foreach ($info as $key => $val) {
        echo "<div class='alert alert-info'>
                <button class='close' data-dismiss='alert'>&times;</button>
                    <span>" . $val . "</span>
            </div>";
    }
}