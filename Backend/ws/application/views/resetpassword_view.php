<?php
//$headerData['doctype'] = 'Transitional';
$headerData = $this->headerlib->data();

/* * ***************************************************
 * *			DEFINE FORM ATTRIBUTES
 * *************************************************** */

$FORM_ATTRIBUTE = array(
    'name' => 'resetPasswordForm',
    'id' => 'resetPasswordForm'
);

$NEW_PASSWORD = array(
    'name' => 'vPassword',
    'id' => 'vPassword',
    "placeholder" => $this->lang->line("ENTER_NEW_PASSWORD")
);
$CONFIRM_PASSWORD = array(
    'name' => 'vConfrimPassword',
    'id' => 'vConfrimPassword',
    "placeholder" => $this->lang->line("ENTER_CONFIRM_PASSWORD")
);

$FORM_BUTTON = array(
    'id' => 'resetPasswordBtn',
    'value' => 'true',
    'type' => 'submit',
    'name' => 'resetPasswordBtn',
    'content' => $this->lang->line("SUBMIT_BTN") . ' <i class=" icon-long-arrow-right"></i>',
    'class' => "btn login-btn"
);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>

        <title><?php echo ADMIN_WEBSITE_TITLE . "-" . $title ?></title>
        <?= $headerData['meta_tags']; ?>
        <?= $headerData['stylesheets']; ?>


    </head>

    <!-- BEGIN BODY -->
    <body class="lock">
        <div class="lock-header">
            <!-- BEGIN LOGO -->
            <?php echo anchor("login", $this->lang->line("LOGO"), array("class" => "center", "id" => "logo")); ?>
            <!-- END LOGO -->
        </div>
        <div class="login-wrap">
            <?php echo $this->general_model->getMessages(); ?>
            <div class="metro single-size red">
                <div class="locked">
                    <i class="icon-lock"></i>
                    <span><?php echo $this->lang->line("RESET_PASSWORD"); ?></span>
                </div>
            </div>

            <?php echo form_open("forgotpassword/resetpassword/$vEmail", $FORM_ATTRIBUTE); ?>
            <div class="metro double-size green">
                <div class="input-append lock-input">
                    <?php echo form_password($NEW_PASSWORD); ?>
                </div>
            </div>
            <div class="metro double-size yellow">
                <div class="input-append lock-input">
                    <?php echo form_password($CONFIRM_PASSWORD); ?>
                </div>
            </div>
            <div class="metro single-size terques login">
                <?php echo form_button($FORM_BUTTON); ?>
            </div>
            <div class="login-footer">
                <div class="forgot-hint pull-right">
                    <?php echo anchor("login", $this->lang->line("BACK_BTN") . " " . $this->lang->line("LOGIN"), array("id" => "forget-password")); ?>
                </div>
            </div>
            <?php echo form_close(); ?>
        </div>
    </body>
    <!-- END BODY -->

</html>
