<?php
$headerData = $this->headerlib->data();

/* * ***************************************************
 * *			DEFINE FORM ATTRIBUTES
 * *************************************************** */


if (isset($getDetail) && $getDetail != '')
    extract($getDetail);

$FORM_ATTR = array(
    "name" => "changePassForm",
    "id" => "changePassForm",
    "class" => "cmxform form-horizontal",
    "method" => "post"
);

$OLD_PASSWORD = array(
    'name' => "vOldPassword",
    'id' => "vOldPassword",
    "class" => "span6",
    "required" => TRUE,
    "placeholder" => "Enter your old password",
    "title" => $this->lang->line("TEXT_OLD_PASSWORD_TIP_PASSWORD_PAGE"),
);

$NEW_PASSWORD = array(
    'name' => "vNewPassword",
    'id' => "vNewPassword",
    "class" => "span6",
    "required" => TRUE,
    "placeholder" => "Enter your new password",
    "title" => $this->lang->line("TEXT_NEW_PASSWORD_TIP_PASSWORD_PAGE"),
);
$CONFIRM_PASSWORD = array(
    'name' => "vConfirmPassword",
    'id' => "vConfirmPassword",
    "class" => "span6",
    "required" => TRUE,
    "placeholder" => "Enter your new password",
    "title" => $this->lang->line("TEXT_CONFIRM_PASSWORD_TIP_PASSWORD_PAGE"),
);
$FORM_BUTTON = array(
    'id' => 'changePassBtn',
    'value' => 'true',
    'type' => 'submit',
    'name' => 'changePassBtn',
    'content' => $this->lang->line("SUBMIT_BTN"),
    'class' => "btn btn-success"
);
$RESET_BUTTON = array(
    "name" => "resetBtn",
    "class" => "btn",
    "type" => "reset",
    "content" => $this->lang->line("RESET_BTN"),
    "style" => "margin-left:10px"
);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>

        <title><?php echo ADMIN_WEBSITE_TITLE . "-" . $title ?></title>
        <?= $headerData['meta_tags']; ?>
        <?= $headerData['stylesheets']; ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body class="fixed-top">
        <!-- BEGIN HEADER -->
        <?php $this->load->view("include/header_view"); ?>
        <!-- END HEADER -->
        <!-- BEGIN CONTAINER -->
        <div id="container" class="row-fluid">
            <!-- BEGIN SIDEBAR -->
            <?php $this->load->view("include/sidebar_view"); ?>
            <!-- END SIDEBAR -->
            <!-- BEGIN PAGE -->
            <div id="main-content">
                <!-- BEGIN PAGE CONTAINER-->
                <div class="container-fluid">
                    <!-- BEGIN PAGE HEADER-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN PAGE TITLE & BREADCRUMB-->
                            <h3 class="page-title">
                                <?php echo $title; ?>
                            </h3>
                            <ul class="breadcrumb">
                                <li>
                                    <?php echo anchor("dashboard",$this->lang->line("LABEL_DASHBOARD"));?>
                                    <span class="divider">/</span>
                                </li>
                                <li class="active">
                                    <?php echo $title; ?>
                                </li>
                            </ul>
                            <!-- END PAGE TITLE & BREADCRUMB-->
                        </div>
                    </div>
                    <!-- END PAGE HEADER-->

                    <!-- BEGIN PAGE CONTENT-->
                    <div class="row-fluid">
                        <div class="span12">
                            <!-- BEGIN VALIDATION STATES-->
                            <div class="widget blue">
                                <div class="widget-title">
                                    <h4><i class="icon-reorder"></i> <?php echo $title; ?></h4>
                                    <div class="tools">
                                        <a href="javascript:;" class="collapse"></a>
                                        <a href="#portlet-config" data-toggle="modal" class="config"></a>
                                        <a href="javascript:;" class="reload"></a>
                                        <a href="javascript:;" class="remove"></a>
                                    </div>
                                </div>
                                <div class="widget-body form">
                                    <!-- BEGIN FORM-->

                                    <?php
                                    echo $this->general_model->getMessages();
                                    echo form_open("changepassword",$FORM_ATTR); 
                                    ?>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_OLD_PASSWORD") . " (" . $this->lang->line("REQUIRED") . ")", $this->lang->line("LABEL_OLD_PASSWORD"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_password($OLD_PASSWORD);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_NEW_PASSWORD") . " (" . $this->lang->line("REQUIRED") . ")", $this->lang->line("LABEL_NEW_PASSWORD"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_password($NEW_PASSWORD);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="control-group ">
                                        <?php echo form_label($this->lang->line("LABEL_CONFIRM_PASSWORD") . " (" . $this->lang->line("REQUIRED") . ")", $this->lang->line("LABEL_CONFIRM_PASSWORD"), array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php
                                            echo form_password($CONFIRM_PASSWORD);
                                            ?>
                                        </div>
                                    </div>
                                    <div class="form-actions">
                                        <?php
                                        echo form_button($FORM_BUTTON);
                                        echo form_button($RESET_BUTTON);
                                        ?>
                                    </div>
                                    <?php echo form_close(); ?>
                                    <!-- END FORM-->
                                </div>
                            </div>
                            <!-- END VALIDATION STATES-->
                        </div>
                    </div>

                    <!-- END PAGE CONTENT-->

                </div>
                <!-- END PAGE CONTAINER-->
            </div>
            <!-- END PAGE -->
        </div>
        <!-- END CONTAINER -->

        <!-- BEGIN FOOTER -->
        <?php $this->load->view("include/footer_view"); ?>
        <!-- END FOOTER -->


        <!-- BEGIN JAVASCRIPTS -->
        <?php echo $headerData['javascript']; ?>
        <!-- END JAVASCRIPTS -->

    </body>
    <!-- END BODY -->

</html>