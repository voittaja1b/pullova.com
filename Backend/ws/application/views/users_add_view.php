<?php
$headerData = $this->headerlib->data();

/* * ***************************************************
 * *			DEFINE FORM ATTRIBUTES
 * *************************************************** */

if ($this->session->userdata('USER_DETAIL') && $this->session->userdata('USER_DETAIL') != "") {
    extract($this->session->userdata('USER_DETAIL'));
    $this->session->unset_userdata('USER_DETAIL');
} else if (isset($getUsersData) && $getUsersData != '')
    extract($getUsersData);


$FORM_ATTR = array(
    "name" => "users" . $ACTION_LABEL . "Form",
    "id" => "users" . $ACTION_LABEL . "Form",
    "class" => "cmxform form-horizontal userform",
    "method" => "post"
);
$USER_ID = array(
    "iUserID" => (isset($iUserID) && $iUserID != '') ? $iUserID : ''
);

$EMAIL_ADDRESS = array(
    'name' => "vEmail",
    'id' => "vEmail",
    "class" => "span6",
    "placeholder" => "Enter Email Address",
    "value" => (isset($vEmail) && $vEmail != '') ? $vEmail : ''
);

$PASSWORD = array(
    'name' => "vPassword",
    'id' => "vPassword",
    "class" => "span6",
    "placeholder" => "Enter Password"
);

$FIRSTNAME = array(
    'name' => "vFirst",
    'id' => "vFirst",
    "class" => "span6",
    "placeholder" => "Enter Firstname",
    "value" => (isset($vFirst) && $vFirst != '') ? $vFirst : ''
);
$LASTNAME = array(
    'name' => "vLast",
    'id' => "vLast",
    "class" => "span6",
    "placeholder" => "Enter Last name",
    "value" => (isset($vLast) && $vLast != '') ? $vLast : ''
);

$USERNAME = array(
    'name' => "vUsername",
    'id' => "vUsername",
    "class" => "span6",
    "placeholder" => "Enter User name",
    "value" => (isset($vUsername) && $vUsername != '') ? $vUsername : ''
);

$FORM_BUTTON = array(
    'id' => "users" . $ACTION_LABEL . "Btn",
    'value' => 'true',
    'type' => 'submit',
    'name' => "users" . $ACTION_LABEL . "Btn",
    'content' => $this->lang->line("ADD"),
    'class' => "btn btn-success"
);
$CANCEL_BUTTON = array(
    "name" => "cancelBtn",
    "id" => "cancelBtn",
    "class" => "btn",
    "type" => "button",
    "content" => $this->lang->line("CANCEL"),
    "style" => "margin-left:10px"
);
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <title><?php echo ADMIN_WEBSITE_TITLE . "-" . $title ?></title>
        <?= $headerData['meta_tags']; ?>
        <?= $headerData['stylesheets']; ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body>
        <?php echo $this->load->view('include/header_view'); ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <!-- left menu starts -->
                <?php echo $this->load->view('include/sidebar_view'); ?>
                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <?php echo anchor('dashboard', $this->lang->line('HOME')) ?><span class="divider">/</span>
                            </li>
                            <li>
                                <?php echo anchor('javascript:;', $this->lang->line('USERS'), 'style="text-decoration:none;color:black; cursor:default; margin-left:-3px"') ?>
                            </li>
                        </ul>
                    </div>
                    <div class="row-fluid sortable">
                        <div class="box span12">
                            <div class="box-header well" data-original-title>
                                <h2><i class="icon-user"></i> <?php echo $ACTION_LABEL . ' ' . $this->lang->line('USER') ?></h2>
                            </div>
                            <div class="box-content">
                                <?php
                                echo $this->general_model->getMessages();
                                echo form_open('users/add', $FORM_ATTR);
                                echo form_hidden($USER_ID);
                                ?>
                                <fieldset>
                                    <div class="control-group">
                                        <?php echo form_label('<span class="ast">&ast;</span> ' . $this->lang->line('FIRST_NAME'), 'vFirstName', array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php echo form_input($FIRSTNAME) ?>
                                        </div>
                                    </div>
                                    <div class="control-group">
                                        <?php echo form_label('<span class="ast">&ast;</span> ' . $this->lang->line('LAST_NAME'), 'vLastName', array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php echo form_input($LASTNAME) ?>
                                        </div>
                                    </div>

                                    <div class="control-group">
                                        <?php echo form_label('<span class="ast">&ast;</span> ' . $this->lang->line('EMAIL_ADDRESS'), 'vEmail', array("class" => "control-label")); ?>
                                        <div class="controls">
                                            <?php echo form_input($EMAIL_ADDRESS) ?>
                                        </div>
                                    </div>          
                                    <div class="control-group">
                                        <?php
                                        if ($ACTION_LABEL == 'Add') {
                                            $ast = '<span class="ast">&ast;</span> ';
                                        } else {
                                            $ast = '';
                                        }
                                        echo form_label($ast . $this->lang->line('PASSWORD'), 'vPassword', array("class" => "control-label"));
                                        ?>
                                        <div class="controls">
                                            <?php echo form_password($PASSWORD) ?>
                                        </div>
                                    </div>          
                                    <div class="form-actions">
                                        <?php
                                        echo form_button($FORM_BUTTON);
                                        echo form_button($CANCEL_BUTTON);
                                        ?>
                                    </div>
                                </fieldset>
                                <?php echo form_close() ?>   
                            </div>
                        </div><!--/span-->
                    </div>
                    <!-- content ends -->
                </div><!--/#content.span10-->
            </div><!--/fluid-row-->
            <!--            <footer>
                            <p class="pull-left">&copy; <a href="http://usman.it" target="_blank">Muhammad Usman</a> 2012</p>
                            <p class="pull-right">Powered by: <a href="http://usman.it/free-responsive-admin-template">Charisma</a></p>
                        </footer>-->
        </div>
        <!-- BEGIN JAVASCRIPTS -->
        <?php echo $headerData['javascript']; ?>
        <script type="text/javascript">
            $(document).ready(function() {
<?php
if ($ACTION_LABEL == 'Add') {
    ?>
                    $("#users<?php echo $ACTION_LABEL ?>Form").validate({
                        errorClass: 'error',
                        rules: {
                            vFirst: {
                                required: true,
                                ONLY_ALPHABET: true,
                            },
                            vLast: {
                                required: true,
                                ONLY_ALPHABET: true,
                            },
                            vEmail: {
                                required: true,
                                email: true
                            },
                            vPassword: {
                                required: true
                            }
                        }
                    });
    <?php
} else {
    ?>
                    $("#users<?php echo $ACTION_LABEL ?>Form").validate({
                        errorClass: 'error',
                        rules: {
                            vFirst: {
                                required: true,
                                ONLY_ALPHABET: true,
                            },
                            vLast: {
                                required: true,
                                ONLY_ALPHABET: true,
                            },
                            vEmail: {
                                required: true,
                                email: true
                            }
                        }
                    });
    <?php
}
?>
                jQuery.validator.addMethod("ONLY_ALPHABET", function(value, element) {
                    return this.optional(element) || /^[a-zA-Z\s]+$/.test(value);
                }, "please enter only alphabet");

                $("#cancelBtn").click(function() {
                    window.location.href = ADMIN_URL + 'users';
                });

            });
        </script>
        <!-- END JAVASCRIPTS -->
    </body>
    <!-- END BODY -->
</html>