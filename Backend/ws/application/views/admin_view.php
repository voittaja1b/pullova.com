<?php
$headerData = $this->headerlib->data();
?>

<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <title><?php echo ADMIN_WEBSITE_TITLE . " - " . $title ?></title>
        <?php echo $headerData['meta_tags']; ?>
        <?php echo $headerData['stylesheets']; ?>
    </head>
    <!-- END HEAD -->
    <!-- BEGIN BODY -->
    <body>
        <?php echo $this->load->view('include/header_view'); ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <!-- left menu starts -->
                <?php echo $this->load->view('include/sidebar_view'); ?>
                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <?php echo anchor('dashboard', $this->lang->line('HOME')) ?><span class="divider">/</span>
                            </li>
                            <li>
                                <?php echo anchor('javascript:;', $this->lang->line('ADMINISTRATORS'),'style="text-decoration:none;color:black; cursor:default; margin-left:-3px"') ?>
                            </li>
                        </ul>
                    </div>
                    <div class="row-fluid sortable">		
                        <div class="box span12">
                            <div class="box-header well" data-original-title>
                                <h2><i class="icon-eye-open"></i> <?php echo $this->lang->line('ADMINISTRATOR_LIST') ?></h2>
                                <?php 
                                 echo anchor("admin/add/", '<i class="icon-plus icon-white"></i> ' . $this->lang->line("ADD"), array('class' => 'btn btn-success','style'=>'float:right; margin-right:-15px','title'=>'Add New Admin'));
                                ?>
                            </div>
                            <div class="box-content">
                                <?php
                                echo $this->general_model->getMessages();
                                if (isset($record_set) && $record_set != '') {
                                    ?>

                                    <table class="table table-striped table-bordered bootstrap-datatable datatable" id="results">
                                        <thead>
                                            <tr>
                                                <th><?php echo $this->lang->line("FIRST_NAME") ?></th>
                                                <th><?php echo $this->lang->line("LAST_NAME") ?></th>
                                                <th><?php echo $this->lang->line("EMAIL_ADDRESS") ?></th>
                                                <th><?php echo $this->lang->line("STATUS") ?></th>
                                                <th><?php echo $this->lang->line("ACTION") ?></th>
                                            </tr>
                                        </thead>   
                                        <tbody>
                                            <tr>
                                            </tr>
                                        </tbody>
                                    </table>            
                                    <?php
                                } else
                                    $this->general_model->noRecordsHere();
                                ?>
                            </div>
                        </div><!--/span-->

                    </div>
                    <!-- content ends -->
                </div><!--/#content.span10-->
            </div><!--/fluid-row-->

            <!--            <footer>
                            <p class="pull-left">&copy; <a href="http://usman.it" target="_blank">Muhammad Usman</a> 2012</p>
                            <p class="pull-right">Powered by: <a href="http://usman.it/free-responsive-admin-template">Charisma</a></p>
                        </footer>-->

        </div>
        <?php echo $headerData['javascript']; ?>
        <script>
            $(document).ready(function() {
                var oTable = $('#results').dataTable
                        ({
                            "bProcessing": true,
                            "sDom": "<'row-fluid'<'span6'l><'span6'f>r>t<'row-fluid'<'span6'i><'span6'p>>",
                            "sPaginationType": "bootstrap",
                            "oLanguage": {
                                "sLengthMenu": "_MENU_ records per page",
                                "oPaginate": {
                                    "sPrevious": "Prev",
                                    "sNext": "Next"
                                }
                            },
                            "aoColumnDefs": [{
                                    'bSortable': false,
                                    'aTargets': [0]
                                }],
                            'bServerSide': true,
                            "bDeferRender": true,
                            'sAjaxSource': '<?php echo base_url(); ?>admin/lists',
                            "fnServerData": function(sSource, aoData, fnCallback) {

                                $.ajax({
                                    "dataType": 'json',
                                    "type": "POST",
                                    "url": sSource,
                                    "data": aoData,
                                    "success": fnCallback

                                });
                            }

                        });
            });
        </script>
    </body>
    <!-- END BODY -->

</html>