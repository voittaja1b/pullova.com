<?php
$pageName = $this->uri->segment(1);
$pageName1 = $this->uri->segment(2);
$pageName2 = $this->uri->segment(3);

?>
<div class="span2 main-menu-span">
    <div class="well nav-collapse sidebar-nav">
        <ul class="nav nav-tabs nav-stacked main-menu">
            <li class="nav-header hidden-tablet">Main</li>
            <li<?php echo ($pageName == 'dashboard') ? ' class="active"' : "" ?>>
                <?php echo anchor('dashboard', '<i class="icon-home"></i><span class="hidden-tablet"> ' . $this->lang->line('DASHBOARD') . '</span>', 'class="ajax-link"'); ?>
<!--                <a class="ajax-link" href="index.html"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a>-->
            </li>
            
            <!-- Admin Mangement starts  -->
            <?php $admin_main_menu_class = ($pageName == 'admin' || ($pageName1 == 'add' && $pageName == 'admin' )) ? 'active' : "" ?>
            <?php $admin_sub_menu_class = ($pageName == 'admin' && $pageName1 == 'add' && $pageName2=='') ? 'active' : "" ?>
            <?php ($admin_sub_menu_class == 'active') ? $admin_main_menu_class = '' : "" ?>
            <li<?php echo ' class="' . $admin_main_menu_class . '"' ?>>
                <?php
                if ($admin_main_menu_class !== 'active') {
                    echo anchor('admin', '<i class="icon-wrench"></i><span class="hidden-tablet"> ' . $this->lang->line('ADMINISTRATORS') . '</span>', 'class="ajax-link"');
                } else {
                    echo anchor('javascript:;', '<i class="icon-eye-open"></i><span class="hidden-tablet"> ' . $this->lang->line('ADMINISTRATORS') . '</span>', 'class="ajax-link"');
                    ?>
                <?php } ?>
                <ul>
                    <li<?php echo ' class="' . $admin_sub_menu_class . '"' ?>>
                        <?php
                        if ($admin_sub_menu_class!=='active') {
                            echo anchor('admin/add', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hidden-tablet"> ' . $this->lang->line('ADD') . '</span>', 'class="ajax-link"');
                        } else {
                            echo anchor('javascript:;', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hidden-tablet"> ' . $this->lang->line('ADD') . '</span>', 'class="ajax-link"');
                        }
                        ?>

<!--                <a class="ajax-link" href="index.html"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a>-->
                    </li>

                </ul>
            </li>


            <!-- User Mangement starts  -->
            <?php $user_main_menu_class = ($pageName == 'users' || ($pageName1 == 'add' && $pageName == 'users' )) ? 'active' : "" ?>
            <?php $user_sub_menu_class = ($pageName == 'users' && $pageName1 == 'add' && $pageName2=='') ? 'active' : "" ?>
            <?php ($user_sub_menu_class == 'active') ? $user_main_menu_class = '' : "" ?>
            <li<?php echo ' class="' . $user_main_menu_class . '"' ?>>
                <?php
                if ($user_main_menu_class !== 'active') {
                    echo anchor('users', '<i class="icon-user"></i><span class="hidden-tablet"> ' . $this->lang->line('USERS') . '</span>', 'class="ajax-link"');
                } else {
                    echo anchor('javascript:;', '<i class="icon-user"></i><span class="hidden-tablet"> ' . $this->lang->line('USERS') . '</span>', 'class="ajax-link"');
                    ?>
                <?php } ?>
                <ul>
                    <li<?php echo ' class="' . $user_sub_menu_class . '"' ?>>
                        <?php
                        if ($user_sub_menu_class!=='active') {
                            echo anchor('users/add', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hidden-tablet"> ' . $this->lang->line('ADD') . '</span>', 'class="ajax-link"');
                        } else {
                            echo anchor('javascript:;', '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="hidden-tablet"> ' . $this->lang->line('ADD') . '</span>', 'class="ajax-link"');
                        }
                        ?>

<!--                <a class="ajax-link" href="index.html"><i class="icon-home"></i><span class="hidden-tablet"> Dashboard</span></a>-->
                    </li>

                </ul>
            </li>
        </ul>
    </div><!--/.well -->
</div>