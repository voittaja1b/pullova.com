<?php
$headerData = $this->headerlib->data();
?>
<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9"> <![endif]-->
<!--[if !IE]><!--> <html lang="en"> <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>

        <title><?php echo ADMIN_WEBSITE_TITLE . "-" . $title ?></title>
        <?php echo $headerData['meta_tags']; ?>
        <?php echo $headerData['stylesheets']; ?>
    </head>

    <!-- BEGIN BODY -->
    <body>
        <?php echo $this->load->view('include/header_view'); ?>
        <div class="container-fluid">
            <div class="row-fluid">
                <!-- left menu starts -->
                <?php echo $this->load->view('include/sidebar_view'); ?>
                <div id="content" class="span10">
                    <!-- content starts -->
                    <div>
                        <ul class="breadcrumb">
                            <li>
                                <?php echo anchor('javascript:;', $this->lang->line('HOME')) ?>
                            </li>
                        </ul>
                    </div>
                    <?php
                    if ($this->session->userdata('ADMINUSERTYPE') && $this->session->userdata('ADMINUSERTYPE') == "Super") {
                        ?>
                        <div class="sortable row-fluid">
                            <?php
                            echo anchor('admin', '<span class="icon32 icon-red icon-wrench"></span>
                                <div>Total Admins</div>
                                <div>' . $cntAdmin . '</div>', 'title="' . $cntAdmin . ' Admins" class="well span3 top-block"');
                            echo anchor('users', '<span class="icon32 icon-red icon-user"></span>
                                <div>Total Users</div>
                                <div>' . $cntUser . '</div>', 'title="' . $cntUser . ' Users" class="well span3 top-block"');
                            ?>
                        </div>
                        <?php
                    }
                    ?>
                    <!-- content ends -->
                </div><!--/#content.span10-->
            </div><!--/fluid-row-->

<!--            <footer>
                <p class="pull-left">&copy; <a href="http://usman.it" target="_blank">Muhammad Usman</a> 2012</p>
                <p class="pull-right">Powered by: <a href="http://usman.it/free-responsive-admin-template">Charisma</a></p>
            </footer>-->

        </div>
    </body>
    <!-- END BODY -->
    <?php echo $headerData['javascript']; ?>
</html>