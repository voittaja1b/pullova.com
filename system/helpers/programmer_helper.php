<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

if (!function_exists('pre')) {

    function pre($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
        exit;
    }

}

if (!function_exists('pr')) {

    function pr($data) {
        echo "<pre>";
        print_r($data);
        echo "</pre>";
    }

}

if (!function_exists('_j')) {

    function _j($data) {
        if (is_array($data))
            echo json_encode($data);
        else
            echo json_encode(array($data));
    }

}

if (!function_exists('je')) {

    function je($data) {
        if (is_array($data))
            echo json_encode($data);
        else
            echo json_encode(array($data));

        exit;
    }

}

if (!function_exists('compulsoryAlphaNumeric')) {

    function compulsoryAlphaNumeric($my_str) {

        $alpha_okay = FALSE;
        $num_okay = FALSE;
        $string_okay = TRUE;

        $without_alpha = str_replace(
                array_merge(
                        range("a", "z"), range("A", "Z")
                ), '', strtolower($my_str)
        );

        if (strlen($my_str) > strlen($without_alpha))
            $alpha_okay = true;

        $without_num = str_replace(range(0, 9), '', $without_alpha);


        if (strlen($without_alpha) > strlen($without_num))
            $num_okay = true;

        if (strlen($without_num) > 0)
            $string_okay = false;


        if (!($alpha_okay))
            return 'noalpha';
        else if (!($num_okay))
            return 'nonum';
        else if ($string_okay == false)
            return 'specialchar';
        else
            return TRUE;
    }

}

if (!function_exists('Xauto_link')) {

    /**
     * Replace links in text with html links
     *
     * @param  string $text
     * @return string
     */
    function Xauto_link($str, $attributes = array()) {
        $attrs = '';
        foreach ($attributes as $attribute => $value) {
            $attrs .= " {$attribute}=\"{$value}\"";
        }
        $str = ' ' . $str;
        $str = preg_replace(
                '`([^"=\'>])(((http|https|ftp)://|www.)[^\s<]+[^\s<\.)])`i', '$1<a href="$2"' . $attrs . ' target="_blank">$2</a>', $str
        );
        $str = substr($str, 1);
        $str = preg_replace('`href=\"www`', 'href="http://www', $str);
        // fügt http:// hinzu, wenn nicht vorhanden
        return $str;
    }

}

/* End of file programmer_helper.php */
/* Location: ./system/helpers/programmer_helper.php */