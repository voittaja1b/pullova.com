<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * @requires 'url' helper and 'session' library 
 */
class MY_Controller extends CI_Controller {
    function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
    }

    protected function _isLoggedIn() {
        return $this->session->userdata('user_logged_in');
    }

    protected function _checkLoginOrRedirect() {
        if ( ! $this->_isLoggedIn() )
        { 
            redirect('login');
            return false;
        }
        return true;
    }

    protected function _objToAry(&$obj, &$keys) {
        $ary = array();
        foreach($keys as $k) {
            $ary []= isset($obj->$k) ? $obj->$k : null;
        }
        return $ary;
    }
}