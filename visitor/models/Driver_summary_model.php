<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver_summary_model extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Try to get app usage state
     * @param $period :string all | day | week | month
     * @return array(
     *    'totalEarnings' => , 
     *    'totalTips' => ,
     *    'avgRating' => ,
     *    'totalMileage' => ,
     *    'totalRides' => ,
     * )
     */
    function getProfitSummary($driverId, $period = 'all') {
        $ret = array();
        // Count total Earnings, Profits, totalRides
        $query = $this->db
            ->select('SUM(vCost) as TotalEarnings, COUNT(*) as TotalRides, SUM(feedback) / COUNT(*) as AvgRating')
            ->from('tbl_feed')
            ->where('iDriverID', $driverId);
        switch($period) {
            case 'day':
                $query->where('dCreatedDate >= now() - INTERVAL 1 DAY ');
            case 'week':
                $query->where('dCreatedDate >= now() - INTERVAL 7 DAY ');
            case 'month':
                $query->where('dCreatedDate >= now() - INTERVAL 30 DAY ');
        }
        $query = $query->limit(1)->get();
        $res = $query->result() [0];
        $ret['totalEarnings'] = $res->TotalEarnings;
        $ret['totalRides'] = $res->TotalRides;
        $ret['avgRating'] = $res->AvgRating;

        return $ret;
    }
}