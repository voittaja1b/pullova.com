<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Driver_model extends CI_Model {
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    /**
     * Try authenticate a user by email and password
     * @return user record if successful, false if not successful
     */
    function doAuth($email, $password) {
        $hashedPassword = md5($password); // !BEWARE: no salt is used. the original version was like this.
        $query = $this->db
            ->select('iUserID, vEmail, vUsername, vDriverorNot, vDriverAccepts'
                . ', vDriverEarnings, vDriverValue, vDriverNameofBank, vDriverRoutingNr'
                . ', vDriverAccountNr, vDriverPaypal, vPassword'
                . ', vFirst, vLast, carPicture, vImage, vFbID'
                . ', fLat, fLong, eUserType, vActivationKey'
                . ', vDeviceToken, typeofcard, last4, vToken'
                . ', vCustomerId, eStatus, version, eLogin'
                . ', eDelete, dLoginDate, dCreateDate, tModifyDate'
                . ', iDeletedBy, iDriverPhone, userPhone'
                . ', driverLicensePlate, vCarType, codefordiscount'
                . ', balance, verifiedPhone, onlineornot'
                . ', bdDay, bdYear, bdMonth, vPromoCode')
            ->from('tbl_user')
            ->where('vEmail', $email)
            ->where('vPassword', $hashedPassword)
            ->where('eLogin', '1')
            ->limit(1)
            ->get();
        if ($query->num_rows() == 1) {
            return $query->result() [0];
        } else {
            return false;
        }
    }

}