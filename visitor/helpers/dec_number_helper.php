<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
// ------------------------------------------------------------------------

/**
 * Formats a numbers as bytes, based on size, and adds the appropriate suffix
 *
 * @access	public
 * @param	mixed	// will be cast as int
 * @return	string
 */
if ( ! function_exists('dec_k_format'))
{
	function dec_k_format($num, $precision = 1)
	{
		if ($num >= 1000)
		{
			$num = round($num / 1000, $precision);
			$unit = 'k';
		}
		else
		{
			$unit = '';
			return number_format($num).' '.$unit;
		}

		return number_format($num, $precision).' '.$unit;
	}
}


/* End of file number_helper.php */
/* Location: ./system/helpers/number_helper.php */