<?php
$this->load->helper('dec_number');
$summary = isset($summary) ? $summary : array( 'totalEarnings'=>0, 'totalProfits'=>0, 'totalDrivers'=>0, 'totalRides'=>0 );
?>
<div class="content-pane">
    <div class="pane-header">
        <h3>Dashboard</h3>
    </div>
    <div class="pane-body">
        <div class="filterbox underlined">
            <div class="col-md-2 col-md-offset-10">
                <div class="dropdown">
                    <?php
                        $periodFltValues = array(
                            'all' => 'All Time',
                            'day' => 'Daily',
                            'week' => 'Weekly',
                            'month' => 'Monthly'
                        );
                    ?>
                    <button class="btn btn-sm btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
                        <?=$periodFltValues[$period]?>
                        <span class="caret pull-right"></span>
                    </button>
                    <ul class="dropdown-menu dropdown-menu-right" aria-labelledby="dropdownMenu1">
                        <?php foreach($periodFltValues as $pfv => $pfl) {
                            echo '<li><a href="' . $baseUrl . 'dashboard?period=' . $pfv . '">' . $pfl . '</a></li>';
                        }?>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row text-center">
            <div class="col-20p">
                <h1 class="text-accent-1">$<?=dec_k_format($summary['totalEarnings'], 2)?></h1>
                <p>Total Earnings</p>
            </div>
            <div class="col-20p">
                <h1 class="text-gray">$<?=dec_k_format($summary['totalTips'], 2)?></h1>
                <p>Total Tips</p>
            </div>
            <div class="col-20p">
                <h1 class="text-gray"><?=number_format($summary['totalRides'])?></h1>
                <p>Total Rides</p>
            </div>
            <div class="col-20p">
                <h1 class="text-gray"><?=number_format($summary['avgRating'], 1)?></h1>
                <p>Avg. Rating</p>
            </div>
            <div class="col-20p">
                <h1 class="text-gray"><?=number_format($summary['totalMileage'])?><sub style="font-size:14px">Miles</sub></h1>
                <p>Total Mileage</p>
            </div>
        </div>
    </div>
</div>