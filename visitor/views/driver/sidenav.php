<?php
    $baseUrl = $this->config->item('base_url');
    $rootUrl = $this->config->item('root_url');
    $assetsUrl = $this->config->item('assets_url');
    $auth = isset($auth) ? $auth : array( 'user_fname'=>'', 'user_lname'=>'', 'user_avatar'=>'' );
?>
<div class="sidenav">
    <div class="text-center" style="padding: 30px">
        <img class="avatar hcenter" src="<?=$rootUrl . 'Backend/ws/images/profile/thumb/' . $auth['user_avatar']?>"/>
        <br/><span><?=$auth['user_fname']?></span>
    </div>
    <ul class="list-group">
        <li class="list-group-item <?= $active=='dashboard' ? 'active' : ''?>"><a href="<?=$baseUrl?>dashboard">Dashboard</a></li>
        <li class="list-group-item <?= $active=='trips' ? 'active' : ''?>"><a href="<?=$baseUrl?>mytrips">My Trips</a></li>
        <li class="list-group-item <?= $active=='profile' ? 'active' : ''?>"><a href="<?=$baseUrl?>profile">Profile</a></li>
        <li class="list-group-item <?= $active=='payment' ? 'active' : ''?>"><a href="<?=$baseUrl?>payment">Payment</a></li>
        <li class="list-group-item <?= $active=='pay_settings' ? 'active' : ''?>"><a href="<?=$baseUrl?>paysettings">Payout Settings</a></li>
    </ul>
</div>