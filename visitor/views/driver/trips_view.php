<?php
$baseUrl = $this->config->item('base_url');
$trips = isset($trips) ? $trips : array();
?>
<div class="content-pane">
    <div class="pane-header">
        <h3 class="text-uppercase">Trips</h3>
    </div>
    <div class="pane-body">
        <table id="data_table" class="table table-hover" width="100%">
        <thead>
            <tr>
                <th>Id</th>
                <th>Phone #</th>
                <th>Status</th>
                <th>From Address</th>
                <th>To Address</th>
                <th>Current Address</th>
                <th>View</th>
            </tr>
        </thead>
        <tbody>
        </tbody>
        </table>
    </div>
</div>
<script>
function initDataTable() {
    if($('#data_table').length > 0) {
        /* Set the defaults for DataTables initialisation */
        $.extend( true, $.fn.dataTable.defaults, {
            dom:"<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-2'l><'col-sm-3 text-right'i><'col-sm-7'p>>",
            renderer: 'bootstrap'
        } );
        $.fn.dataTable.ext.errMode = 'none';
        
        $('#data_table').DataTable( {
            "ajax" : '<?=$baseUrl?>mytrips/get_table_data',
            "columnDefs": [ //will use the first column as id column
                {
                    "targets": [ 0 ],
                    "visible": false,
                    "searchable": false
                },
                {
                    "targets": [ 6 ],
                    "searchable": false,
                    "sortable": false,
                    "data": null,
                    "render": function( data, type, row, meta ) {
                        return '<a href="<?=$baseUrl?>trips/view_trip/'+data[0]+'"><iron-icon icon="icons:visibility"></iron-icon></a>';
                    }
                }
            ]
        });
    }
}
</script>