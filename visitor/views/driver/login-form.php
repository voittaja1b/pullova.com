<?php
    $baseUrl = $this->config->item('base_url');
    $assetsUrl = $this->config->item('assets_url');
    $bowerUrl = $this->config->item('bower_dep_url');
?>
<form method="POST" action="<?=$baseUrl?>login">
    <div class="form hcenter" style="width:40%;margin-top:40px;">
        <div class="form-group">
            <button class="btn btn-lg text-uppercase form-group" type="button" name="facebook">Connect with Facebook</button>
        </div>
        <div>Separator</div>
        <div class="paper-material form-group">
            <label class="col-md-3">Email</label>
            <div class="col-md-9">
                <gold-email-input label="" noLabelFloat="true" name="email"></gold-email-input>
            </div>
        </div>
        <div class="paper-material form-group">
            <label class="col-md-3">Password</label>
            <div class="col-md-9">
                <input type="password" name="password" required="true" class="no-polymer" style="width:100%"/>
            </div>
        </div>
        <div class="form-group text-center" style="margin-top: 20px">
            <a href="#">Forgot password?</a>
        </div>
        <div class="form-group">
            <button class="btn btn-primary btn-lg text-uppercase form-group" role="button" type="submit" name="login">Sign in</button>
        </div>
    </div>
</form>
