<!DOCTYPE html>
<html lang="en">

<?php
    $baseUrl = $this->config->item('base_url');
    $assetsUrl = $this->config->item('assets_url');
    $auth = isset($auth) ? $auth : array( 'user_fullname'=>'', 'user_avatar'=>'' );
?>

<head>
	<meta charset="utf-8">
    <title>Pullova</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?=$assetsUrl?>styles/bootstrap.custom.css">
    <link rel="stylesheet" href="<?=$assetsUrl?>styles/style.css">
</head>
<body class="color-2">
    <div class="header color-1">
        <nav class="navbar navbar-inverse no-margin">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand no-padding" href="#">
                        <img src="<?=$assetsUrl?>images/logo.png" class="pull-left" style="margin:0 5px;"/>
                        <img src="<?=$assetsUrl?>images/logo-text.png" style="margin:2px 5px;" class="pull-left"/>
                    </a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="text-uppercase"><a href="<?=$baseUrl?>login">Login</a></li>
                    <li class="active text-uppercase"><a href="<?=$baseUrl?>signup">Sign up</a></li>
                </ul>
                <div class="nav navbar-nav navbar-right">
                    <a href="<?=$baseUrl?>signup" class="button btn-primary text-uppercase pull-right">Become a driver</a>
                </div>
            </div><!-- /.container -->
        </nav>
    </div>
    <div class="container" style="padding-bottom:100px">
        <h3 class="text-uppercase underlined">Contact us</h3>
        <form class="form" method="POST" action="<?=$baseUrl?>contact_us">
            <div class="form-wrapper hcenter" style="width:50%"> 
                <div class="form-group text-center row">
                    <div class="col-md-6" style="padding-left:0">
                        <input class="form-control" placeholder="First Name" name="first_name"/>
                    </div>
                    <div class="col-md-6" style="padding-right:0">
                        <input class="form-control" placeholder="Last Name" name="last_name"/>
                    </div>
                </div>
                <div class="form-group text-center">
                    <div class="col-md-6" style="padding-left:0">
                        <input class="form-control" placeholder="Email" name="email"/>
                    </div>
                    <div class="col-md-6" style="padding-right:0">
                        <input class="form-control"  placeholder="Phone #" name="phone"/>
                    </div>
                </div>
                <div class="form-group text-center">
                    <textarea class="form-control" placeholder="Comments" name="comments">
                    </textarea>
                </div>
                <div class="form-group text-center">
                    <button class="button btn-primary text-uppercase" type="submit" name="send">Send</button>
                </div>
            </div>
        </form>
    </div>
    <div class="footer color-1">
        <nav>
            <a href="<?=$baseUrl?>#" class="text-uppercase">Home</a>
            <a href="<?=$baseUrl?>about_us" class="text-uppercase">About us</a>
            <a href="<?=$baseUrl?>faq" class="text-uppercase">FAQ</a>
            <a href="<?=$baseUrl?>contact_us" class="text-uppercase">Contact us</a>
        </nav>
        <div class="copyright">Copyright 2016 &copy; Pullova LLC.</div>
    </div>
</body>
</html>
