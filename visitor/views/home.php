<!DOCTYPE html>
<html lang="en">

<?php
    $baseUrl = $this->config->item('base_url');
    $assetsUrl = $this->config->item('assets_url');
    $bowerUrl = $this->config->item('bower_dep_url');
    $auth = isset($auth) ? $auth : array( 'user_fullname'=>'', 'user_avatar'=>'' );
?>

<head>
	<meta charset="utf-8">
    <title>Pullova</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?=$assetsUrl?>styles/bootstrap.custom.css">
    <link rel="stylesheet" href="<?=$assetsUrl?>styles/style.css">
</head>
<body class="color-2">
    <div class="header color-1">
        <nav class="navbar navbar-inverse no-margin">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand no-padding" href="#">
                        <img src="<?=$assetsUrl?>images/logo.png" class="pull-left" style="margin:0 5px;"/>
                        <img src="<?=$assetsUrl?>images/logo-text.png" style="margin:2px 5px;" class="pull-left"/>
                    </a>
                </div>
                <div class="nav navbar-nav navbar-right">
                    <a href="<?=$baseUrl?>signup" class="button btn-primary text-uppercase pull-right">Become a driver</a>
                </div>
            </div><!-- /.container -->
        </nav>
        <div class="park-image-container">
            <!--img class="park-image" src="landing_1.png"/-->
            <div class="park-image-overlay color-1"></div>
            <div class="park-banner-overlay vcenter-container">
                <div class="vcenter-wrapper">
                    <div class="container vcenter text-center">
                        <h1 class="text-uppercase">Rides in Minutes</h1>
                        <h3>Wherever you're headed, count on Pullova for rides in minutes. The pullova matches you<br/> with local drivers at the tap of a button. Just request and go. </h3>
                        <div class="store-buttons">
                            <a href="#" class="store-image-container"><img src="<?=$assetsUrl?>images/applestore.png"/></a>
                            <a href="#" class="store-image-container"><img src="<?=$assetsUrl?>images/googleplay.png"/></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container text-center" style="padding-bottom:100px">
        <h1 style="margin:50px 0;">How Pullova Works</h1>
        <div class="row">
            <div class="col-md-4">
                <h2>1. Request</h2>
                <p class="text-gray">Whether you are riding solo or with friends, you've got options. Tap to request a ride.</p>
            </div>
            <div class="col-md-4">
                <h2>2. Ride</h2>
                <p class="text-gray">Get picked by the best. Our reliable drivers will get you where you need to go.</p>
            </div>
            <div class="col-md-4">
                <h2>3. Pay</h2>
                <p class="text-gray">When the ride ends, just pay and rate your driver through your phone.</p>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img src="<?=$assetsUrl?>images/./iphone-screen-1.png"/>
            </div>
            <div class="col-md-4">
                <img src="<?=$assetsUrl?>images/./iphone-screen-2.png"/>
            </div>
            <div class="col-md-4">
                <img src="<?=$assetsUrl?>images/./iphone-screen-3.png"/>
            </div>
        </div>
    </div>
    <div class="footer color-1">
        <nav>
            <a href="<?=$baseUrl?>#" class="text-uppercase">Home</a>
            <a href="<?=$baseUrl?>about_us" class="text-uppercase">About us</a>
            <a href="<?=$baseUrl?>faq" class="text-uppercase">FAQ</a>
            <a href="<?=$baseUrl?>contact_us" class="text-uppercase">Contact us</a>
        </nav>
        <div class="copyright">Copyright 2016 &copy; Pullova LLC.</div>
    </div>
</body>
</html>
