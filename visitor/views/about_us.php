<!DOCTYPE html>
<html lang="en">

<?php
    $baseUrl = $this->config->item('base_url');
    $assetsUrl = $this->config->item('assets_url');
    $bowerUrl = $this->config->item('bower_dep_url');
    $auth = isset($auth) ? $auth : array( 'user_fullname'=>'', 'user_avatar'=>'' );
?>

<head>
	<meta charset="utf-8">
    <title>Pullova</title>
    <link href='https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?=$assetsUrl?>styles/bootstrap.custom.css">
    <link rel="stylesheet" href="<?=$assetsUrl?>styles/style.css">
</head>
<body class="color-2">
    <div class="header color-1">
        <nav class="navbar navbar-inverse no-margin">
            <div class="container">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <a class="navbar-brand no-padding" href="#">
                        <img src="<?=$assetsUrl?>images/logo.png" class="pull-left" style="margin:0 5px;"/>
                        <img src="<?=$assetsUrl?>images/logo-text.png" style="margin:2px 5px;" class="pull-left"/>
                    </a>
                </div>
                <ul class="nav navbar-nav">
                    <li class="text-uppercase"><a href="<?=$baseUrl?>login">Login</a></li>
                    <li class="active text-uppercase"><a href="<?=$baseUrl?>signup">Sign up</a></li>
                </ul>
                <div class="nav navbar-nav navbar-right">
                    <a href="<?=$baseUrl?>signup" class="button btn-primary text-uppercase pull-right">Become a driver</a>
                </div>
            </div><!-- /.container -->
        </nav>
    </div>
    <div class="container" style="padding-bottom:100px">
        <h3 class="text-uppercase underlined">About</h3>
        <p class="text-gray">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Fusce sed elit eget purus porttitor tincidunt. Nulla nec neque. Sed dapibus. Phasellus nibh lacus, sodales eu, vestibulum a, condimentum a, magna. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. Vestibulum congue risus in nisl.
        </p>
        <p class="text-gray">
        Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aliquam erat volutpat. Nulla blandit euismod arcu. Etiam ut diam. Aenean viverra. Suspendisse magna ipsum, mattis eu, dictum ut, pulvinar sit amet, ipsum. Vivamus tincidunt adipiscing pede. Nullam pede. Pellentesque congue lacus in risus. Donec laoreet. Cras dictum hendrerit nulla. Maecenas lectus lacus, nonummy ut, volutpat at, sodales vel, velit.
        Cras et nisl et mauris porttitor porta. Donec tristique nibh a velit. Etiam augue leo, cursus semper, adipiscing sit amet, euismod non, elit. Vestibulum in lorem. Phasellus semper diam. Pellentesque eget velit.
        Cras metus orci, condimentum eget, tempor vitae, aliquam at, augue. Phasellus fermentum leo sed magna. Aliquam erat volutpat. Vestibulum volutpat. Maecenas velit urna, vestibulum quis, tempus eu, blandit quis, velit. Integer velit elit, imperdiet et, ultrices sit amet, porttitor vel, mauris.
        </p>
        <p class="text-gray">
        Nam nulla ante, aliquam eget, mattis et, hendrerit eu, eros. Vestibulum vitae ipsum. Integer dolor arcu, interdum sed, luctus ut, porta non, dolor. Suspendisse augue lorem, nonummy sed, mollis non, dignissim a, nunc. Nam placerat metus at ipsum. Curabitur gravida diam at est. Sed vitae nibh. Nullam et nunc semper magna tempus adipiscing. Donec semper wisi sit amet magna nonummy pulvinar.
        Sed wisi. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas dapibus metus nec massa. Cras risus. Cras quis magna. Ut porttitor magna in metus. Cras tempus, magna eget venenatis condimentum, lacus libero tristique felis, vel congue dolor nisl ac lorem. In eu massa. Vivamus ornare nulla non augue. Donec tincidunt. Cras sit amet est at purus condimentum tincidunt. Proin rhoncus bibendum pede. Nunc et dui.
        Vestibulum pulvinar. Donec vel orci quis ligula dictum scelerisque. Morbi dignissim. Ut eget velit in sem varius mattis. Aenean accumsan pulvinar felis. Praesent at mauris. Curabitur purus lorem, ullamcorper non, rutrum at, accumsan vel, eros. Aenean condimentum. Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos hymenaeos. 
        </p>
    </div>
    <div class="footer color-1">
        <nav>
            <a href="<?=$baseUrl?>#" class="text-uppercase">Home</a>
            <a href="<?=$baseUrl?>about_us" class="text-uppercase">About us</a>
            <a href="<?=$baseUrl?>faq" class="text-uppercase">FAQ</a>
            <a href="<?=$baseUrl?>contact_us" class="text-uppercase">Contact us</a>
        </nav>
        <div class="copyright">Copyright 2016 &copy; Pullova LLC.</div>
    </div>
</body>
</html>
