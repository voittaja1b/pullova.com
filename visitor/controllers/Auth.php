<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends MY_Controller {

    public function index()
    {
        $this->login();
    }

	public function login()
	{
        if( $this->_isLoggedIn() ) { //if already logged in, redirect to dashboard
            redirect('dashboard');
        } elseif($this->input->post('login') !== FALSE) { //when method is post, and the user has sent the login request
            $this->_doAuth();
        } else {
            $data = array(
                'contentPaneView' => 'driver/login-form',
                'contentPaneViewData' => array()
            );
            $this->load->view('driver/template-layout-pub', $data);
        }
    }

    public function register()
	{
        if($this->input->post('register') !== FALSE) { //when method is post, and the user has sent the sign-up request
            $this->_doRegister();
        } else {
            $data = array(
                'contentPaneView' => 'driver/signup-form',
                'contentPaneViewData' => array()
            );
            $this->load->view('driver/template-layout-pub', $data);
        }
    }

    public function logout() {
        $this->session->unset_userdata( array(
            'user_logged_in' => false,
            'SESS_RID_USRID' => '',
            'SESS_RID_USRAVATAR' => '',
            'SESS_RID_USREMAIL' => '',
            'SESS_RID_USRROLE' => '',
            'SESS_RID_USRFNAME' => '',
            'SESS_RID_USRLNAME' => '',
        ));
        redirect('login');
    }

    protected function _doAuth() {
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->load->model('Driver_model');
        // setting up form validation
        $this->form_validation->set_rules('email', 'Email', 'trim|required|email|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        if ($this->form_validation->run() == FALSE) { //form validation fails
            $viewData = array(
                'toasts' => array(
                    array('message' => 'Invalid Email and Password!', 'type'=>'warn')
                ),
                'contentPaneView' => 'driver/login-form',
                'contentPaneViewData' => array()
            );
            $this->load->view('driver/template-layout-pub', $viewData);
        } else { // form validation success
            $username = $this->input->post('email');
            $password = $this->input->post('password');
            $user = $this->Driver_model->doAuth($username, $password);
            if($user && $user->vDriverorNot == 'driver') { // authentication successs
                $this->session->set_userdata('user_logged_in', TRUE);
                $this->session->set_userdata('SESS_RID_USRID', $user->iUserID);
                $this->session->set_userdata('SESS_RID_USRAVATAR', $user->vImage);
                $this->session->set_userdata('SESS_RID_USREMAIL', $user->vEmail);
                $this->session->set_userdata('SESS_RID_USRROLE', $user->vDriverorNot);
                $this->session->set_userdata('SESS_RID_USRFNAME', $user->vFirst);
                $this->session->set_userdata('SESS_RID_USRLNAME', $user->vLast);
                redirect('dashboard');
                return;
            } else {
                $msg = 'Wrong Email or Password!';
                if($user && $user->vDriverorNot != 'driver') {
                    $msg = 'Only drivers can login to the website!';
                }
                $viewData = array(
                    'toasts' => array(
                        array('message' => $msg, 'type'=>'warn')
                    ),
                    'contentPaneView' => 'driver/login-form',
                    'contentPaneViewData' => array()
                );
                $this->load->view('driver/template-layout-pub', $viewData);
            }
        }
    }
}