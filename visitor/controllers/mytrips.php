<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Mytrips extends MY_Controller {

    /**
     * Trips Page
     */
    public function index()
	{
		if( $this->_checkLoginOrRedirect() ) {
            $auth = array(
                'user_fname'=>$this->session->userdata('SESS_RID_USRFNAME'),
                'user_lname'=>$this->session->userdata('SESS_RID_USRLNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_USRAVATAR')
            );
            $data = array(
                'sideNavViewData' => array(
                    'active' => 'trips',
                    'auth' => $auth
                ),
                'contentPaneView' => 'driver/trips_view',
                'contentPaneViewData' => array(),
                'auth' => $auth
            );
            $this->load->view('driver/template-layout-auth', $data);
        }
	}

    public function get_table_data()
    {
        //DB column map
        $columns = array(
            'iFeedID' => 'iFeedID',
            'userPhone' => 'user.userPhone',
            'vUserView' => 'vUserView',
            'fAddress' => 'fAddress',
            'tAddress' => 'tAddress',
            'cAddress' => 'cAddress'
        );
        $data = null;
        if( $this->_isLoggedIn() ) {
            $this->load->model('Trip_model');
            $predicate = array(
                'iDriverID' => $this->session->userdata('SESS_RID_USRID'),
                'offset' => ($this->input->post('start') >= 0) ? $this->input->post('start') : 0,
                'limit' => ($this->input->post('length') > 0) ? $this->input->post('length') : 100
            );
            $recordsTotal = $this->Trip_model->count($predicate);
            
            $dbColumns = array_values($columns);
            $objKeys = array_keys($columns);

            $res = $this->Trip_model->find($predicate, $dbColumns);
            foreach($res as $i=>$r) { //convert objects to array
                $res[$i] = $this->_objToAry($r, $objKeys);
            }
            $data = array(
                'draw' => $this->input->post('draw'),
                'recordsTotal' => $recordsTotal,
                'recordsFiltered' => $recordsTotal,
                'data' => &$res
            );
        } else {
            $data = array(
                'draw' => $this->input->post('draw'),
                'recordsTotal' => 0,
                'recordsFiltered' => 0,
                'data' => [],
                'error' => 'Not authorized'
            );
        }
        $this->output->set_content_type('application/json');
        echo json_encode($data);
    }

}
