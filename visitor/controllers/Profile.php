<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {

    /**
     * Profile Page
     */
    public function index()
	{
		if( $this->_checkLoginOrRedirect() ) {
            $auth = array(
                'user_fname'=>$this->session->userdata('SESS_RID_USRFNAME'),
                'user_lname'=>$this->session->userdata('SESS_RID_USRLNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_USRAVATAR')
            );
            $data = array(
                'sideNavViewData' => array(
                    'active' => 'profile',
                    'auth' => $auth
                ),
                'contentPaneView' => 'driver/profile_view',
                'contentPaneViewData' => array(
                    'summary' => $summary
                ),
                'auth' => $auth
            );
            $this->load->view('driver/template-layout-auth', $data);
        }
	}
}
