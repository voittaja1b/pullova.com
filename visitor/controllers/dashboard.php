<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Dashboard extends MY_Controller {

	/**
	 * Dashboard Page
	 */
	public function index()
	{
		if( $this->_checkLoginOrRedirect() ) {
            $period = ($this->input->get('period')!==FALSE) ? $this->input->get('period') : 'all';
            $this->load->model('Driver_summary_model');
            $summary = $this->Driver_summary_model->getProfitSummary($this->session->userdata('SESS_RID_USRID'), $period);
            $auth = array(
                'user_fname'=>$this->session->userdata('SESS_RID_USRFNAME'),
                'user_lname'=>$this->session->userdata('SESS_RID_USRLNAME'),
                'user_avatar'=>$this->session->userdata('SESS_RID_USRAVATAR')
            );
            $data = array(
                'sideNavViewData' => array(
                    'active' => 'dashboard',
                    'auth' => $auth
                ),
                'contentPaneView' => 'driver/dashboard',
                'contentPaneViewData' => array(
                    'summary' => $summary,
                    'period' => $period
                ),
                'auth' => $auth
            );
            $this->load->view('driver/template-layout-auth', $data);
        }
	}
}
