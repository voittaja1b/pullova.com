
-- --------------------------------------------------------

--
-- Table structure for table `tbl_rate`
--

CREATE TABLE IF NOT EXISTS `tbl_rate` (
  `ID` int(11) NOT NULL,
  `fBase` float NOT NULL COMMENT 'Base fare',
  `fPerMinute` float NOT NULL COMMENT 'Per minute',
  `fPerMile` float NOT NULL COMMENT 'Per mile',
  `fBookingFee` float NOT NULL COMMENT 'Booking Fee',
  `fMinimumFare` float NOT NULL COMMENT 'Minimum Fare',
  `vCity` varchar(100) NOT NULL COMMENT 'City',
  `fAdminFee` float NOT NULL COMMENT 'Admin fee',
  `bDefault` tinyint(1) NOT NULL COMMENT 'is default?'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Appliable Rates';

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_rate`
--
ALTER TABLE `tbl_rate`
  ADD PRIMARY KEY (`ID`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_rate`
--
ALTER TABLE `tbl_rate`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;


INSERT INTO `jeand_pullova`.`tbl_rate` (`ID`, `fBase`, `fPerMinute`, `fPerMile`, `fBookingFee`, `fMinimumFare`, `vCity`, `fAdminFee`, `bDefault`)
 VALUES (NULL, '1.2', '0.2', '1.24', '1.10', '4', 'All', '0.2', '1');